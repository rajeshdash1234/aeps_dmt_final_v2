package com.iserveu.aeps.bbps;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.iserveu.aeps.R;
import com.iserveu.aeps.fundtransferreport.MyErrorMessage;
import com.iserveu.aeps.utils.APIService;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_FETCH_BILL_AMOUNT_URL;
import static com.iserveu.aeps.BuildConfig.GET_AEPS_RECHARGE_URL;

public class Bbps extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private APIService apiService;
    Session session;
    LoadingView loadingView;
    Toolbar toolbar;

    LinearLayout spinner_linear;
    Button bbps_button,bbps_fetch_bill_button;
    SearchableSpinner bbps_select_operator,bbps_select_Spinner;
    EditText bbps_mobileno,bbps_mobilenoo,bbps_mobilenooo;
    TextView bbps_amount;
    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,authString,latitudeString,
            circleCodeString,longitudeString;

    String[] operatorBbps = new String[]{"MOBILE BILL PAYMENT","Vodafone Postpaid","Aircel Postpaid",

            "LANDLINE BILL PAYMENT","MTNL Delhi Landline-Retail","MTNL Delhi Landline-Online","MTNL Mumbai Landline-Retail",
            "MTNL Mumbai Landline-Online","BSNL Landline-Retail","BSNL Landline Utility",

            "ELECTRICITY BILL PAYMENT","Assam Power Distribution Company Ltd (APDCL)- RAPDR","Assam Power Distribution Company Ltd (APDCL)-NON RAPDR",
            "Bangalore Electricity Supply Company","Bharatpur Electricity Services Ltd","Bikaner Electricity Supply Limited (BkESL)",
            "Brihan Mumbai Electric Supply and Transport Undertaking","BSES Rajdhani","BSES Yamuna","Calcutta Electricity Supply Ltd",
            "Chhattisgarh State Electricity Board","Chamundeshwari Electricity Supply Corp Ltd (CESCOM)","Daman and Diu Electricity Department",
            "Dakshin Gujarat Vij Company Limited","Dakshin Haryana Bijli Vitran Nigam","DNH Power Distribution Company Limited",
            "Eastern Power Distribution Company of Andhra Pradesh Limited","Gulbarga Electricity Supply Company Limited",
            "Hubli Electricity Supply Company Ltd (HESCOM)","India Power Corporation Limited - Bihar","India Power Corporation - West Bengal",
            "Jamshedpur Utilities and Services Company Limited","Jaipur and Ajmer Viyut Vitran Nigam","Jodhpur Vidyut Vitran Nigam Ltd",
            "Jharkhand Bijli Vitran Nigam Limited (JBVNL)","Kota Electricity Distribution Ltd","Madhya Pradesh Paschim Kshetra Vidyut Vitaran Indor",
            "Meghalaya Power Distribution Corporation Ltd","Madhya Gujarat Vij Company Limited","MSEDC Limited Utility","Muzaffarpur Vidyut Vitran Limited",
            "North Delhi Power Limited","North Bihar Power Distribution Company Ltd","Noida Power Company Limited","ODISHA Discoms(B2B)",
            "ODISHA Discoms(B2C)","Paschim Gujarat Vij Company Limited","Punjab State Power Corporation Ltd (PSPCL)","Reliance Energy(Mumbai) Utility",
            "Rajasthan Vidyut Vitran Nigam Limited","South Bihar Power Distribution Company Ltd.","SNDL Nagpur","Soouthern power Distribution Company Ltd of Andhra Pradesh( APSPDCL)",
            "Torrent Power Utility","Tamil Nadu Electricity Board (TNEB)","TP Ajmer Distribution Ltd.","Tripura State Electricity Corporation Ltd",
            "Madhya Gujarat Vij Company Limited","Tata Power – Mumbai","Uttar Gujarat Vij Company Limited","Uttar Haryana Bijli Vitran Nigam",
            "Uttarakhand Power Corporation Limited","Uttar Pradesh Power Corp Ltd (UPPCL) - RURAL","Uttar Pradesh Power Corp Ltd (UPPCL) - URBAN",
            "West Bengal State Electricity",

            "BROADBAND PROVIDER","Hathway Broadband - Retail","Hathway Broadband - Online","Connect Broadband-Retail","Connect Broadband Online",

            "WATER BILL PAYMENT","UIT Bhiwadi","Uttarakhand Jal Sansthan(B2B)-Retail","Uttarakhand Jal Sansthan(B2C)-Online","Delhi Jal Board-Retail",
            "Delhi Jal Board-Online","Municipal Corporation of Gurugram",

            "GAS BILL PAYMENT","ADANI GAS","Gujarat Gas company Limited","IGL (Indraprast Gas Limited)","Mahanagar Gas Limited Utility",
            "Haryana City gas","Siti Energy","Tripura Natural Gas Company Ltd","Sabarmati Gas Limited (SGL)",
            "Unique Central Piped Gases Pvt Ltd (UCPGPL)","Vadodara Gas Limited"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbps);

        session = new Session(Bbps.this);

        rechargetypeString="bbps";
        stdCodeString="";
        pincodeString="";
        accountNoString="";
        latitudeString="";
        circleCodeString="";
        longitudeString="";
        authString="";

        bbps_select_operator = (SearchableSpinner) findViewById(R.id.bbps_select_operator);
        bbps_select_Spinner = (SearchableSpinner) findViewById(R.id.bbps_select_Spinner);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final List<String> operatorBbpsList = new ArrayList<>(Arrays.asList(operatorBbps));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_item,operatorBbpsList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0||position==3||position==10||position==65||position==70||position==77)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0||position==3||position==10||position==65||position==70||position==77){
                    // Set the hint text color gray
                    tv.setTextColor(Color.RED);
                    tv.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                }
                else {
//                    tv.setText("jitu");
                    tv.setTextColor(Color.BLACK);
                    tv.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bbps_select_operator.setAdapter(spinnerArrayAdapter);

        bbps_select_operator.setOnItemSelectedListener(this);

        bbps_mobileno = findViewById(R.id.bbps_mobileno);
        bbps_mobilenoo = findViewById(R.id.bbps_mobilenoo);
        bbps_mobilenooo = findViewById(R.id.bbps_mobilenooo);
        bbps_amount = findViewById(R.id.bbps_amount);
        spinner_linear = findViewById(R.id.spinner_linear);

        bbps_fetch_bill_button = findViewById(R.id.bbps_fetch_bill_button);
        bbps_button = findViewById(R.id.bbps_button);

        bbps_fetch_bill_button.setOnClickListener(this);
        bbps_button.setOnClickListener(this);

        bbps_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    mobilenoString = null;
                }else {
                    mobilenoString = bbps_mobileno.getText().toString();
                }
            }
        });

        bbps_mobilenoo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    accountNoString = null;
                }else {
                    accountNoString = bbps_mobilenoo.getText().toString();
                }
            }
        });

        bbps_mobilenooo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    authString = null;
                }else {
                    authString = bbps_mobilenooo.getText().toString();
                }
            }
        });


//        bbps_select_operator.setOnItemSelectedListener(this);
//
//        //Creating the ArrayAdapter instance having the country list
//        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorDth);
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        //Setting the ArrayAdapter data on the Spinner
//        bbps_select_operator.setAdapter(aa);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bbps_fetch_bill_button:
                rechargeBbpsFetchBill();
//                bbps_fetch_bill_button.setVisibility(View.GONE);
//                bbps_button.setVisibility(View.VISIBLE);
//        Toast.makeText(getApplicationContext(),"Enter CustomerId OR Enter Amount" , Toast.LENGTH_LONG).show();

                break;

            case R.id.bbps_button:
//                bbps_button.setVisibility(View.GONE);
//                bbps_fetch_bill_button.setVisibility(View.VISIBLE);
                // do your code
                rechargeBbpsSubmit();
//                Toast.makeText(getApplicationContext(),"Success" , Toast.LENGTH_LONG).show();

                break;

            default:
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        bbps_mobilenoo.setVisibility(View.GONE);
        bbps_mobilenooo.setVisibility(View.GONE);
        spinner_linear.setVisibility(View.GONE);

        operatorString = operatorBbps[i];
       // Toast.makeText(getApplicationContext(),operatorString , Toast.LENGTH_LONG).show();
        if (i<=2){
            bbps_mobileno.setHint("Phone Number");

        }else if (i>2&&i<=7){

            bbps_mobilenoo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Landline Number");
            bbps_mobilenoo.setHint("Account Number");
        }else if (i==8||i==9){

            bbps_mobilenoo.setVisibility(View.VISIBLE);
            spinner_linear.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Landline No. with STD (excl. 0)");
            bbps_mobilenoo.setHint("Account Number");
        }else if (i==11||i==12||i==16||i==19||i==23||i==27||i==29||i==30||i==36||i==37||i==38||i==40||i==43||i==44||i==45||i==46||i==51||i==54
                ||i==56||i==57||i==59||i==62||i==63||i==66||i==67||i==72||i==73||i==84||i==86||i==87){
            bbps_mobileno.setHint("Consumer ID/Number");

        }else if (i==13||i==21||i==22||i==28||i==47||i==60){
            bbps_mobileno.setHint("Account ID/Number");


        }else if (i==14||i==15||i==32||i==33||i==35||i==49||i==55||i==74||i==75||i==76){
            bbps_mobileno.setHint("K No./Number");

        }else if (i==17||i==18||i==42||i==50||i==58||i==79){
            bbps_mobileno.setHint("CA Number");

        }else if (i==20||i==31||i==80){
            bbps_mobileno.setHint("Business Partner No./BP No.");

        }else if (i==24){
            bbps_mobilenoo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Account Number");
            bbps_mobilenoo.setHint("Mobile Number");

        }else if (i==25||i==26||i==52||i==61){
            bbps_mobileno.setHint("Service Connection Number/Service No");

        }else if (i==34){
            bbps_mobilenoo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Consumer Number");
            bbps_mobilenoo.setHint("Sub-division Code");

        }else if (i==39){
            bbps_mobilenoo.setVisibility(View.VISIBLE);
            bbps_mobilenooo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Consumer ID/Number");
            bbps_mobilenoo.setHint("Billing Unit");
            bbps_mobilenooo.setHint("Processing Cycle");

        }else if (i==41){
            bbps_mobileno.setHint("Contract Account Number");

        }else if (i==48){
            bbps_mobilenoo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Consumer Number");
            bbps_mobilenoo.setHint("Cycle Number");

        }else if (i==53){
            bbps_mobilenoo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Service Number");
            bbps_mobilenoo.setHint("City Name");

        }else if (i==64){
            bbps_mobilenoo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("Consumer ID");
            bbps_mobilenoo.setHint("Mobile Number");

        }else if (i==68||i==69){
            bbps_mobileno.setHint("Directory Number");

        }else if (i==71||i==78||i==85){
            bbps_mobileno.setHint("Customer Number/ID");

        }else if (i==81){
            bbps_mobilenoo.setVisibility(View.VISIBLE);

            bbps_mobileno.setHint("CA Number");
            bbps_mobilenoo.setHint("Bill Group Number");

        }else if (i==82){
            bbps_mobileno.setHint("CRN Number");

        }else if (i==83){
            bbps_mobileno.setHint("ARN Number");
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void rechargeBbpsFetchBill() {
        showLoader();
        final BbpsRequestFetchBill bbpsRequestFetchBill = new BbpsRequestFetchBill(stdCodeString,pincodeString,amountString,mobilenoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,authString,longitudeString);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp123456600"+bbpsRequestFetchBill);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp12345660011"+session.getUserToken());
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566001177"+ Constants.BASE_URL+"getallrechargehere");

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_FETCH_BILL_AMOUNT_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            performEncodedBillAmount(bbpsRequestFetchBill,encodedUrl);
                            // proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
    public void proceedRecharge(String url){

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode",stdCodeString);
            obj.put("amount",amountString);
            obj.put("mobileNumber",mobilenoString);
            obj.put("accountNo",accountNoString);
            obj.put("circleCode",circleCodeString);
            obj.put("operatorCode",operatorString);
            obj.put("rechargeType",rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization",session.getUserToken())
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if(obj.getString("status").equalsIgnoreCase("0")){
                                    SuccessDialog(obj.getString("statusDesc"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message=gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            //transaction failed
                            ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                            //  anError.getResponse();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void performEncodedBillAmount(BbpsRequestFetchBill bbpsRequestFetchBill, String encodedUrl){
        final BbpsApiFetchBill bbpsApiFetchBill = this.apiService.getClient().create(BbpsApiFetchBill.class);

        bbpsApiFetchBill.getBbpsFetchBill(session.getUserToken(),bbpsRequestFetchBill, encodedUrl).enqueue(new Callback<BbpsResponseFetchBill>() {
            @Override
            public void onResponse(Call<BbpsResponseFetchBill> call, Response<BbpsResponseFetchBill> response) {
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566"+response.body());
                    bbps_button.setVisibility(View.VISIBLE);
                    bbps_fetch_bill_button.setVisibility(View.GONE);
                    amountString = response.body().getStatusDesc();
                    bbps_amount.setText(amountString);
//                    Toast.makeText(getApplicationContext(),response.body().getStatusDesc(),Toast.LENGTH_LONG).show();
                    hideLoader();
                }else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyOtp1234577"+response.body());
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    ErrorDialog(message.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BbpsResponseFetchBill> call, Throwable t) {
                hideLoader();
                Log.d("STATUS", "TransactionFailedVerifyOtp1234588"+t);
            }
        });

    }


    private void rechargeBbpsSubmit() {
        showLoader();
       final BbpsRequestSubmit bbpsRequestSubmit = new BbpsRequestSubmit(stdCodeString,pincodeString,amountString,mobilenoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,authString,longitudeString);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp123456600"+bbpsRequestSubmit);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp12345660011"+session.getUserToken());
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566001177"+ Constants.BASE_URL+"getallrechargehere");

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            performEncodedRecharge(bbpsRequestSubmit,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void performEncodedRecharge(BbpsRequestSubmit bbpsRequestSubmit, String encodedUrl){
        final BbpsApiSubmit bbpsApiSubmit = this.apiService.getClient().create(BbpsApiSubmit.class);

        bbpsApiSubmit.getBbpsSubmit(session.getUserToken(),bbpsRequestSubmit, encodedUrl).enqueue(new Callback<BbpsResponseSubmit>() {
            @Override
            public void onResponse(Call<BbpsResponseSubmit> call, Response<BbpsResponseSubmit> response) {
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566"+response.body());
                    bbps_button.setVisibility(View.GONE);
                    bbps_fetch_bill_button.setVisibility(View.VISIBLE);
                    hideLoader();
                    SuccessDialog(response.body().getStatusDesc());
                }else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyOtp1234577"+response.body());
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BbpsResponseSubmit> call, Throwable t) {
                hideLoader();
                Log.d("STATUS", "TransactionFailedVerifyOtp1234588"+t);
                ErrorDialog("RECHARGE FAILED! undefined");
            }
        });

    }

    public void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(Bbps.this);
        }
        loadingView.show();

    }


    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }

    private void ErrorDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Bbps.this);
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void SuccessDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Bbps.this);
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                       // onBackPressed();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }



}
