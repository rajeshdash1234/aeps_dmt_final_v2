package com.iserveu.aeps.bbps;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface BbpsApiFetchBill {
    @POST()
    Call<BbpsResponseFetchBill> getBbpsFetchBill(@Header("Authorization") String token, @Body BbpsRequestFetchBill bbpsRequestFetchBill, @Url String url);
}
