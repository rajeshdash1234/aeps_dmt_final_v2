package com.iserveu.aeps.bbps;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface BbpsApiSubmit {
    @POST()
    Call<BbpsResponseSubmit> getBbpsSubmit(@Header("Authorization") String token, @Body BbpsRequestSubmit bbpsRequestSubmit, @Url String url);

}
