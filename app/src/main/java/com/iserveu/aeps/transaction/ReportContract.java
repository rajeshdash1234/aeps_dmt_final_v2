package com.iserveu.aeps.transaction;

import java.util.ArrayList;

/**
 * ReportContract class handles the communication between ReportView and Presenter
 *
 * @author Subhalaxmi Panda
 * @date 23/06/18.
 */
public class ReportContract {


    /**
     * View interface sends report list to ReportActivity
     */
    public interface View {

        /**
         * showReports() showReports on ReportActivity
         */
        void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount);
        void showReports();
        void showLoader();
        void hideLoader();
        void emptyDates();


    }

    /**
     * UserActionsListener interface checks the load of Reports
     */
    interface UserActionsListener {
        void loadReports(String fromDate, String toDate, String token);
    }


}

