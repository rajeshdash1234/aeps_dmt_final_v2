package com.iserveu.aeps.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportModel {
    @SerializedName("txId")
    @Expose
    private String txId;

    @SerializedName("bankName")
    @Expose
    private String bankName;

    @SerializedName("apiTid")
    @Expose
    private String apiTid;

    @SerializedName("referenceNo")
    @Expose
    private String referenceNo;

    @SerializedName("transactionMode")
    @Expose
    private String transactionMode;

    @SerializedName("usertrackId")
    @Expose
    private String usertrackId;

    @SerializedName("user")
    @Expose
    private String user;

    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;


    @SerializedName("apiComment")
    @Expose
    private String apiComment;

    @SerializedName("previousAmount")
    @Expose
    private String previousAmount;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("balanceAmount")
    @Expose
    private String balanceAmount;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("api")
    @Expose
    private String api;

    public String getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        this.previousAmount = previousAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getApiComment() {
        return apiComment;
    }

    public void setApiComment(String apiComment) {
        this.apiComment = apiComment;
    }

    public ReportModel() {
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getApiTid() {
        return apiTid;
    }

    public void setApiTid(String apiTid) {
        this.apiTid = apiTid;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getUsertrackId() {
        return usertrackId;
    }

    public void setUsertrackId(String usertrackId) {
        this.usertrackId = usertrackId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
