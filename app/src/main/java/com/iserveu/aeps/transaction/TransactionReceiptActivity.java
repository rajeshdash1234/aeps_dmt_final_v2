package com.iserveu.aeps.transaction;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Util;

public class TransactionReceiptActivity extends AppCompatActivity {

    public TextView tokenDateTextView;
    public TextView trackIdTextView;
    public TextView bankNameTextView,apiTidTextView,referencesNoTextView,transactionModeTextView,transactionIdTextView,userNameReportTextView,apiCommentTextView;
    Button submitButton;
    ReportModel reportModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_transaction_receipt );


        tokenDateTextView= findViewById ( R.id.tokenDateTextView );
        trackIdTextView= findViewById ( R.id.trackIdTextView );
        bankNameTextView= findViewById ( R.id.bankNameTextView );
        apiTidTextView= findViewById ( R.id.apiTidTextView );
        referencesNoTextView= findViewById ( R.id.referencesNoTextView );
        transactionModeTextView=findViewById ( R.id.transactionModeTextView );
        transactionIdTextView= findViewById ( R.id.transactionIdTextView );
        userNameReportTextView= findViewById ( R.id.userNameReportTextView );
        apiCommentTextView= findViewById ( R.id.apiCommentTextView );
        submitButton= findViewById ( R.id.submitButton );

        trackIdTextView.setText("User Track ID : " + reportModel.getUsertrackId ());
       bankNameTextView.setText("Bank Name : " + reportModel.getBankName ());
        apiTidTextView.setText("API TID : " + reportModel.getApiTid ());
        referencesNoTextView.setText("Reference No : " + reportModel.getReferenceNo ());
        transactionModeTextView.setText("Transaction Mode : "+ reportModel.getTransactionMode ());
        transactionIdTextView.setText("Transaction ID : "+reportModel.getTxId ());
        userNameReportTextView.setText("User Name : "+reportModel.getUser ());
        tokenDateTextView.setText( Util.getDateFromTime(Long.parseLong(reportModel.getUpdatedDate())));
        apiCommentTextView.setText(reportModel.getApiComment());


        submitButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        } );
    }
}
