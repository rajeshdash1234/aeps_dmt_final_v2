package com.iserveu.aeps.transaction;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 6/23/2018.
 */

public class ReportRecyclerViewAdapter extends RecyclerView.Adapter<ReportRecyclerViewAdapter.ReportViewhOlder> implements Filterable {

    private List<ReportModel> reportModelListFiltered;
        private List<ReportModel> reportModels;
        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        reportModelListFiltered = reportModels;
                    } else {
                        List<ReportModel> filteredList = new ArrayList<> ();
                        for (ReportModel row : reportModels) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getApiComment ().toLowerCase().contains(charString.toLowerCase()) ||
                                    Util.getDateFromTime(Long.parseLong(row.getUpdatedDate())).toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getApiTid().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getBankName().toLowerCase().contains(charString.toLowerCase())||
                                    row.getTxId().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getReferenceNo().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getTransactionMode().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getUsertrackId().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getUser().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }

                        reportModelListFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = reportModelListFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    reportModelListFiltered = (ArrayList<ReportModel>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }



        public static class ReportViewhOlder extends RecyclerView.ViewHolder {


            public TextView tokenDateTextView;
            public TextView trackIdTextView;
            public TextView previousAmountTextView,balanceAmountTextView,amountTextView;
            public TextView bankNameTextView,apiTidTextView,referencesNoTextView,transactionModeTextView,transactionIdTextView,userNameReportTextView,apiCommentTextView;


            public ReportViewhOlder(View view) {
                super ( view );

                tokenDateTextView= view.findViewById ( R.id.tokenDateTextView );
                trackIdTextView= view.findViewById ( R.id.trackIdTextView );
                bankNameTextView= view.findViewById ( R.id.bankNameTextView );
                apiTidTextView= view.findViewById ( R.id.apiTidTextView );
                referencesNoTextView= view.findViewById ( R.id.referencesNoTextView );
                transactionModeTextView= view.findViewById ( R.id.transactionModeTextView );
                transactionIdTextView= view.findViewById ( R.id.transactionIdTextView );
                userNameReportTextView= view.findViewById ( R.id.userNameReportTextView );
                apiCommentTextView= view.findViewById ( R.id.apiCommentTextView );

                previousAmountTextView = view.findViewById(R.id.previousAmountTextView);
                balanceAmountTextView = view.findViewById(R.id.balanceAmountTextView);
                amountTextView = view.findViewById(R.id.amountTextView);


                if(previousAmountTextView.getText () !=null && !previousAmountTextView.getText ().toString ().matches ( "" )){
                    previousAmountTextView.setVisibility ( View.VISIBLE );
                }

                if(balanceAmountTextView.getText () !=null && !balanceAmountTextView.getText ().toString ().matches ( "" )){
                    balanceAmountTextView.setVisibility ( View.VISIBLE );
                }

                if(amountTextView.getText () !=null && !amountTextView.getText ().toString ().matches ( "" )){
                    amountTextView.setVisibility ( View.VISIBLE );
                }


                if(tokenDateTextView.getText () !=null && !tokenDateTextView.getText ().toString ().matches ( "" )){
                    tokenDateTextView.setVisibility ( View.VISIBLE );
                }
                if(trackIdTextView.getText () !=null && !trackIdTextView.getText ().toString ().matches ( "" )){
                    trackIdTextView.setVisibility ( View.VISIBLE );
                }
                 if(bankNameTextView.getText () !=null && !bankNameTextView.getText ().toString ().matches ( "" )){
                     bankNameTextView.setVisibility ( View.VISIBLE );
                }
                if(apiCommentTextView.getText () !=null && !apiCommentTextView.getText ().toString ().matches ( "" )){
                    apiCommentTextView.setVisibility ( View.VISIBLE );
                }
                if(apiTidTextView.getText () !=null && !apiTidTextView.getText ().toString ().matches ( "" )){
                    apiTidTextView.setVisibility ( View.VISIBLE );
                }
                if(referencesNoTextView.getText () !=null && !referencesNoTextView.getText ().toString ().matches ( "" )){
                    referencesNoTextView.setVisibility ( View.VISIBLE );
                }
                if(transactionModeTextView.getText () !=null && !transactionModeTextView.getText ().toString ().matches ( "" )){
                    transactionModeTextView.setVisibility ( View.VISIBLE );
                }
                if(transactionIdTextView.getText () !=null && !transactionIdTextView.getText ().toString ().matches ( "" )){
                    transactionIdTextView.setVisibility ( View.VISIBLE );
                }
                if(userNameReportTextView.getText () !=null && !userNameReportTextView.getText ().toString ().matches ( "" )){
                    userNameReportTextView.setVisibility ( View.VISIBLE );
                }


            }
        }



        public ReportRecyclerViewAdapter(List<ReportModel> reportModels) {
            this.reportModels = reportModels;
            this.reportModelListFiltered = reportModels;
        }



        public ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.report_row, parent, false);

            return new ReportViewhOlder(itemView);
        }

        public void onBindViewHolder(ReportViewhOlder holder, int position) {
            ReportModel reportModel = reportModelListFiltered.get(position);
          //  holder.tokenAndDate.setText( reportModels.get(position).getDateReport ()+""+ reportModels.get(position).getTokenDate ());
            holder.trackIdTextView.setText("User Track ID : " + reportModel.getUsertrackId ());
            holder.bankNameTextView.setText("Bank Name : " + reportModel.getBankName ());
            holder.apiTidTextView.setText("API TID : " + reportModel.getApiTid ());
            holder.referencesNoTextView.setText("Reference No : " + reportModel.getReferenceNo ());
            holder.transactionModeTextView.setText("Transaction Mode : "+ reportModel.getTransactionMode ());
            holder.transactionIdTextView.setText("Transaction ID : "+reportModel.getTxId ());
            holder.userNameReportTextView.setText("User Name : "+reportModel.getUser ());

            holder.previousAmountTextView.setText("Previous Amount : "+reportModel.getPreviousAmount());
            holder.balanceAmountTextView.setText("Balanced Amount : "+reportModel.getBalanceAmount());
            holder.amountTextView.setText("Amount : "+reportModel.getAmount());

            
            holder.tokenDateTextView.setText(Util.getDateFromTime(Long.parseLong(reportModel.getUpdatedDate())));
            holder.apiCommentTextView.setText(reportModel.getApiComment());


        }



        public int getItemCount() {
            return reportModelListFiltered.size();
        }

    }
