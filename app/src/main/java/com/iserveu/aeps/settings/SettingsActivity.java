package com.iserveu.aeps.settings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.finopaytech.finosdk.activity.DeviceSettingActivity;
import com.iserveu.aeps.R;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.fundtransfer.FundTransfer;
import com.iserveu.aeps.main2activity.Main2Activity;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.SharePreferenceClass;
import com.rw.loadingdialog.LoadingView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.UUID;

import fr.ganfra.materialspinner.MaterialSpinner;
import static com.iserveu.aeps.utils.Util.showProgress;
import static java.lang.System.in;

public class SettingsActivity extends AppCompatActivity {

    private MaterialSpinner deviceSpinner,btdeviceSpinner;
    private static final String[] ITEMS = {"Morpho","Mantra"};
    private static final String[] BT_ITEMS = {"BLUPRINTS"};
    UsbManager musbManager;
    private UsbDevice usbDevice;
    LoadingView loadingView;
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String morphodeviceid="SAGEM SA";
    String mantradeviceid="MANTRA";
    String morphoe2device="Morpho";
    Session session;
    TextView bt_device_name,device_name;
    boolean first_run = true;
    boolean from_BT = false,from_RD = false;
    SharePreferenceClass sharePreferenceClass;
    private static final UUID MY_UUID = UUID.fromString("0000110E-0000-1000-8000-00805F9B34FB");
    /** The BluetoothAdapter is the gateway to all bluetooth functions **/
    protected BluetoothAdapter bluetoothAdapter = null;

    /** We will write our message to the socket **/
    protected BluetoothSocket socket = null;

    /** The Bluetooth is an external device, which will receive our message **/
    BluetoothDevice blueToothDevice = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setToolbar();
        session = new Session(this);

        sharePreferenceClass = new SharePreferenceClass(SettingsActivity.this);

        deviceSpinner = findViewById(R.id.deviceSpinner);
        btdeviceSpinner = findViewById(R.id.btdeviceSpinner);
        bt_device_name = findViewById(R.id.bt_device_name);
        device_name = findViewById(R.id.device_name);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deviceSpinner.setAdapter(adapter);

        ArrayAdapter<String> bt_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, BT_ITEMS);
        bt_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        btdeviceSpinner.setAdapter(bt_adapter);

        deviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    from_BT = false;
                    from_RD = true;
                    showLoader();
                    musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                    updateDeviceList ();
                }
                if(position == 1){
                    from_BT = false;
                    from_RD = true;
                    showLoader();
                    musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                    updateDeviceList ();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btdeviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!first_run) {
                    from_BT = true;
                    from_RD = false;
                    Intent in = new Intent(SettingsActivity.this, MainActivity.class);
                    startActivity(in);
                }
                    //finish();
               first_run = false;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(SettingsActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
//        Toast.makeText(this, "on resume", Toast.LENGTH_SHORT).show();

        if(from_BT) {

            if (MainActivity.selected_btdevice != null) {
                bt_device_name.setVisibility(View.VISIBLE);
                bt_device_name.setText("Connected Device : " + MainActivity.selected_btdevice.getName());

            } else {
                bt_device_name.setVisibility(View.INVISIBLE);
            }
        }else{


            if(sharePreferenceClass.getBluetoothInstance()!=null) {
              //  blueToothDevice = sharePreferenceClass.getBluetoothInstance();
                try{
                   // if(blueToothDevice.getName()==null) {
                        // Grab the BlueToothAdapter. The first line of most bluetooth programs.
                        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                        // if the BluetoothAdapter.getDefaultAdapter(); returns null then the
                        // device does not have bluetooth hardware. Currently the emulator
                        // does not support bluetooth so this will this condition will be true.
                        // i.e. This code only runs on a hardware device an not on the emulator.
                        if (bluetoothAdapter == null) {
                            Log.e(this.toString(), "Bluetooth Not Available.");
                            return;
                        }
                         String address = sharePreferenceClass.getBluetoothInstance();
                        // This will find the remote device given the bluetooth hardware
                        // address.
                        // @todo: Change the address to the your device address
                    if(address!=null) {
                        blueToothDevice = bluetoothAdapter.getRemoteDevice(address);

                        for (Integer port = 1; port <= 3; port++) {
                            simpleComm(Integer.valueOf(port));
                        }

                        sharePreferenceClass.setBluetoothInstance(blueToothDevice);
                    }
                   // }
                }catch (Exception e){
                    e.printStackTrace();
                }

                bt_device_name.setVisibility(View.VISIBLE);
                bt_device_name.setText("Connected Device : " + blueToothDevice.getName());

            } else {
                bt_device_name.setVisibility(View.INVISIBLE);
            }
        }

        if(from_RD) {
            if (deviceSpinner.getSelectedItemPosition() == 2) {
                devicecheck();
            } else if (deviceSpinner.getSelectedItemPosition() == 1) {
                devicecheck();
            }

              updateDeviceInfo();
        }else{
              updateDeviceInfo();
        }
    }
    public void createBond(BluetoothDevice device)
            throws Exception {
      //  if (device.getBondState() == device.BOND_BONDED) {

           // Log.d(TAG, device.getName());
            BluetoothSocket mSocket = null;
            try {

                mSocket = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                //Log.d(TAG, "socket not created");
                e1.printStackTrace();
            }
            try {

                mSocket.connect();

            } catch (IOException e) {
                try {

                    mSocket.close();
                    //Log.d(TAG, "Cannot connect");
                } catch (IOException e1) {
                    //Log.d(TAG, "Socket not closed");
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }


            }
       // }
    }
    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        hideLoader ();
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            // Toast.makeText(DashboardActivity.this, "No Devices Currently Connected" + usbconnted, Toast.LENGTH_LONG).show();
            deviceConnectMessgae ();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(device !=null && device.getManufacturerName () != null){
                        if(device.getManufacturerName ().equalsIgnoreCase ( mantradeviceid )||device.getManufacturerName ().equalsIgnoreCase ( morphodeviceid )||device.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )){
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName ();
                            session.setUsbDevice(usbDevice.getManufacturerName ());
                            sharePreferenceClass.setUsbDevice(usbDevice.getManufacturerName());
                            sharePreferenceClass.setUsbDeviceSerial(deviceSerialNumber);
                        }

                        sharePreferenceClass.setConnectedRD_Device(device.getManufacturerName ());

                    }
                }
            }
            devicecheck ();
        }
    }

    private void updateDeviceInfo() {
        String rd_device_name = sharePreferenceClass.getConnectedRD_Device().trim();
        if (rd_device_name != null && rd_device_name.length()!=0) {
            device_name.setVisibility(View.VISIBLE);
            if (rd_device_name.equalsIgnoreCase(mantradeviceid)) {
                device_name.setText("Connected Device : " + mantradeviceid);
            }
            if (rd_device_name.equalsIgnoreCase(morphodeviceid)) {
                device_name.setText("Connected Device : " + morphodeviceid);
            }
            if (rd_device_name.equalsIgnoreCase(morphoe2device)) {
                device_name.setText("Connected Device : " + morphodeviceid);
            }

        }else {
            device_name.setVisibility(View.GONE);

        }

    }



    public void showLoader() {
        if (loadingView ==null){
            loadingView = showProgress(this);
        }
        loadingView.show();
    }

    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }
    }

    private void deviceConnectMessgae (){
        hideLoader();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect))
                .setMessage(getResources().getString(R.string.decive_please_connect))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();
    }
    private void rdserviceMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_install))
                .setMessage(getResources().getString(R.string.mantra_rd_service))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private void mantraMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private void morphoMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.morpho))
                .setMessage(getResources().getString(R.string.install_morpho_message))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         * play store intent
                         */
                        final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void devicecheck() {
        if(usbDevice == null) {
            deviceConnectMessgae ();
        }else {
            if(deviceSpinner.getSelectedItemPosition() == 1){
                if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphodeviceid )||usbDevice.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )) {
                    morphoinstallcheck ();
                }else{
                    deviceConnectMessgae();
                }
            }else if(deviceSpinner.getSelectedItemPosition() == 2) {
                if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(mantradeviceid)) {
                    installcheck();
                } else {
                    deviceConnectMessgae();
                }
            }
        }
    }

    private  void installcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");
        if(isAppInstalled) {
// This intent will help you to launch if the package is already installed
            if (serviceAppInstalled){
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.INFO");
                intent.setPackage ( "com.mantra.rdservice" );
                startActivityForResult ( intent, 1 );
            }else{
                rdserviceMessage ();

            }
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage ();
        }
    }
    private  void morphoinstallcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");
        if(isAppInstalled) {
//This intent will help you to launch if the package is already installed
            Intent intent1 = new Intent ();
            intent1.setAction ( "in.gov.uidai.rdservice.fp.INFO" );
            intent1.setPackage ( "com.scl.rdservice" );
//            intent1.addFlags ( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivityForResult ( intent1, 2 );
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            morphoMessage ();
        }
    }

    /*
   app installation check
   */
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                hideLoader();
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText ( this, "Your device is ready for use", Toast.LENGTH_SHORT ).show ();
                                finish ();
                                Intent intent = new Intent(SettingsActivity.this,SettingsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);

                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "result"+""+result, Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.hide();
                        }
                    }
                }
                break;
            case 2:
                if (resultCode == RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                hideLoader();
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText ( this, "Your device is ready for use", Toast.LENGTH_SHORT ).show ();
                                finish ();
                                Intent intent = new Intent(SettingsActivity.this,SettingsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);

                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "Decive info check", Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.hide();
                        }
                    }
                }
                break;
        }
    }
    private void setToolbar() {
        Toolbar mToolbar = findViewById ( R.id.toolbar );
        mToolbar.setTitle (getResources().getString(R.string.settings) );
        mToolbar.inflateMenu ( R.menu.bank_menu );
        mToolbar.setOnMenuItemClickListener ( new Toolbar.OnMenuItemClickListener () {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()==R.id.action_close) {
                    Intent intent = new Intent(SettingsActivity.this,Main2Activity.class);
                    startActivity(intent);
                    finish ();
                }
                return false;
            }
        } );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SettingsActivity.this,Main2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish ();
    }

    protected void simpleComm(Integer port) {
        // byte [] inputBytes = null;

        // The documents tell us to cancel the discovery process.
        bluetoothAdapter.cancelDiscovery();

        Log.d(this.toString(), "Port = " + port);
        try {
            // This is a hack to access "createRfcommSocket which is does not
            // have public access in the current api.
            // Note: BlueToothDevice.createRfcommSocketToServiceRecord (UUID
            // uuid) does not work in this type of application. .
            Method m = blueToothDevice.getClass().getMethod(
                    "createRfcommSocket", new Class[] { int.class });
            socket = (BluetoothSocket) m.invoke(blueToothDevice, port);

            // debug check to ensure socket was set.
            assert (socket != null) : "Socket is Null";

            // attempt to connect to device
            socket.connect();
            try {
                Log.d(this.toString(),
                        "************ CONNECTION SUCCEES! *************");

                // Grab the outputStream. This stream will send bytes to the
                // external/second device. i.e it will sent it out.
                // Note: this is a Java.io.OutputStream which is used in several
                // types of Java programs such as file io, so you may be
                // familiar with it.
                OutputStream outputStream = socket.getOutputStream();

                // Create the String to send to the second device.
                // Most devices require a '\r' or '\n' or both at the end of the
                // string.
                // @todo set your message
                String message = "---";

                // Convert the message to bytes and blast it through the
                // bluetooth
                // to the second device. You may want to use:
                // public byte[] getBytes (Charset charset) for proper String to
                // byte conversion.
                outputStream.write(message.getBytes());

            } finally {
                // close the socket and we are done.
                socket.close();
            }
            // IOExcecption is thrown if connect fails.
        } catch (IOException ex) {
            Log.e(this.toString(), "IOException " + ex.getMessage());
            // NoSuchMethodException IllegalAccessException
            // InvocationTargetException
            // are reflection exceptions.
        } catch (NoSuchMethodException ex) {
            Log.e(this.toString(), "NoSuchMethodException " + ex.getMessage());
        } catch (IllegalAccessException ex) {
            Log.e(this.toString(), "IllegalAccessException " + ex.getMessage());
        } catch (InvocationTargetException ex) {
            Log.e(this.toString(),
                    "InvocationTargetException " + ex.getMessage());
        }
    }


}