package com.iserveu.aeps.utility;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.iserveu.aeps.R;
import com.iserveu.aeps.fundtransferreport.MyErrorMessage;
import com.iserveu.aeps.utils.APIService;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_FETCH_BILL_AMOUNT_URL;
import static com.iserveu.aeps.BuildConfig.GET_AEPS_RECHARGE_URL;

public class Utility extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private APIService apiService;
    Session session;
    LoadingView loadingView;
    Toolbar toolbar;

    LinearLayout spinner_linear;
    Button utility_button,utility_fetch_bill_button;
    SearchableSpinner utility_select_operator,utility_select_Spinner;
    EditText utility_mobileno,utility_mobilenoo,utility_mobilenooo;
    TextView utility_amount;
    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,authString,latitudeString,
            circleCodeString,longitudeString;

    String[] operatorUtility = {"MTNL Delhi LandLine Utility","BSNL Landline Utility","Reliance Energy(Mumbai) Utility","MSEDC Limited Utility",
            "Torrent Power Utility","Mahanagar Gas Limited Utility","Tata AIG Life Utility","ICICI Pru Life Utility","BSES Rajdhani","BSES Yamuna",
            "North Delhi Power Limited","Brihan Mumbai Electric Supply and Transport Undertaking","Rajasthan Vidyut Vitran Nigam Limited",
            "Soouthern power Distribution Company Ltd of Andhra Pradesh( APSPDCL)","Bangalore Electricity Supply Company","Madhya Pradesh Madhya Kshetra Vidyut Vitaran Company Limited - Bhopal",
            "Noida Power Company Limited","Madhya Pradesh Paschim Kshetra Vidyut Vitaran Indor","Calcutta Electricity Supply Ltd","Chhattisgarh State Electricity Board","India Power Corporation Limited",
            "Jamshedpur Utilities and Services Company Limited","IGL (Indraprast Gas Limited)","Gujarat Gas company Limited","ADANI GAS",
            "Tikona Postpaid","Hathway Broadband Retail"};

   String[] serviceType = {"Landline Individual","Landline Corporate"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);

        session = new Session(Utility.this);

        rechargetypeString="Utility_bills";
        stdCodeString="";
        pincodeString="";
        accountNoString="";
        latitudeString="";
        circleCodeString="";
        longitudeString="";
        authString="";

        utility_select_operator = (SearchableSpinner) findViewById(R.id.utility_select_operator);
        utility_select_Spinner = (SearchableSpinner) findViewById(R.id.utility_select_Spinner);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        utility_select_operator.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorUtility);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        utility_select_operator.setAdapter(aa);


        ArrayAdapter serviceTypeAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,serviceType);
        serviceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        utility_select_Spinner.setAdapter(serviceTypeAdapter);




        utility_mobileno = findViewById(R.id.utility_mobileno);
        utility_mobilenoo = findViewById(R.id.utility_mobilenoo);
        utility_mobilenooo = findViewById(R.id.utility_mobilenooo);
        utility_amount = findViewById(R.id.utility_amount);
        spinner_linear = findViewById(R.id.spinner_linear);

        utility_fetch_bill_button = findViewById(R.id.utility_fetch_bill_button);
        utility_button = findViewById(R.id.utility_button);

        utility_fetch_bill_button.setOnClickListener(this);
        utility_button.setOnClickListener(this);

        utility_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    mobilenoString = null;
                }else {
                    mobilenoString = utility_mobileno.getText().toString();
                }
            }
        });

        utility_mobilenoo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    accountNoString = null;
                }else {
                    accountNoString = utility_mobilenoo.getText().toString();
                }
            }
        });

        utility_mobilenooo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    authString = null;
                }else {
                    authString = utility_mobilenooo.getText().toString();
                }
            }
        });


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.utility_fetch_bill_button:
                rechargeUtilityFetchBill();

                break;

            case R.id.utility_button:
                // do your code
                rechargeUtilitySubmit();
                //Toast.makeText(getApplicationContext(),"Success" , Toast.LENGTH_LONG).show();

                break;

            default:
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {



            utility_mobilenoo.setVisibility(View.GONE);
            utility_mobilenooo.setVisibility(View.GONE);
            spinner_linear.setVisibility(View.GONE);

            operatorString = operatorUtility[i];
            if (i == 0) {
                utility_mobilenoo.setVisibility(View.VISIBLE);
                utility_mobileno.setHint("Phone number");
                utility_mobilenoo.setHint("Customer Account Number");

            } else if (i == 1) {

                utility_mobilenoo.setVisibility(View.VISIBLE);
                spinner_linear.setVisibility(View.VISIBLE);

                utility_mobileno.setHint("Phone number");
                utility_mobilenoo.setHint("Account Number");
            } else if (i == 2) {
                utility_mobilenoo.setVisibility(View.VISIBLE);

                utility_mobileno.setHint("Customer number");
                utility_mobilenoo.setHint("Cycle Number");

            } else if (i == 3) {
                utility_mobilenoo.setVisibility(View.VISIBLE);
                utility_mobilenooo.setVisibility(View.VISIBLE);

                utility_mobileno.setHint("Customer number");
                utility_mobilenoo.setHint("Billing Unit");
                utility_mobilenooo.setHint("Processing Cycle");


            } else if (i == 4) {
                utility_mobilenoo.setVisibility(View.VISIBLE);

                utility_mobileno.setHint("Service Number");
                utility_mobilenoo.setHint("City");

            } else if (i == 5) {
                utility_mobilenoo.setVisibility(View.VISIBLE);

                utility_mobileno.setHint("Number");
                utility_mobilenoo.setHint("Account Number");

            } else if (i == 6 || i == 7) {
                utility_mobilenoo.setVisibility(View.VISIBLE);

                utility_mobileno.setHint("Policy Number");
                utility_mobilenoo.setHint("Date of Birth");

            } else if (i == 8 || i == 9 || i == 10 || i == 13 || i == 14 || i == 15 || i == 16 || i == 17 || i == 20 || i == 22 || i == 25 || i == 26) {
                utility_mobileno.setHint("Customer number");
            } else if (i == 11 || i == 23) {
                utility_mobileno.setHint("Service Number");
            } else if (i == 12) {
                utility_mobileno.setHint("K Number");
            } else if (i == 18) {
                utility_mobileno.setHint("Consumer ID");
            } else if (i == 19 || i == 21) {
                utility_mobileno.setHint("Business Partener Number");
            } else if (i == 24) {
                utility_mobileno.setHint("Customer ID");
            }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void rechargeUtilityFetchBill() {
        showLoader();
        final UtilityRequestFetchBill utilityRequestFetchBill = new UtilityRequestFetchBill(stdCodeString,pincodeString,amountString,mobilenoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,authString,longitudeString);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp12345660011"+session.getUserToken());
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566001177"+ Constants.BASE_URL+"getallrechargehere");

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_FETCH_BILL_AMOUNT_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                          //  performEncodedBillAmount(utilityRequestFetchBill,encodedUrl);
                             proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    public void proceedRecharge(String url){

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode",stdCodeString);
            obj.put("amount",amountString);
            obj.put("mobileNumber",mobilenoString);
            obj.put("accountNo",accountNoString);
            obj.put("circleCode",circleCodeString);
            obj.put("operatorCode",operatorString);
            obj.put("rechargeType",rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization",session.getUserToken())
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if(obj.getString("status").equalsIgnoreCase("0")){

                                    utility_button.setVisibility(View.VISIBLE);
                                    utility_fetch_bill_button.setVisibility(View.GONE);
                                    amountString = obj.getString("statusDesc");
                                    utility_amount.setText("Amount: "+amountString);
                                    SuccessDialog(amountString);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message=gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            //transaction failed
                            ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                            //  anError.getResponse();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
 /* public void performEncodedBillAmount(UtilityRequestFetchBill utilityRequestFetchBill, String encodedUrl){
      final UtilityApiFetchBill utilityApiFetchBill = this.apiService.getClient().create(UtilityApiFetchBill.class);

      utilityApiFetchBill.getUtilityFetchBill(session.getUserToken(),utilityRequestFetchBill, encodedUrl).enqueue(new Callback<UtilityResponseFetchBill>() {
          @Override
          public void onResponse(Call<UtilityResponseFetchBill> call, Response<UtilityResponseFetchBill> response) {
              if (response.isSuccessful()){
                  Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566"+response.body());
                  utility_button.setVisibility(View.VISIBLE);
                  utility_fetch_bill_button.setVisibility(View.GONE);
                  amountString = response.body().getStatusDesc();
                  utility_amount.setText(amountString);
                  hideLoader();
              }else {
                  hideLoader();
                  Log.d("STATUS", "TransactionFailedVerifyOtp1234577"+response.body());
                  Gson gson = new Gson();
                  MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                  //transaction failed
                  ErrorDialog(message.getMessage());
              }
          }

          @Override
          public void onFailure(Call<UtilityResponseFetchBill> call, Throwable t) {
              hideLoader();
              Log.d("STATUS", "TransactionFailedVerifyOtp1234588"+t);
          }
      });
  }
*/



    private void rechargeUtilitySubmit() {
        showLoader();
       final UtilityRequestSubmit utilityRequestSubmit = new UtilityRequestSubmit(stdCodeString,pincodeString,amountString,mobilenoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,authString,longitudeString);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp123456600"+utilityRequestSubmit);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp12345660011"+session.getUserToken());
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566001177"+ Constants.BASE_URL+"getallrechargehere");

        if (this.apiService == null) {
            this.apiService = new APIService();
        }

        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            performEncodedRecharge(utilityRequestSubmit,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void performEncodedRecharge(UtilityRequestSubmit utilityRequestSubmit, String encodedUrl){

        final UtilityApiSubmit utilityApiSubmit = this.apiService.getClient().create(UtilityApiSubmit.class);

        utilityApiSubmit.getUtilitySubmit(session.getUserToken(),utilityRequestSubmit, encodedUrl).enqueue(new Callback<UtilityResponseSubmit>() {
            @Override
            public void onResponse(Call<UtilityResponseSubmit> call, Response<UtilityResponseSubmit> response) {
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566"+response.body());
                    utility_button.setVisibility(View.GONE);
                    utility_fetch_bill_button.setVisibility(View.VISIBLE);
                    hideLoader();
                    SuccessDialog(response.body().getStatusDesc());
                }else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyOtp1234577"+response.body());
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UtilityResponseSubmit> call, Throwable t) {
                hideLoader();
                Log.d("STATUS", "TransactionFailedVerifyOtp1234588"+t);
                ErrorDialog("RECHARGE FAILED! undefined");

            }
        });

    }

    public void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(Utility.this);
        }
        loadingView.show();

    }


    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }

    private void ErrorDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Utility.this);
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void SuccessDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Utility.this);
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();


                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
