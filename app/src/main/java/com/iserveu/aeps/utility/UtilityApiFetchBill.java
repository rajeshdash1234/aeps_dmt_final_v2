package com.iserveu.aeps.utility;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface UtilityApiFetchBill {
    @POST()
    Call<UtilityResponseFetchBill> getUtilityFetchBill(@Header("Authorization") String token, @Body UtilityRequestFetchBill utilityRequestFetchBill, @Url String url);
}
