package com.iserveu.aeps.utility;

public class UtilityRequestFetchBill {

    @Override
    public String toString() {
        return "PrepaidRequest{" +
                "stdCode='" + stdCode + '\'' +
                ", pincode='" + pincode + '\'' +
                ", amount='" + amount + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", latitude='" + latitude + '\'' +
                ", circleCode='" + circleCode + '\'' +
                ", operatorCode='" + operatorCode + '\'' +
                ", rechargeType='" + rechargeType + '\'' +
                ", auth='" + auth + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    private String stdCode;

    public UtilityRequestFetchBill(String stdCode, String pincode, String amount, String mobileNumber, String accountNo, String latitude, String circleCode, String operatorCode, String rechargeType, String auth, String longitude) {
        this.stdCode = stdCode;
        this.pincode = pincode;
        this.amount = amount;
        this.mobileNumber = mobileNumber;
        this.accountNo = accountNo;
        this.latitude = latitude;
        this.circleCode = circleCode;
        this.operatorCode = operatorCode;
        this.rechargeType = rechargeType;
        this.auth = auth;
        this.longitude = longitude;
    }

    private String pincode;

    /*public PrepaidRequest(String amount, String mobileNumber, String operatorCode, String rechargeType) {
        this.amount = amount;
        this.mobileNumber = mobileNumber;
        this.operatorCode = operatorCode;
        this.rechargeType = rechargeType;
    }*/

    private String amount;

    private String auth;

    private String mobileNumber;

    private String accountNo;

    private String latitude;

    private String circleCode;

    private String operatorCode;

    private String rechargeType;

    private String longitude;

    public String getStdCode ()
    {
        return stdCode;
    }

    public void setStdCode (String stdCode)
    {
        this.stdCode = stdCode;
    }

    public String getPincode ()
    {
        return pincode;
    }

    public void setPincode (String pincode)
    {
        this.pincode = pincode;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getAuth ()
    {
        return auth;
    }

    public void setAuth (String auth)
    {
        this.auth = auth;
    }

    public String getMobileNumber ()
    {
        return mobileNumber;
    }

    public void setMobileNumber (String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountNo ()
    {
        return accountNo;
    }

    public void setAccountNo (String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getCircleCode ()
    {
        return circleCode;
    }

    public void setCircleCode (String circleCode)
    {
        this.circleCode = circleCode;
    }

    public String getOperatorCode ()
    {
        return operatorCode;
    }

    public void setOperatorCode (String operatorCode)
    {
        this.operatorCode = operatorCode;
    }

    public String getRechargeType ()
    {
        return rechargeType;
    }

    public void setRechargeType (String rechargeType)
    {
        this.rechargeType = rechargeType;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

}
