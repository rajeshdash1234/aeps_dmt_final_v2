package com.iserveu.aeps.onboarding;

import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.AppSignatureHashHelper;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.SMSReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import cdflynn.android.library.checkview.CheckView;

public class OtpVerificationFragment extends Fragment implements SMSReceiver.OTPReceiveListener {

    Button validate_otp;
    EditText editPhoneNo,edit_otp;
    TextView m_pin_headerrr;
    CheckView mCheckView;
    OnboardingActivity activity;
    private SMSReceiver smsReceiver;

    private EditText pin1,pin2,pin3,pin4,pin5,pin6;
    ProgressBar api_progress;
    String OTP="";
    public static String UniqueKey="";
    ImageView log_img;
    //Boolean ApiChange=false;




    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_otp_verification, container, false);
        activity = (OnboardingActivity) getActivity();
        mCheckView = (CheckView)view.findViewById(R.id.check);
        log_img = view.findViewById(R.id.log_img);
        api_progress = (ProgressBar)view.findViewById(R.id.api_progress);
        api_progress.setVisibility(View.VISIBLE);

        m_pin_headerrr = (TextView)view.findViewById(R.id.m_pin_headerrr);
        m_pin_headerrr.setText("We have sent a verification code to your registered mobile no."+ Constants.VERIFIED_USERPHONE+" , Please verify your code.");


        pin1 = (EditText)view.findViewById(R.id.pin1);
        pin2 = (EditText)view.findViewById(R.id.pin2);
        pin3 = (EditText)view.findViewById(R.id.pin3);
        pin4 = (EditText)view.findViewById(R.id.pin4);
        pin5 = (EditText)view.findViewById(R.id.pin5);
        pin6 = (EditText)view.findViewById(R.id.pin6);


        validate_otp = (Button)view.findViewById(R.id.validate_otp);
        validate_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // mCheckView.check();
               // activity.otpVerifiedUpdate(true);
                api_progress.setVisibility(View.GONE);
                OtpVerificationAPI();
            }
        });

        AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(getActivity());
        // This code requires one time to get Hash keys do comment and share key
        Log.d("TAG", "Apps Hash Key: " + appSignatureHashHelper.getAppSignatures().get(0));
        System.out.println("Hash Key::>>---  "+appSignatureHashHelper.getAppSignatures().get(0));
        UniqueKey = appSignatureHashHelper.getAppSignatures().get(0);
       // CallMobileNumberVerifiedApi(UniqueKey);
        startSMSListener();



        return view;

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(OnboardingActivity.onboardingActivity);
            // This code requires one time to get Hash keys do comment and share key
            Log.d("TAG", "Apps Hash Key: " + appSignatureHashHelper.getAppSignatures().get(0));
            //System.out.println("Hash Key::>>---  "+appSignatureHashHelper.getAppSignatures().get(0));
            UniqueKey = appSignatureHashHelper.getAppSignatures().get(0);

            CallMobileNumberVerifiedApi(UniqueKey);
        }


    }

    private void startSMSListener() {
        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            getActivity().registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onOTPReceived(String otp) {
        //showToast("OTP Received: " + otp);
        System.out.println(">>-----OTP"+otp);
        OTP = otp;

        String str1 = String.valueOf(otp.charAt(0));
        String str2 = String.valueOf(otp.charAt(1));
        String str3 = String.valueOf(otp.charAt(2));
        String str4 = String.valueOf(otp.charAt(3));
        String str5= String.valueOf(otp.charAt(4));
        String str6 = String.valueOf(otp.charAt(5));

        pin1.setText(str1);
        pin2.setText(str2);
        pin3.setText(str3);
        pin4.setText(str4);
        pin5.setText(str5);
        pin6.setText(str6);



        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(smsReceiver);
        }
    }

    @Override
    public void onOTPTimeOut() {
        showToast("OTP Time out");
    }

    @Override
    public void onOTPReceivedError(String error) {
        showToast(error);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(smsReceiver);
        }
    }


    private void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    private void CallMobileNumberVerifiedApi(String key) {

        JSONObject obj = new JSONObject();
        String url = "http://35.200.175.157:8080/apitest/api/generateMobileOtp/"+ Constants.VERIFIED_USERPHONE+"/"+key;
        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            api_progress.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response.toString());
                            if(obj.getString("status").equalsIgnoreCase("0")){

                                System.out.println(">>>>-----OTP RESPONSE" + response.toString());

                            }else{

                                Toast.makeText(getActivity(),obj.getString("statusDesc"),Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("...>>>>Error");

                    }
                });
    }

    public void OtpVerificationAPI(){
        JSONObject obj = new JSONObject();
        String Url_str = "http://35.200.175.157:8080/apitest/api/verifyMobile/"+ Constants.VERIFIED_USERPHONE+"/"+OTP;
        AndroidNetworking.post(Url_str)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            api_progress.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response.toString());
                            if(obj.getString("status").equalsIgnoreCase("0")){

                                System.out.println(">>>>-----OTP VERIFIED" + response.toString());
                                log_img.setVisibility(View.GONE);


                                mCheckView.check();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        activity.otpVerifiedUpdate(true);
                                    }
                                }, 500);

                            }else{

                                Toast.makeText(getActivity(),obj.getString("statusDesc"),Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("...>>>>Error");

                    }
                });


    }




}

