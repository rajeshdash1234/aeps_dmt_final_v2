package com.iserveu.aeps.onboarding;

public interface UpdateFragmentInterface {

    void userNameVerifiedUpdate(Boolean usernameFlag);
    void mpinVerifiedUpdate(Boolean mpinFlag);
    void passwordVerifiedUpdate(Boolean passwordFlag);
    void videoVerifiedUpdate(Boolean videoFlag);
    void otpVerifiedUpdate(Boolean otpFlag);
    void emailVerifiedUpdate(Boolean emailFlag);

}
