package com.iserveu.aeps.onboarding;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.UpdateLocationInterface;

import org.json.JSONException;
import org.json.JSONObject;

import cdflynn.android.library.checkview.CheckView;


public class UsernameVerificationFragment extends Fragment implements UpdateLocationInterface {

    CheckView mCheckView;
    private TextView location_tx;
      OnboardingActivity activity;
    private ProgressBar loc_progress,api_progress;
    private  Button validate_user;
    private EditText edit_username;
    private ImageView log_img;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_username_verification, container, false);
        mCheckView = (CheckView)rootView.findViewById(R.id.check);
        validate_user = rootView.findViewById(R.id.validate_user);
        loc_progress = (ProgressBar)rootView.findViewById(R.id.loc_progress);
        api_progress = (ProgressBar)rootView.findViewById(R.id.api_progress);
        edit_username = (EditText)rootView.findViewById(R.id.edit_username);
        log_img = (ImageView)rootView.findViewById(R.id.log_img);
        validate_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edit_username.getText().toString().isEmpty()) {
                    api_progress.setVisibility(View.VISIBLE);
                    ValidUserNameVerified(edit_username.getText().toString());

                }else{
                    edit_username.setError("Please Put your UserName");
                }
            }
        });
        location_tx = (TextView)rootView.findViewById(R.id.location_tx);
        activity = (OnboardingActivity) getActivity();
        activity.setLocationListener(this);
        return rootView;
    }

    private void ValidUserNameVerified(final String usernameString) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("userName", usernameString);
            obj.put("user_latt", Constants.VERIFIED_USER_LACT);
            obj.put("user_long", Constants.VERIFIED_USER_LONG);

            AndroidNetworking.post("http://35.200.175.157:8080/apitest/api/validateUser")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                api_progress.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                if(obj.getString("status").equalsIgnoreCase("0")){

                                    System.out.println(">>>>-----" + response.toString());
                                    String userMobile = obj.getString("mobileNumber");
                                    String userEmail = obj.getString("email");
                                    Constants.VERIFIED_USERPHONE = userMobile;
                                    Constants.VERIFIED_USER_EMAIL = userEmail;
                                    Constants.VERIFIED_USERID = usernameString;

                                    JSONObject stepsObj = obj.getJSONObject("verificationStatus");
                                    final Boolean step1 = stepsObj.getBoolean("step1");
                                    final Boolean step2 = stepsObj.getBoolean("step2");
                                    final Boolean step3 = stepsObj.getBoolean("step3");
                                    final Boolean step4 = stepsObj.getBoolean("step4");
                                    log_img.setVisibility(View.GONE);
                                    mCheckView.check();
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            activity.userNameVerifiedUpdate(true,step1,step2,step3,step4);
                                        }
                                    }, 500);

                                }else{

                                    Toast.makeText(getActivity(),obj.getString("statusDesc"),Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("...>>>>Error");

                        }
                    });
        }catch (JSONException e) {
            e.printStackTrace();
        }


    }




    ///-----------------------------------------








    @Override
    public void onLocationUpdate(Location location){
       // location_tx.setText(location.getLatitude()+","+location.getLongitude());
        Constants.VERIFIED_USER_LACT = String.valueOf(location.getLatitude());
        Constants.VERIFIED_USER_LONG = String.valueOf(location.getLongitude());
    }

    @Override
    public void onAddressUpdate(String address) {
        loc_progress.setVisibility(View.GONE);
        location_tx.setText(address);
        Constants.VERIFIED_USER_LOC = address;

    }
}
