package com.iserveu.aeps.onboarding;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.iserveu.aeps.R;
import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.utils.UpdateLocationInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class OnboardingActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    ViewPager onbording_view_pager;

    private OnboardingViewPagerAdapter onboardingViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    //private int[] layouts;
    private Button btnSkip, btnNext;
    //private PreferenceManager prefManager;
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    UpdateLocationInterface updateLocationInterface;
    int currentPage=0;
    UsernameVerificationFragment fragment;
    public static OnboardingActivity onboardingActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        setUpGClient();
        onboardingActivity = this;

        onbording_view_pager = (ViewPager) findViewById(R.id.onbording_view_pager);
        onbording_view_pager.beginFakeDrag();
        onboardingViewPagerAdapter = new OnboardingViewPagerAdapter(getSupportFragmentManager());
        onbording_view_pager.setAdapter(onboardingViewPagerAdapter);

        dotsLayout = (LinearLayout) findViewById ( R.id.layoutDots );
        btnSkip = (Button) findViewById ( R.id.btn_skip );
        btnNext = (Button) findViewById ( R.id.btn_next );

        /*layouts = new int[]{R.layout.first_intro_screen,
                R.layout.second_intro_screen,
                R.layout.third_intro_screen};*/
        // adding bottom dots
        addBottomDots ( 0 );
        onbording_view_pager.addOnPageChangeListener ( viewPagerPageChangeListener );

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onbording_view_pager.setCurrentItem(currentPage+1);
                //currentPage = currentPage+1;
               // btnNext.setVisibility(View.GONE);
                Intent intent = new Intent(OnboardingActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


    }
    private void addBottomDots(int currentPage) {
        dots = new TextView[ 6 ];

        int[] colorsActive = getResources ().getIntArray ( R.array.array_dot_active );
        int[] colorsInactive = getResources ().getIntArray ( R.array.array_dot_inactive );

        dotsLayout.removeAllViews ();
        for (int i = 0; i < dots.length; i++) {
            dots[ i ] = new TextView ( this );
            dots[ i ].setText ( Html.fromHtml ( "&#8226;" ) );
            dots[ i ].setTextSize ( 35 );
            dots[ i ].setTextColor ( colorsInactive[ currentPage ] );
            dotsLayout.addView ( dots[ i ] );
        }

        if (dots.length > 0) dots[ currentPage ].setTextColor ( colorsActive[ currentPage ] );
    }
    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener () {

        @Override
        public void onPageSelected(int position) {
            addBottomDots ( position );
            if (position == 0) {
                btnSkip.setVisibility(View.INVISIBLE);
            } else if( position == 2) {
                btnSkip.setVisibility ( View.INVISIBLE );
            }else{
                btnSkip.setVisibility ( View.INVISIBLE );
             }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };




    //--------------------------------------------------------------------------------------------
    public void setLocationListener(UpdateLocationInterface listener) {
        this.updateLocationInterface = listener;
    }


    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();



    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            updateLocationInterface.onLocationUpdate(location);


            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(OnboardingActivity.this, Locale.getDefault());

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            Log.e("latitude", "latitude--" + latitude);

            try {
                Log.e("latitude", "inside latitude--" + latitude);
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses != null && addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    updateLocationInterface.onAddressUpdate(address);

                    //locationTxt.setText(address + " " + city + " " + country);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(OnboardingActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(OnboardingActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(OnboardingActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }

    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(OnboardingActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int permissionLocation = ContextCompat.checkSelfPermission(OnboardingActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }
    public void userNameVerifiedUpdate(Boolean flag,Boolean step1,Boolean step2,Boolean step3,Boolean step4){
        if(flag==true){
            //Toast.makeText(getApplicationContext(),"Show",Toast.LENGTH_SHORT).show();
            //btnNext.setVisibility(View.VISIBLE);
            if(step2==true){

                if(step3==true){
                    if(step4==true){
                        Intent intent = new Intent(this, LoginActivity.class);
                        startActivity(intent);
                        finish();

                    }else{
                    onbording_view_pager.setCurrentItem(5);
                    }
                }else{
                    currentPage=4;
                    onbording_view_pager.setCurrentItem(4);
                    currentPage = currentPage+1;
                }

            }else{
                onbording_view_pager.setCurrentItem(currentPage+1);
                currentPage = currentPage+1;
            }


        }

    }

    public void mpinVerifiedUpdate(Boolean flag){
        if(flag==true){
            //Toast.makeText(getApplicationContext(),"Show",Toast.LENGTH_SHORT).show();
            onbording_view_pager.setCurrentItem(currentPage+1);
            currentPage = currentPage+1;
        }

    }

    public void passwordVerifiedUpdate(Boolean flag){
        if(flag==true){
           // Toast.makeText(getApplicationContext(),"Show",Toast.LENGTH_SHORT).show();
            onbording_view_pager.setCurrentItem(currentPage+1);
            currentPage = currentPage+1;
        }
    }



    public void videoVerifiedUpdate(Boolean flag){
        if(flag==true){
           // Toast.makeText(getApplicationContext(),"Show",Toast.LENGTH_SHORT).show();
            onbording_view_pager.setCurrentItem(currentPage+1);
            currentPage = currentPage+1;
        }
    }

    public void otpVerifiedUpdate(Boolean flag){

        if(flag==true){
            //Toast.makeText(getApplicationContext(),"Show",Toast.LENGTH_SHORT).show();
            onbording_view_pager.setCurrentItem(currentPage+1);
            currentPage = currentPage+1;
        }

    }

    public void emailVerifiedUpdate(Boolean flag){
        if(flag==true){
            Toast.makeText(getApplicationContext(),"Successfully verified. ",Toast.LENGTH_SHORT).show();
            //btnNext.setVisibility(View.VISIBLE);
            //onbording_view_pager.setCurrentItem(currentPage+1);
            //currentPage = currentPage+1;
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
