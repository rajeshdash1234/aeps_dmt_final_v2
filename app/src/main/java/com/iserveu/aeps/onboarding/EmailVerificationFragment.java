package com.iserveu.aeps.onboarding;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import cdflynn.android.library.checkview.CheckView;

public class EmailVerificationFragment extends Fragment {

    private TextView header;
    private Button validate_email;
    private CheckView mCheckView;
    OnboardingActivity activity;

    private EditText pin1,pin2,pin3,pin4,pin5,pin6;
    ProgressBar api_progress;
    String OTP="";
    private ImageView log_img;
    boolean check1=false,check2=false,check3=false,check4=false,check5=false,check6=false;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_email_verification, container, false);
        activity = (OnboardingActivity) getActivity();
        mCheckView = (CheckView)view.findViewById(R.id.check);
        api_progress = (ProgressBar)view.findViewById(R.id.api_progress);
        api_progress.setVisibility(View.VISIBLE);
        log_img = (ImageView)view.findViewById(R.id.log_img);

        header = (TextView)view.findViewById(R.id.m_pin_headerrr);
        header.setText("We have sent a verification code to your registered email id "+ Constants.VERIFIED_USER_EMAIL+" , Please verify your code.");
        //edit_email = (EditText)view.findViewById(R.id.edit_email);

       // CallEmailVerifiedApi();


        pin1 = (EditText)view.findViewById(R.id.pin1);
        pin2 = (EditText)view.findViewById(R.id.pin2);
        pin3 = (EditText)view.findViewById(R.id.pin3);
        pin4 = (EditText)view.findViewById(R.id.pin4);
        pin5 = (EditText)view.findViewById(R.id.pin5);
        pin6 = (EditText)view.findViewById(R.id.pin6);

        pin1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()>=1){
                    pin2.requestFocus();
                }

            }
        });
        pin2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    pin3.requestFocus();
                }

            }
        });
        pin3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    pin4.requestFocus();
                }

            }
        });
        pin4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    pin5.requestFocus();
                }

            }
        });
        pin5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    pin6.requestFocus();
                }

            }
        });
        pin6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    Util.hideKeyboard(getActivity(),pin6);

                }

            }
        });




        validate_email = (Button)view.findViewById(R.id.validate_email);
        validate_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!pin1.getText().toString().isEmpty()&& !pin2.getText().toString().isEmpty() &&
                        !pin3.getText().toString().isEmpty()&& !pin4.getText().toString().isEmpty()&&
                        !pin5.getText().toString().isEmpty()&& !pin6.getText().toString().isEmpty()){

                    OTP = pin1.getText().toString().trim()+pin2.getText().toString().trim()+pin3.getText().toString().trim()+pin4.getText().toString().trim()+pin5.getText().toString().trim()+pin6.getText().toString().trim();

                    api_progress.setVisibility(View.GONE);
                    OtpVerificationAPI();
                   // mCheckView.check();
                   // activity.emailVerifiedUpdate(true);


                }else{
                    Toast.makeText(getActivity(),"Enter your OTP", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            CallEmailVerifiedApi();
        }


    }
    private void CallEmailVerifiedApi() {
        try {
        JSONObject obj = new JSONObject();
        obj.put("email", Constants.VERIFIED_USER_EMAIL);


        String url = "http://35.200.175.157:8080/apitest/api/sendOtpToEmail";
        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            api_progress.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response.toString());
                            if(obj.getString("status").equalsIgnoreCase("0")){

                                System.out.println(">>>>-----Email sent" + response.toString());

                            }else{

                                Toast.makeText(getActivity(),obj.getString("statusDesc"),Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("...>>>>Error");

                    }
                });
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void OtpVerificationAPI(){
        try {
            JSONObject obj = new JSONObject();
            obj.put("email", Constants.VERIFIED_USER_EMAIL);
            obj.put("otp", OTP);

        String Url_str = "http://35.200.175.157:8080/apitest/api/verifyEmail";
        AndroidNetworking.post(Url_str)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            api_progress.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response.toString());
                            if(obj.getString("status").equalsIgnoreCase("0")){

                                System.out.println(">>>>-----Email Verified" + response.toString());
                                log_img.setVisibility(View.GONE);
                                mCheckView.check();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        activity.emailVerifiedUpdate(true);
                                    }
                                }, 500);

                            }else{

                                Toast.makeText(getActivity(),obj.getString("statusDesc"),Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("...>>>>Error");

                    }
                });
        }catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
