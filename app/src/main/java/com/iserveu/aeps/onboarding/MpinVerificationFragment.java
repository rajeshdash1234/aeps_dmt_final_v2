package com.iserveu.aeps.onboarding;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Util;

import cdflynn.android.library.checkview.CheckView;

public class MpinVerificationFragment extends Fragment {

    //private EditText edit_mpin,edit_reenter_mpin;
    private Button validate_mpin;
    OnboardingActivity activity;
    CheckView mCheckView;
    EditText pin1,pin2,pin3,pin4,re_pin1,re_pin2,re_pin3,re_pin4;
    private ImageView log_img;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_mpin_verification, container, false);
        mCheckView = (CheckView)rootView.findViewById(R.id.check);

        log_img = (ImageView)rootView.findViewById(R.id.log_img);
        pin1 = (EditText)rootView.findViewById(R.id.pin1);
        pin2 = (EditText)rootView.findViewById(R.id.pin2);
        pin3 = (EditText)rootView.findViewById(R.id.pin3);
        pin4 = (EditText)rootView.findViewById(R.id.pin4);
        re_pin1 = (EditText)rootView.findViewById(R.id.re_pin1);
        re_pin2 = (EditText)rootView.findViewById(R.id.re_pin2);
        re_pin3 = (EditText)rootView.findViewById(R.id.re_pin3);
        re_pin4 = (EditText)rootView.findViewById(R.id.re_pin4);

        pin1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    pin2.requestFocus();
                }else{
                    clearAllFocous();
                }
            }
        });
        pin2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    pin3.requestFocus();
                }else{
                    clearAllFocous();
                }
            }
        });
        pin3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    pin4.requestFocus();
                }else{
                    clearAllFocous();

                }
            }
        });
        pin4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    re_pin1.requestFocus();
                }else{
                    clearAllFocous();
                }
            }
        });

        re_pin1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    re_pin2.requestFocus();
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);

                }else{
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);
                }
            }
        });
        re_pin2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    re_pin3.requestFocus();
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);
                }else{
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);
                }
            }
        });
        re_pin3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                    re_pin4.requestFocus();
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);
                }else{
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);
                }
            }
        });
        re_pin4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=1){
                   // re_pin_str=re_pin_str+s.toString();
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);
                    Util.hideKeyboard(getActivity(),re_pin4);

                }else{
                    String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                    String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());
                    channgeBackGround(pin_str,re_pin_str);
                }
            }
        });





        validate_mpin = rootView.findViewById(R.id.validate_mpin);
        activity = (OnboardingActivity) getActivity();
        validate_mpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pin_str = getMpinStr(pin1.getText().toString(),pin2.getText().toString(),pin3.getText().toString(),pin4.getText().toString());
                String re_pin_str = getMpinStr(re_pin1.getText().toString(),re_pin2.getText().toString(),re_pin3.getText().toString(),re_pin4.getText().toString());

                if(pin_str.isEmpty()){
                    Toast.makeText(getActivity(),"Put your M-pin here.",Toast.LENGTH_SHORT).show();
                }
                if(!pin_str.equalsIgnoreCase(re_pin_str)){
                    Toast.makeText(getActivity(),"M-pin and Re entered M-pin doesn't match",Toast.LENGTH_SHORT).show();

                }
                else{
                    Constants.VERIFIED_USER_MPIN = pin_str;
                    log_img.setVisibility(View.GONE);
                    mCheckView.check();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            activity.mpinVerifiedUpdate(true);
                        }
                    }, 500);


                    mCheckView.check();

                }
            }
        });



        return rootView;

    }



    private  String getMpinStr(String stm1, String stm2, String stm3, String stm4){
        String mpinStr = stm1+stm2+stm3+stm4;

        return mpinStr;


    }
    public void channgeBackGround(String pin_str,String re_pin_str){
        if(pin_str.equalsIgnoreCase(re_pin_str)){
            pin1.setBackgroundResource(R.drawable.mpin_success_edit_bg);
            pin2.setBackgroundResource(R.drawable.mpin_success_edit_bg);
            pin3.setBackgroundResource(R.drawable.mpin_success_edit_bg);
            pin4.setBackgroundResource(R.drawable.mpin_success_edit_bg);
            re_pin1.setBackgroundResource(R.drawable.mpin_success_edit_bg);
            re_pin2.setBackgroundResource(R.drawable.mpin_success_edit_bg);
            re_pin3.setBackgroundResource(R.drawable.mpin_success_edit_bg);
            re_pin4.setBackgroundResource(R.drawable.mpin_success_edit_bg);

        }else{
            pin1.setBackgroundResource(R.drawable.mpin_edit_bg);
            pin2.setBackgroundResource(R.drawable.mpin_edit_bg);
            pin3.setBackgroundResource(R.drawable.mpin_edit_bg);
            pin4.setBackgroundResource(R.drawable.mpin_edit_bg);
            re_pin1.setBackgroundResource(R.drawable.mpin_edit_bg);
            re_pin2.setBackgroundResource(R.drawable.mpin_edit_bg);
            re_pin3.setBackgroundResource(R.drawable.mpin_edit_bg);
            re_pin4.setBackgroundResource(R.drawable.mpin_edit_bg);
        }

    }
    public void clearAllFocous(){
        re_pin1.setText("");
        re_pin2.setText("");
        re_pin3.setText("");
        re_pin4.setText("");
        pin1.setBackgroundResource(R.drawable.mpin_edit_bg);
        pin2.setBackgroundResource(R.drawable.mpin_edit_bg);
        pin3.setBackgroundResource(R.drawable.mpin_edit_bg);
        pin4.setBackgroundResource(R.drawable.mpin_edit_bg);
        re_pin1.setBackgroundResource(R.drawable.mpin_edit_bg);
        re_pin2.setBackgroundResource(R.drawable.mpin_edit_bg);
        re_pin3.setBackgroundResource(R.drawable.mpin_edit_bg);
        re_pin4.setBackgroundResource(R.drawable.mpin_edit_bg);
    }

}
