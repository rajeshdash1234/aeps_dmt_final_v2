package com.iserveu.aeps.onboarding;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.iserveu.aeps.R;
import com.iserveu.aeps.VideoCompressor.VideoCompress;
import com.iserveu.aeps.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import cdflynn.android.library.checkview.CheckView;

import static android.app.Activity.RESULT_OK;

public class VideoVerificationFragment extends Fragment {

    private Button validate_user;
    private LinearLayout ll_loc_vid_upload;
    OnboardingActivity activity;
    CheckView mCheckView;
    Boolean uploadVideoFlag=false;
    String username ="Uttam";
    final int VIDEO = 1;
    ProgressBar api_progress;
    ImageView log_img;
    private static final int REQUEST_CAMERA_PERMISSIONS = 931;
    ProgressDialog progressDialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_video_verification, container, false);
        mCheckView = (CheckView)rootView.findViewById(R.id.check);
        log_img = rootView.findViewById(R.id.log_img);
        ll_loc_vid_upload = (LinearLayout)rootView.findViewById(R.id.ll_loc_vid_upload);
        validate_user = (Button)rootView.findViewById(R.id.validate_user);
        api_progress = (ProgressBar)rootView.findViewById(R.id.api_progress);
        activity = (OnboardingActivity) getActivity();

        ll_loc_vid_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadVideoFlag=true;
                checkPermission();

            }
        });

        validate_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uploadVideoFlag){
                   // mCheckView.check();
                    //activity.videoVerifiedUpdate(true);
                    api_progress.setVisibility(View.VISIBLE);
                    ValidUserVideoVerified();
                }else{
                    Toast.makeText(getActivity(),"Video is not uploaded successfully.Please try again.",Toast.LENGTH_SHORT).show();
                }
            }
        });


        return rootView;

    }

    public void checkPermission()
    {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
               // ActivityCompat.requestPermissions(getActivity(), permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CAMERA_PERMISSIONS);
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSIONS);

            } else  openVideoCamera();
        } else {
            openVideoCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            openVideoCamera();
        }
    }
    public void openVideoCamera(){


        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,10);
        intent.putExtra("android.intent.extras.CAMERA_FACING",1);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
        startActivityForResult(intent, VIDEO );
        //finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)     {
        if (resultCode == RESULT_OK && requestCode == VIDEO) {


            try {
                File newfile,outfile;

                AssetFileDescriptor videoAsset = getActivity().getContentResolver().openAssetFileDescriptor(data.getData(), "r");
                FileInputStream in = videoAsset.createInputStream();

                File filepath = Environment.getExternalStorageDirectory();

                System.out.println(filepath);


                File dir1 = new File(filepath.getAbsolutePath() + "/" +"AEPS_OUTPUT" + "/");
                if (!dir1.exists()) {
                    dir1.mkdirs();
                }

                outfile = new File(dir1, "save_"+System.currentTimeMillis()+".mp4");



                File dir = new File(filepath.getAbsolutePath() + "/" +"AEPS" + "/");
                if (!dir.exists()) {
                    dir.mkdirs();
                }

                newfile = new File(dir, "save_"+System.currentTimeMillis()+".mp4");

                if (newfile.exists()) newfile.delete();

                OutputStream out = new FileOutputStream(newfile);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();

                Log.v("", "Copy file successful.");

                videoCompress(newfile,outfile);
            } catch (Exception e) {
                e.printStackTrace();
            }




        }

    }


    public void videoCompress(final File inputFile,final File outputFile){
        VideoCompress.compressVideoLow(inputFile.getPath(), outputFile.getPath(), new VideoCompress.CompressListener() {
            @Override
            public void onStart() {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setCancelable(false);
                progressDialog.setTitle("Uploading");
                progressDialog.show();
               /*tv_indicator.setText("Compressing..." + "\n"
                   + "Start at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()));
               pb_compress.setVisibility(View.VISIBLE);
               startTime = System.currentTimeMillis();
               Util.writeFile(MainActivity.this, "Start at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()) + "\n");
           */}

            @Override
            public void onSuccess() {
                uploadFile(Uri.fromFile(outputFile));
              /* String previous = tv_indicator.getText().toString();
               tv_indicator.setText(previous + "\n"
                   + "Compress Success!" + "\n"
                   + "End at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()));
               pb_compress.setVisibility(View.INVISIBLE);
               endTime = System.currentTimeMillis();
               Util.writeFile(MainActivity.this, "End at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()) + "\n");
               Util.writeFile(MainActivity.this, "Total: " + ((endTime - startTime)/1000) + "s" + "\n");
               Util.writeFile(MainActivity.this);*/
            }

            @Override
            public void onFail() {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please check your internet connection and try again", Toast.LENGTH_LONG).show();

              /* tv_indicator.setText("Compress Failed!");
               pb_compress.setVisibility(View.INVISIBLE);
               endTime = System.currentTimeMillis();
               Util.writeFile(MainActivity.this, "Failed Compress!!!" + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()));
          */ }

            @Override
            public void onProgress(float percent) {
                progressDialog.setMessage("Please wait...Preparing to upload your video.");

/*
               tv_progress.setText(String.valueOf(percent) + "%");
*/
            }
        });
    }
    private void uploadFile(Uri filePath) {
        //checking if file is available

        try{


            StorageReference storageReference = FirebaseStorage.getInstance().getReference();
            if (filePath != null) {
                //displaying progress dialog while image is uploading

                //getting the storage reference
                // StorageReference sRef = storageReference.child("gs://iserveu_android_user/" + System.currentTimeMillis() + "." + getFileExtension(filePath));

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReferenceFromUrl("gs://isuaepsdev.appspot.com/");

// Create a reference to "file"
                final StorageReference mountainsRef = storageRef.child(System.currentTimeMillis() + "." + getFileExtension(filePath));


                //adding the file to reference
                mountainsRef.putFile(filePath)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                //dismissing the progress dialog
                                progressDialog.dismiss();
                                //displaying success toast
                                Toast.makeText(getActivity(), "File Uploaded ", Toast.LENGTH_LONG).show();

                                //creating the upload object to store uploaded image details

                                mountainsRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        //Log.d(TAG, "onSuccess: uri= "+ uri.toString());
                                        //Toast.makeText(getActivity(), "Url "+uri.toString(), Toast.LENGTH_LONG).show();
                                        Constants.VERIFIED_USER_VIDEO = uri.toString();

                                    }
                                });
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                //displaying the upload progress
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                            }
                        });
            } else {
                //display an error if no file is selected
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }




    private void ValidUserVideoVerified() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("userName", Constants.VERIFIED_USERID);
            obj.put("mpin", Constants.VERIFIED_USER_MPIN);
            obj.put("password", Constants.VERIFIED_USER_PWD);
            obj.put("profileVideo", Constants.VERIFIED_USER_VIDEO);
            AndroidNetworking.post("http://35.200.175.157:8080/apitest/api/updateUser")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                api_progress.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                if(obj.getString("status").equalsIgnoreCase("0")){

                                    System.out.println(">>>>-----" + response.toString());

                                    log_img.setVisibility(View.GONE);
                                    mCheckView.check();
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            activity.videoVerifiedUpdate(true);
                                        }
                                    }, 500);

                                }else{

                                    Toast.makeText(getActivity(),obj.getString("statusDesc"),Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("...>>>>Error");

                        }
                    });
        }catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
