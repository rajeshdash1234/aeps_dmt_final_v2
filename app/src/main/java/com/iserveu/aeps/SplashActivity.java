package com.iserveu.aeps;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.utils.ForceUpdateAsync;
import com.iserveu.aeps.utils.Session;

import java.util.HashMap;


public class SplashActivity extends AppCompatActivity {

    private UsbDevice usbDevice;
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String morphodeviceid="SAGEM SA";
    String mantradeviceid="MANTRA";
    String morphoe2device="Morpho";
    boolean tag = false;
    UsbManager musbManager;
    AlertDialog alertDialog;
    Boolean navigationflag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_splash );
    }

/*
* Usb Device Check
*
*/

    private void devicecheck() {

        if(usbDevice == null) {
            deviceConnectMessgae ();
        }else {
            if (deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                installcheck ();
            } else if (deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )|| deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                morphoinstallcheck ();
            }
        }
    }
    private void deviceConnectMessgae (){

        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialogBuilder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    alertDialogBuilder = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    alertDialogBuilder = new AlertDialog.Builder(SplashActivity.this);
                }
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setTitle(getResources().getString(R.string.device_connect))
                        .setMessage(getResources().getString(R.string.decive_please_connect))
                        .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                                finish ();
                            }
                        });

                alertDialog = alertDialogBuilder.create();
                alertDialog.show ();
            }
        });
    }
  /*  private void deviceNotConnectMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(SplashActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.fail_error))
                .setMessage(getResources().getString(R.string.decive_unsupport))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish ();
                    }
                })
                .show();

    }*/

    private void morphoMessage(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(SplashActivity.this);
                }
                builder.setCancelable(false);
                builder.setTitle(getResources().getString(R.string.morpho))
                        .setMessage(getResources().getString(R.string.install_morpho_message))
                        .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                                try {

                                    startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (ActivityNotFoundException anfe) {
                                    startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                            }
                        })
                        .show();
            }
        });
    }

    public void openAppRating() {
        String appId = "com.mantra.clientmanagement";
        Uri uri = Uri.parse("market://details?id=" + appId);
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }
    private void mantraMessage(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(SplashActivity.this);
                }
                builder.setCancelable(false);
                builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                        .setMessage(getResources().getString(R.string.mantra))
                        .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            /* final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                             try {
                                 Toast.makeText ( SplashActivity.this, "package"+appPackageName, Toast.LENGTH_SHORT ).show ();
                                 startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                             } catch (ActivityNotFoundException anfe) {
                                 startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                             }*/
                                openAppRating ();
                            }
                        })
               /* .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(in);
                        finish();

                    }
                })*/
                        .show();
            }
        });


    }

    private void rdserviceMessage(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(SplashActivity.this);
                }
                builder.setCancelable(false);
                builder.setTitle(getResources().getString(R.string.mantra_install))
                        .setMessage(getResources().getString(R.string.mantra_rd_service))
                        .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                                try {
                                    startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (ActivityNotFoundException anfe) {
                                    startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                            }
                        })
               /* .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(in);
                        finish();
                    }
                })*/
                        .show();            }
        });

    }
    private  void installcheck(){

        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        tag = false;
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");
        if(isAppInstalled) {
// This intent will help you to launch if the package is already installed
            if (serviceAppInstalled){
                Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(in);
                finish();
            }else{
                rdserviceMessage ();
            }
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage ();
            mantraMessage ();
        }
    }

    private  void morphoinstallcheck(){

        boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");

        tag = true;
        if(isAppInstalled) {
//This intent will help you to launch if the package is already installed
            Intent in = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(in);
            finish();

        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            morphoMessage ();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        final Session session = new Session(SplashActivity.this);
        session.clear();
        //============================================================================================
//        AppController.getInstance().clearApplicationData();
        if (alertDialog!=null){
            alertDialog.dismiss ();
            alertDialog =null;
        }
        forceUpdate();
    }

    public void forceUpdate(){
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;

        try {
            packageInfo =  packageManager.getPackageInfo(getPackageName(),0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String currentVersion = packageInfo.versionName;

        new ForceUpdateAsync(currentVersion, SplashActivity.this, new ForceUpdateAsync.AsyncListener() {
            @Override
            public void doStuff(String latestVersion, String currentVersion) {
                if (latestVersion != null) {
                    if (!currentVersion.equalsIgnoreCase ( latestVersion )) {

                        showForceUpdateDialog ( latestVersion );

                    } else {

                        if(navigationflag==false) {
                            Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(in);
                            finish();
                            navigationflag = true;
                       /* musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

                        updateDeviceList ();*/
                        }
                    }
                } else {

                    if(navigationflag==false) {

                        Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(in);
                        finish();
                        navigationflag = true;
                    }
                   /* musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);


                    updateDeviceList ();*/
                }
            }
        }).execute();
    }
    public void showForceUpdateDialog(final String latestVersion){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alertbuilderupdate;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    alertbuilderupdate = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    alertbuilderupdate = new AlertDialog.Builder(SplashActivity.this);
                }
                alertbuilderupdate.setCancelable(false);
                // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                String message = "A new version of this app is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                alertbuilderupdate.setTitle(getResources().getString(R.string.youAreNotUpdatedTitle))
                        .setMessage(message)
                        .setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                                dialog.dismiss();
                            }
                        })
                        /*.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                                dialog.dismiss();
                                updateDeviceList ();
                            }
                        })*/
                        .show();
            }
        });

    }

    private void updateDeviceList() {

        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            deviceConnectMessgae ();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(device !=null && device.getManufacturerName () != null){
                        if(device.getManufacturerName ().equalsIgnoreCase ( mantradeviceid )||device.getManufacturerName ().equalsIgnoreCase ( morphodeviceid )||device.getManufacturerName ().equalsIgnoreCase ( morphoe2device )){
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName ();
                        }
                    }
                }
                devicecheck ();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy ();
       // updateDeviceList ();

    }


    private boolean appInstalledOrNot(String uri) {

        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

}




