package com.iserveu.aeps.refund;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.utils.AEPSAPIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_MICRO_ATM_REFUND_URL;


/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class RefundPresenter implements RefundContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private RefundContract.View refundContractView;
    private AEPSAPIService aepsapiService;
    /**
     * Initialize LoginPresenter
     */
    public RefundPresenter(RefundContract.View refundContractView) {
        this.refundContractView = refundContractView;
    }




    @Override
    public void performRefund(final String token,final RefundRequestModel refundRequestModel) {
        if (refundRequestModel!=null && refundRequestModel.getTransactionId() !=null && !refundRequestModel.getTransactionId().matches("") &&
                refundRequestModel.getFreshnessFactor() !=null && !refundRequestModel.getFreshnessFactor().matches("")
                ) {
            refundContractView.showLoader();

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }

            // this.aepsapiService = new AEPSAPIService();
            AndroidNetworking.get(GET_MICRO_ATM_REFUND_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            encryptRefund(token,refundRequestModel,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        } else {
            refundContractView.checkEmptyFields();
        }
    }

    public void encryptRefund(String token, RefundRequestModel refundRequestModel, String encodedUrl){
        RefundAPI refundAPI =
            this.aepsapiService.getClient().create(RefundAPI.class);

        refundAPI.checkRefund(token,refundRequestModel,encodedUrl).enqueue(new Callback<RefundResponse>() {
            @Override
            public void onResponse(Call<RefundResponse> call, Response<RefundResponse> response) {

                if(response.isSuccessful()) {
                    // String message = "";
                    if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {
                        //message = "Login Successful";
                        Log.v("laxmi","hf"+response.body().getStatus());
                        Log.v("laxmi","hf"+response.body().getApiComment());
                        refundContractView.hideLoader();
                        refundContractView.checkRefundStatus(response.body().getStatus(), response.body().getStatusDesc());

                    }else{
                        refundContractView.hideLoader();
                        refundContractView.checkRefundStatus("", "Refund Failed");

                    }

                }else{
                    refundContractView.hideLoader();
                    refundContractView.checkRefundStatus("", "Refund Failed");

                }
            }

            @Override
            public void onFailure(Call<RefundResponse> call, Throwable t) {
                refundContractView.hideLoader();
                refundContractView.checkRefundStatus("", "Refund Failed");
            }
        });
    }
}
