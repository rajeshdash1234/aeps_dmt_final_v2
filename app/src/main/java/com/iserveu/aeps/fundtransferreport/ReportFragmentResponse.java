package com.iserveu.aeps.fundtransferreport;

import java.util.ArrayList;

public class ReportFragmentResponse {

    @Override
    public String toString() {
        return "ReportFragmentResponse{" +
                "finoTransactionReports=" + finoTransactionReports +
                '}';
    }

    private ArrayList <FinoTransactionReports> finoTransactionReports;

    public ArrayList <FinoTransactionReports> getFinoTransactionReports ()
    {
        return finoTransactionReports;
    }

    public void setFinoTransactionReports (ArrayList <FinoTransactionReports> finoTransactionReports)
    {
        this.finoTransactionReports = finoTransactionReports;
    }



}
