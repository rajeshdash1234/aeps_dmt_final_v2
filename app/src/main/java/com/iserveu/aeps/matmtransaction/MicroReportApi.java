package com.iserveu.aeps.matmtransaction;

import com.iserveu.aeps.microatm.MicroAtmRequestModel;
import com.iserveu.aeps.microatm.MicroAtmResponse;
import com.iserveu.aeps.transaction.ReportRequest;
import com.iserveu.aeps.transaction.ReportResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface MicroReportApi {
   //transactiondetails
    @POST()
    Call<MicroReportResponse> insertUser(@Header("Authorization") String token, @Body MicroReportRequest body, @Url String url);

}
