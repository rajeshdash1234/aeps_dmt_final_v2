package com.iserveu.aeps.matmtransaction;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.utils.AEPSAPIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_MICRO_ATM_REPORT_URL;
import static com.iserveu.aeps.BuildConfig.GET_TRANSACTION_REPORT_URL;


public class MicroReportPresenter implements MicroReportContract.UserActionsListener {

    /**
     * Initialize ReportContractView
     */
    private MicroReportContract.View microReportContractView;
    private AEPSAPIService aepsapiService;
    private ArrayList<MicroReportModel> microReportModelArrayList ;
    /**
     * Initialize ReportPresenter
     */
    public MicroReportPresenter(MicroReportContract.View microReportContractView) {
        this.microReportContractView = microReportContractView;
    }

    @Override
    public void loadReports(final String fromDate, final String toDate, final String token, final String transactionType) {
        if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("") ) {
            microReportContractView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            AndroidNetworking.get(GET_TRANSACTION_REPORT_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            encyptedMicroReport(fromDate,toDate,token,transactionType,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


        } else {
            microReportContractView.emptyDates();
        }
    }

    public void encyptedMicroReport(String fromDate, String toDate, String token, String transactionType, String encodedUrl){
        MicroReportApi reportAPI =
            this.aepsapiService.getClient().create(MicroReportApi.class);

        Call<MicroReportResponse> call = reportAPI.insertUser(token,new MicroReportRequest(fromDate,toDate,transactionType),encodedUrl);

        call.enqueue(new Callback<MicroReportResponse>() {
            @Override
            public void onResponse(Call<MicroReportResponse> call, Response<MicroReportResponse> response) {
                if(response.isSuccessful()) {

                    MicroReportResponse reportResponse = response.body();
                    if (reportResponse != null && reportResponse.getmATMTransactionReport() != null) {
                        ArrayList<MicroReportModel> result = reportResponse.getmATMTransactionReport();
                        double totalAmount = 0;
                        for(int i = 0; i<result.size(); i++) {
                            totalAmount += Double.parseDouble(result.get(i).getAmountTransacted());
                        }
                        microReportContractView.reportsReady(result, String.valueOf(totalAmount));
                    }
                }
                microReportContractView.hideLoader();
                microReportContractView.showReports();
            }

            @Override
            public void onFailure(Call<MicroReportResponse> call, Throwable t) {
                microReportContractView.hideLoader();
                microReportContractView.showReports();
            }
        });
    }

    @Override
    public void refreshReports(final String token, final String amount,final String transactionType, final String transactionMode,  final String clientUniqueId) {
        microReportContractView.showLoader();
        if(amount == null || amount.matches("")){
            microReportContractView.checkAmount("1");
            return;
        }
        if(transactionMode ==null || transactionMode.matches("")){
            microReportContractView.checkTransactionMode("1");
            return;
        }
        if(transactionType == null || transactionType.matches("")){
            microReportContractView.checkTransactionType("1");
            return;
        }
        if(clientUniqueId == null || clientUniqueId.matches("")){
            microReportContractView.checkClientId("1");
            return;
        }

        AndroidNetworking.get(GET_MICRO_ATM_REPORT_URL)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject obj = new JSONObject(response.toString());
                        String key = obj.getString("hello");
                        System.out.println(">>>>-----"+key);
                        byte[] data = Base64.decode(key,Base64.DEFAULT);
                        String encodedUrl = new String(data, "UTF-8");

                        encryptLoadReport(token,amount,transactionType,transactionMode,clientUniqueId,encodedUrl);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onError(ANError anError) {

                }
            });

    }


    /**
     *  load Reports of  ReportActivity
     */

    public void encryptLoadReport(String token, final String amount, final String transactionType, final String transactionMode, final String clientUniqueId,String encodedUrl){
        RefreshApi reportAPI =
            this.aepsapiService.getClient().create(RefreshApi.class);

        Call<RefreshModel> call = reportAPI.insertUser(token,new RefreshRequest(amount,transactionType,transactionMode,clientUniqueId),encodedUrl);

        call.enqueue(new Callback<RefreshModel>() {
            @Override
            public void onResponse(Call<RefreshModel> call, Response<RefreshModel> response) {

                if(response.isSuccessful()){
                    RefreshModel refreshModel = response.body();
                    if (refreshModel != null) {
                        microReportContractView.hideLoader();
                        microReportContractView.refreshDone(refreshModel);
                    }
                }else{
                    microReportContractView.hideLoader();
                    microReportContractView.emptyRefreshData("1");
                }
            }

            @Override
            public void onFailure(Call<RefreshModel> call, Throwable t) {
                microReportContractView.hideLoader();
                microReportContractView.emptyRefreshData("1");
            }
        });
    }

}
