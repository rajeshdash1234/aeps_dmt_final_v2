package com.iserveu.aeps.matmtransaction;

import com.iserveu.aeps.transaction.ReportModel;

import java.util.ArrayList;

public class MicroReportContract {

    public interface View {

        /**
         * showReports() showReports on ReportActivity
         */
        void reportsReady(ArrayList<MicroReportModel> reportModelArrayList, String totalAmount);
        void refreshDone(RefreshModel refreshModel);
        void showReports();
        void showLoader();
        void hideLoader();
        void emptyDates();
        void checkAmount(String status);
        void checkTransactionMode(String status);
        void checkTransactionType(String status);
        void checkClientId(String status);
        void emptyRefreshData(String status);


    }

    /**
     * UserActionsListener interface checks the load of Reports
     */
    interface UserActionsListener {
        void loadReports(String fromDate, String toDate, String token, String transactionType);
        void refreshReports(String token, String amount, String transactionType, String transactionMode, String clientUniqueId);
    }

}
