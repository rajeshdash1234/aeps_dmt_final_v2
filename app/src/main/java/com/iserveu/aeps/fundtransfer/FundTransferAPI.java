package com.iserveu.aeps.fundtransfer;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface FundTransferAPI {

//    FundTransferRequest mobNumber = new FundTransferRequest();
//    String no = "9040845440";



    @POST()
    // @FormUrlEncoded
    @Headers("Content-Type: application/json")


    Call<FundTransferResponse> getStatus(@Header("Authorization") String token, @Url String url);

}
