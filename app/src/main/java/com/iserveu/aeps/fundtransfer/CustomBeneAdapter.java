package com.iserveu.aeps.fundtransfer;

import android.content.Context;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static com.iserveu.aeps.BuildConfig.DELETE_BENE_DMT;

public class CustomBeneAdapter extends BaseAdapter {
    Context context;

    ArrayList<String> beneName;
    ArrayList<String> beneAccount;
    ArrayList<String> beneBank;
    ArrayList<String> beneId;
    String custtomerNo;
    LayoutInflater inflter;
    FundTransfer activity;
    String beneIdStr = "";

    public CustomBeneAdapter(FundTransfer activity, ArrayList<String> beneName, ArrayList<String> beneAccount, ArrayList<String> beneBank, ArrayList<String> beneId, String custtomerNo) {
        this.context = activity;
        this.beneName = beneName;
        this.beneAccount = beneAccount;
        this.beneBank = beneBank;
        this.beneId = beneId;
        this.custtomerNo = custtomerNo;
        inflter = (LayoutInflater.from(context));
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return beneAccount.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.bene_dialog, null);
        ImageView deleteView = (ImageView) view.findViewById(R.id.deleteView);
        TextView beneNameTxt = (TextView) view.findViewById(R.id.beneNameTxt);
        TextView beneAccNameTxt = (TextView) view.findViewById(R.id.beneAccNameTxt);
        TextView beneBankname = (TextView) view.findViewById(R.id.beneBankname);
        View lineView = (View) view.findViewById(R.id.lineView);
//        icon.setImageResource(flags[i]);
        if (i == 0) {
            deleteView.setVisibility(View.GONE);
        }
        beneNameTxt.setText(beneName.get(i));
        beneAccNameTxt.setText(beneAccount.get(i));
        beneBankname.setText(beneBank.get(i));

        if (beneId.size() > 0) {

        }

        beneIdStr = beneId.get(i);
        System.out.println(">>>>>---->>>" + beneIdStr);

        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (i != 0) {

                    CallDeleteAPI(v, custtomerNo, beneId.get(i));

                    System.out.println(">>>>>--inner-->>>" + beneId.get(i));
                }


            }
        });


        return view;
    }

    private void CallDeleteAPI(View v, final String custtomerNoo, final String beneIdStr) {
        AndroidNetworking.get(DELETE_BENE_DMT)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            System.out.println(">>>>-----" + encodedUrl);
                            enCriptedDeleteBene(custtomerNoo, beneIdStr, encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    //-------------------------------------
    private void enCriptedDeleteBene(String custtomerNo, String beneIdStr, String encodedUrl) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("beneId", beneIdStr);
            obj.put("customerNumber", custtomerNo);


            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .setContentType("application/json; charset=utf-8")
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                //String key = obj.getString("hello");
                                System.out.println(">>>>-----" + response.toString());
                                activity.UpdateList();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


