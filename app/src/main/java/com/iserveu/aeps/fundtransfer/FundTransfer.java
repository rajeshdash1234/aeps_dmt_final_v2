package com.iserveu.aeps.fundtransfer;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.iserveu.aeps.R;
import com.iserveu.aeps.aeps2.Aeps2TransactionStatusActivity;
import com.iserveu.aeps.bluetooth.BluetoothController;
import com.iserveu.aeps.bluetooth.BluetoothPrinter;
import com.iserveu.aeps.fundtransferreport.MyErrorMessage;
import com.iserveu.aeps.settings.MainActivity;
import com.iserveu.aeps.settings.SettingsActivity;
import com.iserveu.aeps.utils.APIFundTransferService;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.CustomSpinner;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.SharePreferenceClass;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.iserveu.aeps.BuildConfig.ADD_AND_PAY;
import static com.iserveu.aeps.BuildConfig.ADD_BENE_URL;
import static com.iserveu.aeps.BuildConfig.ADD_CUSTOMER;
import static com.iserveu.aeps.BuildConfig.BANK_IFSC;
import static com.iserveu.aeps.BuildConfig.BULK_TRANSFER;
import static com.iserveu.aeps.BuildConfig.RESEND_OTP;
import static com.iserveu.aeps.BuildConfig.TRANSACTION;
import static com.iserveu.aeps.BuildConfig.VERIFY_BENE_URL;
import static com.iserveu.aeps.BuildConfig.VERIFY_OTP;
import static com.iserveu.aeps.BuildConfig.WALLET2_TRANSACTION_API;


public class FundTransfer extends AppCompatActivity implements FundTransferContract.View{

    EditText fundTransferMobileNo,findIfscCode,enterBeneName,enterAmount,fundtransferCustomerName;
    RelativeLayout wallet1,wallet2,wallet3,addBenificiaryRelativelayout,selectBenificiaryAmountRelativelayout,addedBeneDetailsRelative,enrollRelativeLayout;
    LinearLayout selectBenificiaryLinear;
    EditText enterAccountNo;
    //==============================================================================================================//
    Button submitButtonFundTransfer,findIfscButton,addBeneButton,verifyBeneButton,enrollCustomerButton,addNewBeneButton;
    TextView wallet1Balance,wallet2Balance,wallet3Balance;
    Spinner fundTransferTransactionType;
    CustomSpinner fundtransferBeneName;
    AutoCompleteTextView searchBankNameSpinner;
    String getAccountNo;
    String kkk;
    TextView account_No,bank_Name,ifsc_Code;
    int amountNumber;

    String beneid,customerIds,beneNameEnter,amount,transactionMode,pipeNo,mob,ifscCode,bankName,customerName,otp;
    String pipeNoo;
    boolean verificationflags;
    int selectBenePosition;
    LinearLayout wallet_info;

    String patternAccountNo;
    String bene_name_str="",bene_acc_no_str="",bank_name_str="",transaction_amount_str="";
    Boolean walletBooolen = false;
    Boolean walletInfoStatus= true;
    TextView w2,w3,w4,w5,w6;



    //    ArrayList<String> list=new ArrayList<String>();;
    ArrayList<String> beneName=new ArrayList<String>();
    ArrayList<String> beneName1=new ArrayList<String>();
    ArrayList<String> beneName2=new ArrayList<String>();
    ArrayList<String> beneName3=new ArrayList<String>();

    ArrayList<BeneListModel> beneNameSingle=new ArrayList<BeneListModel>();
    ArrayList<String> paymentMode=new ArrayList<String>();;
    ArrayList<String> paymentMode1=new ArrayList<String>();;
    ArrayList<String> paymentMode2=new ArrayList<String>();;
    ArrayList<BankListModel> stringArrayBankNames=new ArrayList<BankListModel>();
    private  List<IfscListModel> ifscBranch= new ArrayList<>();;
    TransactionRequest task ;
    Wallet2TransactionRequest wallet_task ;
    AddNPayRequest addNPayRequest;
    AddCustomerRequest addCustomerRequest;

    BeneListModel beneListModel;
    BeneListModel bb;

    ArrayAdapter<String> adpcc;


    BankListModel bankListModel;

    Session session;
    private FundTransferPresenter mActionsListener;

    private APIFundTransferService apiFundTransferService;

    LoadingView loadingView;

    public static final String TAG = "firebase";

    String notificationDesc,notificationTitle;
    Toolbar toolbar;
    TextView toolbar_title;
    ArrayList<TransactionDetails> transactionDetailsList;
    FundTransfer fundTransfer;
    boolean showWalletFlag=false;
    SharePreferenceClass sharePreferenceClass;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fund_transfer);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        session = new Session(FundTransfer.this);
        sharePreferenceClass = new SharePreferenceClass(FundTransfer.this);

        transactionDetailsList = new ArrayList<TransactionDetails>();

        fundTransfer =this.fundTransfer;

//        showNotification();
//===========================================================================================================
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("NotificationManagement")
                .orderBy("createdDate", Query.Direction.DESCENDING).limit(1)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.get("createdDate") != null) {
//                                cities.add(doc.getString("name"));
                                notificationTitle = String.valueOf(doc.get("header"));
                                notificationDesc = String.valueOf(doc.get("description"));
                                showNotification();
                                Log.d(TAG, "Current cites in CA: " + doc.getData());
                                Log.d(TAG, "Current cites in CA: " + notificationDesc);
                                if (doc.get("username").equals("$global")){
                                    Log.d(TAG, "Current cites in CA: " + doc.get("username"));
                                    fundTransferMobileNo.setText("");
                                }
                            }
                        }
                    }
                });


        fundTransferMobileNo = findViewById(R.id.fundtransfer_mobileno);
        wallet1 = findViewById(R.id.wallet1);
        wallet2 = findViewById(R.id.wallet2);
        wallet3 = findViewById(R.id.wallet3);
        w2 = findViewById(R.id.w2);
        w3 = findViewById(R.id.w3);
        w4 = findViewById(R.id.w4);
        w5 = findViewById(R.id.w5);
        w6 = findViewById(R.id.w6);

        selectBenificiaryLinear = findViewById(R.id.select_benificiary_linear);
        addBenificiaryRelativelayout = findViewById(R.id.add_benificiary_relativelayout);
        submitButtonFundTransfer = findViewById(R.id.submitbutton_fundtransfer);
        selectBenificiaryAmountRelativelayout = findViewById(R.id.select_benificiary_amount_relativelayout);
        wallet1Balance = findViewById(R.id.wallet1_balance);
        wallet2Balance = findViewById(R.id.wallet2_balance);
        wallet3Balance = findViewById(R.id.wallet3_balance);
        account_No = findViewById(R.id.get_account_no);
        bank_Name = findViewById(R.id.get_bank_name);
        ifsc_Code = findViewById(R.id.get_ifsc_code);
        addedBeneDetailsRelative = findViewById(R.id.added_bene_details_relative);
        searchBankNameSpinner = findViewById(R.id.search_bank_name_spinner);
        findIfscCode = findViewById(R.id.find_ifsc_code);
        enterAccountNo = findViewById(R.id.enter_account_no);
        findIfscButton = findViewById(R.id.find_ifsc_button);
        addBeneButton = findViewById(R.id.add_bene_button);
        verifyBeneButton = findViewById(R.id.verify_bene_button);
        enterBeneName = findViewById(R.id.enter_bene_name_et);
        enterAmount = findViewById(R.id.enter_amount);
        enrollRelativeLayout = findViewById(R.id.enroll_relative_layout);
        fundtransferCustomerName = findViewById(R.id.fundtransfer_customer_name);
        enrollCustomerButton = findViewById(R.id.enroll_customer_button);
        wallet_info= findViewById(R.id.wallet_info);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //=====================================================================================================//
        addNewBeneButton = findViewById(R.id.addNewBene);
//        customerName = fundtransferCustomerName.getText().toString();
        addNewBeneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                clear();

                submitButtonFundTransfer.setText("PAY");
                fundtransferBeneName.setSelection(0);
                addBenificiaryRelativelayout.setVisibility(View.VISIBLE);
                selectBenificiaryAmountRelativelayout.setVisibility(View.VISIBLE);
                submitButtonFundTransfer.setVisibility(View.VISIBLE);
                addedBeneDetailsRelative.setVisibility(GONE);

                if(walletBooolen==true){
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    submitButtonFundTransfer.setVisibility(GONE);
                }

            }
        });
        enrollCustomerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCustomer();
//                verifyOtpDialog();
            }
        });

        enterBeneName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                beneNameEnter = enterBeneName.getText().toString();
                Log.d("STATUS", "benenamecheck"+beneNameEnter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        findIfscCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ifscCode = findIfscCode.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        enterAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                amount = enterAmount.getText().toString();
                try {
                    amountNumber = Integer.parseInt(amount.trim());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                Log.d("TEST", "amountNumber: "+amountNumber);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        enterAccountNo.addTextChangedListener(new TextWatcher() {
            int length_before = 0;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                length_before = charSequence.length();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getAccountNo = enterAccountNo.getText().toString();

                if(charSequence.toString().contains("-")){
                    getAccountNo = charSequence.toString().replace("-","");
                    System.out.println(">>---->>>>"+getAccountNo);
                }
                //====================================================================================================================================================================//
                if(getCurrentFocus()==enterAccountNo) {
                    if (charSequence.length() == 0) {
                        findIfscCode.setText("");
                        enterBeneName.setText("");
                    }

                    if (selectBenePosition == 0) {
                        //ifcsFind();


                        if (!isValidAccount(getAccountNo)) {
                            enterAccountNo.setError("Invalid AccountNo");
//                    Log.d("validation", "pattern check112"+editable);
                            findIfscCode.setText("");
                        } else {
                            ifcsFind();
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(getCurrentFocus()==enterAccountNo) {
                    if (editable.toString().length() == 0) {
                        findIfscCode.setText("");
                        enterBeneName.setText("");



                    }
                }

                if (length_before < editable.length()) {
                    if (editable.length() == 4 || editable.length() == 9 ||editable.length() == 14 || editable.length() == 19)
                        editable.append("-");
                    if (editable.length() > 4) {
                        if (Character.isDigit(editable.charAt(4)))
                            editable.insert(4, "-");
                    }
                    if (editable.length() > 9) {
                        if (Character.isDigit(editable.charAt(9)))
                            editable.insert(9, "-");
                    }
                    if (editable.length() > 14) {
                        if (Character.isDigit(editable.charAt(14)))
                            editable.insert(14, "-");
                    }
                    if (editable.length() > 19) {
                        if (Character.isDigit(editable.charAt(19)))
                            editable.insert(19, "-");
                    }
                }
                if(editable.toString().contains("-")){
                    getAccountNo = editable.toString().replace("-","");
                    System.out.println(">>---->>>>"+getAccountNo);
                }


            }
        });

        searchBankNameSpinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    enterAccountNo.setText("");
                    findIfscCode.setText("");
                    enterBeneName.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        fundTransferMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length()<10) {
                    wallet1.setVisibility(GONE);
                    wallet2.setVisibility(GONE);
                    wallet3.setVisibility(GONE);
                    wallet_info.setVisibility(GONE);

                    wallet1.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    wallet1.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    wallet3.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    wallet1.setEnabled(true);
                    wallet2.setEnabled(true);
                    wallet3.setEnabled(true);
                    fundtransferCustomerName.setText("");
                    selectBenificiaryLinear.setVisibility(GONE);
                    addBenificiaryRelativelayout.setVisibility(GONE);
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    submitButtonFundTransfer.setVisibility(GONE);
                    addedBeneDetailsRelative.setVisibility(GONE);
                    enrollRelativeLayout.setVisibility(GONE);
                    fundtransferBeneName.setSelection(0);
//                    addedBeneDetailsRelative.setVisibility(View.GONE);
                    searchBankNameSpinner.setText("");
                    try {
                        enterAccountNo.setText("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    findIfscCode.setText("");
                    enterBeneName.setText("");
//                    enterAmount.setText(null);
//                    clear();
                    paymentMode.clear();
                    paymentMode1.clear();
                    paymentMode2.clear();
                    beneName.clear();
                    beneName1.clear();
                    beneName2.clear();
                    beneName3.clear();

                }else{

                    mob = fundTransferMobileNo.getText().toString();
                    mActionsListener = new FundTransferPresenter(FundTransfer.this);
                    mActionsListener.loadWallet(session.getUserToken(), mob);
                    selectBenificiaryAmountRelativelayout.setVisibility(View.GONE);

                    beneNameSingle.clear();
                    beneName.clear();
                    beneName1.clear();
                    beneName2.clear();
                    beneName3.clear();

                    beneName.add("  NONE"); //comented by rajesh
                    //============================================================================//
                    beneName1.add("");
                    beneName2.add("");
                    beneName3.add("");
                }
            }
        });

        submitButtonFundTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//=====================================================================================================================================//
                if (selectBenePosition==0){
                    if (bankName==null){
                        Toast.makeText(getApplicationContext(),"please select a bank.",Toast.LENGTH_LONG).show();
                    }else if(searchBankNameSpinner.getText().toString().isEmpty()){
                        Toast.makeText(getApplicationContext(),"please select a bank.",Toast.LENGTH_LONG).show();
                    }
                    else if (getAccountNo.isEmpty()){
                        Toast.makeText(getApplicationContext(),"Please enter account no.",Toast.LENGTH_LONG).show();
                    }else if (ifscCode.equals("no IFSC")| ifscCode.isEmpty()){
                        Toast.makeText(getApplicationContext(),"Please enter ifsc code.",Toast.LENGTH_LONG).show();
                    }else if (beneNameEnter.isEmpty()){
                        Toast.makeText(getApplicationContext(),"Please enter bene name.",Toast.LENGTH_LONG).show();
                    }else if (amount==null){
                        Toast.makeText(getApplicationContext(),"Please enter amount.",Toast.LENGTH_LONG).show();
                    }else if(enterAmount.getText().toString().isEmpty()){
                        Toast.makeText(getApplicationContext(),"Please enter amount.",Toast.LENGTH_LONG).show();

                    }else if(Integer.valueOf(enterAmount.getText().toString())<100){
                        Toast.makeText(getApplicationContext(),"The minimum amount should be Rs.100",Toast.LENGTH_LONG).show();
                    }else if(Integer.valueOf(enterAmount.getText().toString())>5000){
                        Toast.makeText(getApplicationContext(), "The amount limit should be maximum Rs.5000 for the new beneficiary account.", Toast.LENGTH_LONG).show();

                    }
                    else {transactionConformationDialog();
                    }

                }else{
                    if (amount==null){
                        Toast.makeText(getApplicationContext(),"Please enter amount",Toast.LENGTH_LONG).show();
                    }else if(enterAmount.getText().toString().isEmpty()){
                        Toast.makeText(getApplicationContext(),"Please enter amount.",Toast.LENGTH_LONG).show();

                    }else if(Integer.valueOf(enterAmount.getText().toString())<100){
                        Toast.makeText(getApplicationContext(),"The minimum amount should be Rs.100",Toast.LENGTH_LONG).show();
                    }

                    else {transactionConformationDialog();}
                }


            }
        });

//edited by rajesh
        fundtransferBeneName = findViewById(R.id.fundtransfer_bene_name);


        fundtransferBeneName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int position, long l) {
                selectBenePosition=position;
                //===============================================================================================================//


                if(adapterView!=null) {
                    ImageView deleteView = adapterView.findViewById(R.id.deleteView);

                    deleteView.setVisibility(GONE);


                    TextView beneNameTxt = adapterView.findViewById(R.id.beneNameTxt);
                    TextView beneAccNameTxt = adapterView.findViewById(R.id.beneAccNameTxt);
                    beneAccNameTxt.setVisibility(GONE);
                    TextView beneBankname = adapterView.findViewById(R.id.beneBankname);
                    beneBankname.setVisibility(GONE);
                    View lineView = adapterView.findViewById(R.id.lineView);
                    lineView.setVisibility(GONE);

                    if (position == 0) {

                        addedBeneDetailsRelative.setVisibility(GONE);
                         selectBenificiaryAmountRelativelayout.setVisibility(View.INVISIBLE);
                         submitButtonFundTransfer.setVisibility(View.INVISIBLE);
                        enterAmount.setText("");
                        beneAccNameTxt.setVisibility(GONE);
                        beneBankname.setVisibility(GONE);
                        lineView.setVisibility(GONE);


                    } else if (position > 0) {
                        enterAmount.setText("");

                        submitButtonFundTransfer.setText("QUICK PAY");

                        System.out.println("POSITION:::" + position);

                        int a = position - 1; //rajesh
                        //==================================================================================================================================
                        /*BeneListModel*/
                        bb = beneNameSingle.get(a);
//                    Log.d("ABCD","showid8877now"+bb);

                        account_No.setText(bb.getAccountNo());
                        bank_Name.setText(bb.getBankName());
                        ifsc_Code.setText(bb.getIfscCode());
                        beneid = bb.getId();

                        Log.d("transfer detail", "check response detail " + bb.getAccountNo() + bb.getBankName() + bb.getIfscCode() + bb.getId());

                        selectBenificiaryAmountRelativelayout.setVisibility(View.VISIBLE);
                        addBenificiaryRelativelayout.setVisibility(GONE);
                        submitButtonFundTransfer.setVisibility(View.VISIBLE);
                        addedBeneDetailsRelative.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //-----------Edited By rajesh

        // public void UpdateList();




        fundTransferTransactionType = findViewById(R.id.fund_transfer_transaction_type);

        fundTransferTransactionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                transactionMode=adapterView.getItemAtPosition(i).toString();
                transactionMode =  transactionMode.toLowerCase();
                ((TextView) view).setTextColor(Color.BLACK);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        try {
            JSONObject obj = new JSONObject(loadJSONFromAssetBank());
            JSONArray m_jArry = obj.getJSONArray("allbanknames");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                bankListModel = new BankListModel();
                bankListModel.setBANKNAME(jo_inside.getString("BANKNAME"));
                bankListModel.setBANKCODE(jo_inside.getString("BANKCODE"));
                bankListModel.setPATTERN(jo_inside.getString("PATTERN"));
                Log.d("BANKCODE", "bankListModeFlag102030" + bankListModel);
                try {
                    bankListModel.setFLAG(jo_inside.getString("FLAG"));
                } catch (JSONException e) {
                    Log.d("BANKCODE", "bankListModeFlag102030" + e);
                    e.printStackTrace();
                }

                stringArrayBankNames.add(bankListModel);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("BANKCODE", "bankListModel987789" + stringArrayBankNames);

        final ArrayAdapter<BankListModel> adapterBankName = new ArrayAdapter<BankListModel>(this, /*android.*/R.layout.bank_row_list_dialog_item,stringArrayBankNames );
        adapterBankName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        searchBankNameSpinner.setAdapter(adapterBankName);
        searchBankNameSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                /*String*/ bankName = adapterView.getItemAtPosition(i).toString();
                enterAccountNo.setEnabled(true);
                Log.d("Adapter View", "onItemClick002211: "+bankName);
                Log.d("Adapter View", "onItemClick885522: "+adapterBankName);
//=====================================================================================================================
                DocumentReference docRef = db.collection("BankList").document(bankName);
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//                    public static final String TAG = "firebase";

                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                if (document.get("active").equals(true)){
//                                    Toast.makeText(getApplicationContext(),"BANK IS ACTIVE",Toast.LENGTH_LONG).show();
                                    submitButtonFundTransfer.setEnabled(true);
                                    submitButtonFundTransfer.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
                                }else{
                                    Toast.makeText(getApplicationContext(),"Bank is inactive, try after sometime.",Toast.LENGTH_LONG).show();
                                    submitButtonFundTransfer.setEnabled(false);
                                    submitButtonFundTransfer.setBackgroundColor(getResources().getColor(R.color.colorGrey));
                                }
                            } else {
                                Log.d(TAG, "No such document.");
                            }
                        } else {
                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                });
//=====================================================================================================================

                try {
                    findIfscCall();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (enterAccountNo.getText().toString()!=null){

                    Toast.makeText(getApplicationContext()," Please enter account No.",Toast.LENGTH_SHORT).show();
                }

            }
        });

        findIfscButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    showCustomDialog();
                } catch (IOException e) {
                    Log.d("RESPONSE", "findIfscCall889: "+e);
                    e.printStackTrace();
                }
            }
        });

        addBeneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wallet_info.setVisibility(GONE);
                if(searchBankNameSpinner.getText().toString().isEmpty()){
                    searchBankNameSpinner.setError("please enter your bank name. ");
                }else if(enterAccountNo.getText().toString().isEmpty()){
                    enterAccountNo.setError("Please enter your account no.");
                }else if(findIfscCode.getText().toString().isEmpty()){
                    findIfscCode.setError("Please enter IFSC code.");
                }else if(enterBeneName.getText().toString().isEmpty()){
                    enterBeneName.setError("Please enter benificiary name.");
                }/*else if(enterAmount.getText().toString().isEmpty()){
                    enterAmount.setError("Please enter amount.");
                }*/else {
                    showLoader();
                    addBene();
                }


//            clear();
            }
        });

        verifyBeneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLoader();
                verifyBene();

            }
        });

//        transactionBody(beneid,customerIds,amount,transactionMode,pipeNo);

    }

    public void showNotification(){

        // define sound URI, the sound to be played when there's a notification
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification mNotification = new Notification.Builder( this)

                .setContentTitle(notificationTitle)
                .setContentText(notificationDesc)
                .setSmallIcon(R.drawable.home)
//                .setContentIntent(pIntent)
                .setSound(soundUri)

                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // If you want to hide the notification after it was selected, do the code below
        // myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, mNotification);
    }

    public void cancelNotification(int notificationId){

        if (Context.NOTIFICATION_SERVICE!=null) {
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
            nMgr.cancel(notificationId);
        }
    }

    private boolean isValidAccount(String accountPattern) {
//        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
//                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        /*String patternAccountNo;*/
        Log.d("validation", "pattern check11");
        for(BankListModel bankl:stringArrayBankNames){
            Log.d("validation", "pattern check112"+bankl.getBANKNAME());
            Log.d("validation", "pattern check1122"+bankName);
            if(bankl.getBANKNAME().equalsIgnoreCase(bankName)){
                Log.d("validation", "pattern check113");
//                        findIfscCode.setText(bankl.getBANKCODE());
                if (bankl.getPATTERN()!=null){
                    Log.d("validation", "pattern check114");
//                    if (bankl.getPATTERN().equalsIgnoreCase("PATTERN")){
                    /*String*/ patternAccountNo=bankl.getPATTERN();
                    Log.d("validation", "pattern check115"+patternAccountNo);
                    /*}*/}}}


//        String patternAccountNo = bankListModel.getPATTERN();

        Pattern pattern = Pattern.compile(patternAccountNo);
        Matcher matcher = pattern.matcher(accountPattern);
        Log.d("validation", "pattern check"+matcher);
        return matcher.matches();
    }

    private void verifyBene() {

        if (customerIds==null||getAccountNo==null||bankName==null||beneNameEnter==null||ifscCode==null){
            Toast.makeText(getApplicationContext(),"Beneficiary details field are empty",Toast.LENGTH_LONG).show();
        }else {

            if ((pipeNo.equals("3")) && verificationflags) {
                pipeNoo = "3";
            } else if ((pipeNo.equals("1")) && verificationflags) {
                pipeNoo = "1";
            } else /*if (pipeNo.equals("2"))*/ {
                pipeNoo = "22";
            }
            final VerifyBeneRequest verifyBeneRequest = new VerifyBeneRequest(customerIds, getAccountNo, bankName, beneNameEnter, ifscCode, pipeNoo);
            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(VERIFY_BENE_URL)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                    encodedUrl = encodedUrl.replace("itpl","dmt");
                                }
                                System.out.println(">>>>-----"+encodedUrl);
                                enCriptedVerifyBene(verifyBeneRequest,encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {

                        }
                    });










        }
    }
    //Edited by Rajesh
    public void enCriptedVerifyBene(VerifyBeneRequest verifyBeneRequest, String encodedUrl){

        final VerifyBeneApi verifyBeneApi = this.apiFundTransferService.getClient().create(VerifyBeneApi.class);

        verifyBeneApi.getVerifyBene(session.getUserToken(), verifyBeneRequest, encodedUrl).enqueue(new Callback<VerifyBeneResponse>() {
            @Override
            public void onResponse(Call<VerifyBeneResponse> call, Response<VerifyBeneResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("STATUS", "Transaction successful VerifyBene" + response.body());

                    Toast.makeText(getApplicationContext(),  response.body().getStatusDesc(), Toast.LENGTH_SHORT).show();
                    enterBeneName.setText(response.body().getBeneName());
                    hideLoader();
                } else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyBene" + response.body());
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<VerifyBeneResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    private void addBene() {

        final AddBeneRequest addBeneRequest = new AddBeneRequest(mob,mob,bankName,getAccountNo,beneNameEnter,ifscCode);
        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }
        AndroidNetworking.get(ADD_BENE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                encodedUrl = encodedUrl.replace("itpl","dmt");
                            }
                            System.out.println(">>>>-----"+encodedUrl);
                            encriptAddBene(addBeneRequest,apiFundTransferService,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    //Edited by Rajesh
    public void encriptAddBene(AddBeneRequest addBeneRequest, APIFundTransferService apiFundTransferService, String encodedUrl){

        final AddBeneAPI addBeneAPI = this.apiFundTransferService.getClient().create(AddBeneAPI.class);
        addBeneAPI.getAddBene(session.getUserToken(),addBeneRequest, encodedUrl).enqueue(new Callback<AddBeneResponse>() {
            @Override
            public void onResponse(Call<AddBeneResponse> call, Response<AddBeneResponse> response) {
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulAddBene"+response.body());

                    beneName.clear(); //rajesh
                    beneName1.clear();
                    beneName2.clear();
                    beneName3.clear();
                    beneNameSingle.clear();
                    paymentMode.clear();
                    paymentMode1.clear();
                    paymentMode2.clear();

                    beneName.add(" NONE");
                    beneName1.add("");
                    beneName2.add("");
                    beneName3.add("");

                    mActionsListener.loadWallet(session.getUserToken(), mob);
                    hideLoader();
                    addBenificiaryRelativelayout.setVisibility(GONE);
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    Toast.makeText(getApplicationContext(),response.body().getStatusDesc(),Toast.LENGTH_LONG).show();
                    clear();
                }else {
                    Log.d("STATUS", "TransactionFailedAddBene"+response.body());
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<AddBeneResponse> call, Throwable t) {

            }
        });


    }

    private void transactionConformationDialog() {
        final Dialog dialog = new Dialog(FundTransfer.this);
        dialog.setContentView(R.layout.transaction_conformation_dialog);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);

        beneMobileTv.setText(mob);
        transactionAmountTv.setText(amount);
        transactionTypeTv.setText(transactionMode);

        transaction_amount_str =amount;
//===============================================================================================================================================//
        if (selectBenePosition==0) {
            beneNameTv.setText(beneNameEnter);
            beneAccountTv.setText(getAccountNo);
            bankNameTv.setText(bankName);
            bene_name_str = beneNameEnter;
            bene_acc_no_str = getAccountNo;
            bank_name_str = bankName;
        }else{
            beneNameTv.setText(bb.getBeneName());
            beneAccountTv.setText(bb.getAccountNo());
            bankNameTv.setText(bb.getBankName());
            bene_name_str = bb.getBeneName();
            bene_acc_no_str = bb.getAccountNo();
            bank_name_str = bb.getBankName();
        }

        Button dialogButtonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);

        dialogButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conformTransaction();
                dialog.dismiss();
            }
        });

        Button dialogButtonNo = (Button) dialog.findViewById(R.id.dialog_button_no);

        dialogButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        hideLoader();

    }

    private void conformTransaction(){

        enterAccountNo.setEnabled(false);
        Log.d("TEST", "PipeNo: "+pipeNo);
        Log.d("TEST", "PipeNoo: "+beneid);
        Log.d("TEST", "PipeNooo: "+customerIds);
        Log.d("TEST", "PipeNoooo: "+transactionMode);
//                amount = enterAmount.getText().toString();
//                beneNameEnter = enterBeneName.getText().toString();
//                ifscCode = findIfscCode.getText().toString();
        Log.d("TEST", "PipeNooooo: "+amount);

        Log.d("TEST quick AND pay ON", "selectBenePosition"+selectBenePosition);
//===================================================================================================================================================================//

      if(pipeNo.equalsIgnoreCase("2")){

          wallet_task = new Wallet2TransactionRequest(amount, customerIds, pipeNo, beneid, transactionMode,"");

          wallet2transaction();


      }else {

          if (selectBenePosition == 0) {

              if (amountNumber <= 5000) {
                  addNPayRequest = new AddNPayRequest(amount, mob, transactionMode, beneNameEnter, ifscCode, bankName, getAccountNo, mob, pipeNo);
                  Log.d("TEST quick AND pay ON", "TransactionSuccessful0012163" + addNPayRequest);
                  Log.d("TEST quick AND pay ON", "quick AND pay ON ");
                  addNPay();
                  clear();
              } else {
                  Toast.makeText(this, "The amount limit should be maximum Rs.5000 for the new beneficiary account.", Toast.LENGTH_SHORT).show();
              }


          } else {

              task = new TransactionRequest(amount, customerIds, pipeNo, beneid, transactionMode);
              Log.d("STATUS", "Transaction0011880013" + amount + customerIds + pipeNo + beneid + transactionMode);

              Log.d("STATUS", "Transaction001188" + task);
              if (amountNumber <= 5000) {

                  transaction();
                  clear();
              } else {
                  bulkTransfer();
                  clear();
              }
          }
      }

    }

    private void verifyOtpDialog() {
        final Dialog dialog = new Dialog(FundTransfer.this);
        dialog.setContentView(R.layout.verify_otp_dialog);
        final EditText etOtpDialog = dialog.findViewById(R.id.otp_dialog);

        Button dialogButtonResend = (Button) dialog.findViewById(R.id.dialog_button_resend);
        final ProgressBar load_progress = (ProgressBar)dialog.findViewById(R.id.load_progress);

        dialogButtonResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load_progress.setVisibility(View.VISIBLE);
                resendOtp(load_progress);
//                dialog.dismiss();
            }
        });

        Button dialogButtonVerify = (Button) dialog.findViewById(R.id.dialog_button_verify);

        dialogButtonVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp=etOtpDialog.getText().toString();
                load_progress.setVisibility(GONE);
                verifyOtp();
                dialog.dismiss();
            }
        });

        dialog.show();
        hideLoader();
    }

    private void verifyOtp() {
        showLoader();
        final  VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest(otp,mob);

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }
        AndroidNetworking.get(VERIFY_OTP)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                encodedUrl = encodedUrl.replace("itpl","dmt");
                            }
                            System.out.println(">>>>-----"+encodedUrl);
                            encriptedVerifyOtp(verifyOtpRequest,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
    //Edited By Rajesh
    public void encriptedVerifyOtp(VerifyOtpRequest verifyOtpRequest, String encodedUrl){
        final VerifyOtpApi verifyOtpApi = this.apiFundTransferService.getClient().create(VerifyOtpApi.class);

        verifyOtpApi.getVerifyOtp(session.getUserToken(),verifyOtpRequest, encodedUrl).enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
//                Log.d("STATUS", "Transaction"+response.body());
//                Log.d("Test", "Transaction: "+"going inside 2255" +transactionAPI);
                if (response.isSuccessful()){
//                                        hideLoader();
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp"+response.body());
                    Toast.makeText(getApplicationContext(),response.body().getStatusDesc(),Toast.LENGTH_LONG).show();
                    hideLoader();
                    enrollRelativeLayout.setVisibility(GONE);
                    mActionsListener.loadWallet(session.getUserToken(), mob);
                }else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyOtp"+response.body());
                    Toast.makeText(getApplicationContext(),response.body().getStatusDesc(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
            }
        });

    }

    private void resendOtp(final ProgressBar load_progress) {

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("number",mob);

            AndroidNetworking.post(RESEND_OTP)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                    encodedUrl = encodedUrl.replace("itpl","dmt");
                                }
                                System.out.println(">>>>-----"+encodedUrl);
                                encriptedResendOtp(load_progress,encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //Edited By Rajesh
    public void encriptedResendOtp(final ProgressBar load_progress, String encodedUrl){

        final ResendOtpAPI resendOtpAPI = this.apiFundTransferService.getClient().create(ResendOtpAPI.class);

        resendOtpAPI.getResendOtpReport(session.getUserToken(), encodedUrl).enqueue(new Callback<ResendOtpResponse>() {
            @Override
            public void onResponse(Call<ResendOtpResponse> call, Response<ResendOtpResponse> response) {
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulResendOtp"+response.body());

                    load_progress.setVisibility(GONE);
                }else {
                    Log.d("STATUS", "TransactionFailedResendOtp"+response.body());
                    load_progress.setVisibility(GONE);
                }
            }

            @Override
            public void onFailure(Call<ResendOtpResponse> call, Throwable t) {

            }
        });


    }

    private void addCustomer() {


        customerName = fundtransferCustomerName.getText().toString();
        if(customerName != null && !customerName.isEmpty()){
            showLoader();
            addCustomerRequest = new AddCustomerRequest(customerName, mob);

            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(ADD_CUSTOMER)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                    encodedUrl = encodedUrl.replace("itpl","dmt");
                                }
                                System.out.println(">>>>-----"+encodedUrl);
                                encriptedAccCustomer(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }else {
            Log.d("STATUS", "TransactionSuccesscustomername1" + customerName);
            Toast.makeText(getApplicationContext(),"Please enter customer name",Toast.LENGTH_LONG).show();
        }

    }
    //Edited By Rajesh
    public void encriptedAccCustomer(String encodedUrl){
        final AddCustomerAPI addCustomerAPI = this.apiFundTransferService.getClient().create(AddCustomerAPI.class);

        addCustomerAPI.getAddCustomer(session.getUserToken(), addCustomerRequest, encodedUrl).enqueue(new Callback<AddCustomerResponse>() {
            @Override
            public void onResponse(Call<AddCustomerResponse> call, Response<AddCustomerResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("STATUS", "TransactionSuccessfulAddCustomer" + response.body());
                    verifyOtpDialog();
                    hideLoader();
                } else {
                    Log.d("STATUS", "TransactionFailedAddCustomer" + response.body());
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<AddCustomerResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }



    private void addNPay() {
        showLoader();

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }
        AndroidNetworking.get(ADD_AND_PAY)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                encodedUrl = encodedUrl.replace("itpl","dmt");
                            }
                            System.out.println(">>>>-----"+encodedUrl);
                            encriptedAddAndPay(encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
    //Edited By Rajesh
    public void encriptedAddAndPay(String encodedUrl){
        AddNPayAPI addNPayAPI = this.apiFundTransferService.getClient().create(AddNPayAPI.class);
        addNPayAPI.getAddNPayReport(session.getUserToken(),addNPayRequest, encodedUrl).enqueue(new Callback<AddNPayResponse>() {
            @Override
            public void onResponse(Call<AddNPayResponse> call, Response<AddNPayResponse> response) {
                if (response.isSuccessful()){
                    hideLoader();
                    Log.d("STATUS", "TransactionSuccessfulAddNPAY"+response.body());
                    addBenificiaryRelativelayout.setVisibility(GONE);
                    //=================================================================================================================23691,23566
                    paymentMode.clear();
                    paymentMode1.clear();
                    paymentMode2.clear();
                    beneName.clear();
                    beneName1.clear();
                    beneName2.clear();
                    beneName3.clear();
                    mActionsListener.loadWallet(session.getUserToken(), mob);
                    beneName.add("  NONE");
                    //=======================================================================================================//
                    beneName1.add("");
                    beneName2.add("");
                    beneName3.add("");

                    transactionDetailsList = new ArrayList<TransactionDetails>();
                    TransactionDetails transactionDetails = new TransactionDetails();
                    transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                    transactionDetails.setTracking_no(response.body().gettxnId());
                    transactionDetails.setTransaction_type(transactionMode);
                    transactionDetailsList.add(transactionDetails);
                    transactionSuccessDialog(response.body().gettxnId(), response.body().getstatusDesc(), transactionDetailsList);

                }else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedAddNPAY"+response.body());
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    showPaymentFailedDialog(message.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddNPayResponse> call, Throwable t) {

            }
        });
    }


    private void bulkTransfer() {
        showLoader();
        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }
        AndroidNetworking.get(BULK_TRANSFER)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                encodedUrl = encodedUrl.replace("itpl","dmt");
                            }
                            System.out.println(">>>>-----"+encodedUrl);
                            encriptedBulkTransfer(encodedUrl);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
    //Edited By Rajesh
    public void encriptedBulkTransfer(String encodedUrl){
        BulkTransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(BulkTransactionAPI.class);
        transactionAPI.getTransectionReport(session.getUserToken(),task, encodedUrl).enqueue(new Callback<BulkTransactionResponse>() {
            @Override
            public void onResponse(Call<BulkTransactionResponse> call, Response<BulkTransactionResponse> response) {
                if(response.body().getTransactionResponse()!=null) {

                    if (response.isSuccessful()) {
                        hideLoader();


                        Log.d("STATUS", "TransactionSuccessful" + response.body());
                        //=================================================================================================================23691,23566
                        paymentMode.clear();
                        paymentMode1.clear();
                        paymentMode2.clear();
                        beneName.clear();
                        beneName1.clear();
                        beneName2.clear();
                        beneName3.clear();
                        mActionsListener.loadWallet(session.getUserToken(), mob);
                        beneName.add("  NONE");
                        //============================================================================================================//
                        beneName1.add("");
                        beneName2.add("");
                        beneName3.add("");
                        ArrayList<TransactionResponse> BulkList = response.body().getTransactionResponse();
                        String txnId = "";
                        transactionDetailsList = new ArrayList<TransactionDetails>();
                        int amountTemp = amountNumber;

                        for (int i = 0; i < BulkList.size(); i++) {
                            TransactionDetails transactionDetails = new TransactionDetails();
                            transactionDetails.setTracking_no(BulkList.get(i).getTxnId());
                            transactionDetails.setTransaction_type(transactionMode);
                            if(amountTemp>5000){
                                amountTemp = amountTemp-5000;
                                transactionDetails.setTransaction_amount("5000");
                            }else{
                                transactionDetails.setTransaction_amount(String.valueOf(amountTemp));
                            }

                            transactionDetailsList.add(transactionDetails);

                            if (i == 0) {
                                txnId = BulkList.get(i).getTxnId();
                            } else {
                                txnId = txnId + "," + BulkList.get(i).getTxnId();
                            }
                        }

                        transactionSuccessDialog(txnId, BulkList.get(1).getStatusDesc(),transactionDetailsList);
                    } else {
                        hideLoader();
                        Gson gson = new Gson();
                        MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                        //transaction failed
                        showPaymentFailedDialog(message.getMessage());

                    }
                }else{
                    hideLoader();
                    showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");
                }
            }

            @Override
            public void onFailure(Call<BulkTransactionResponse> call, Throwable t) {
                hideLoader();
                showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");


            }
        });
    }

    private void wallet2transaction() {
        showLoader();
        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }
        AndroidNetworking.get(WALLET2_TRANSACTION_API)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                encodedUrl = encodedUrl.replace("itpl","dmt");
                            }
                            System.out.println(">>>>-----"+encodedUrl);
                            wallet2encriptedTransaction(encodedUrl);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    public void wallet2encriptedTransaction(String encodedUrl){

        final Wallet2TransactionAPI wallet2TransactionAPI = this.apiFundTransferService.getClient().create(Wallet2TransactionAPI.class);
        wallet2TransactionAPI.getTransectionReport(session.getUserToken(),wallet_task, encodedUrl).enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                if (response.isSuccessful()){
                    hideLoader();
                    //=================================================================================================================23691,23566
                    paymentMode.clear();
                    paymentMode1.clear();
                    paymentMode2.clear();
                    beneName.clear();
                    beneName1.clear();
                    beneName2.clear();
                    beneName3.clear();
                    mActionsListener.loadWallet(session.getUserToken(), mob);
                    beneName.add("  NONE");
                    //==============================================================================//
                    beneName1.add("");
                    beneName2.add("");
                    beneName3.add("");
                    Log.d("STATUS", "TransactionSuccessful"+response.body());
                    // wallet_info.setVisibility(GONE);

                    transactionDetailsList = new ArrayList<TransactionDetails>();
                    TransactionDetails transactionDetails = new TransactionDetails();
                    transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                    transactionDetails.setTracking_no(response.body().getTxnId());
                    transactionDetails.setTransaction_type(transactionMode);
                    transactionDetailsList.add(transactionDetails);

                    transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);


                }else {
                    hideLoader();
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    showPaymentFailedDialog(message.getMessage());
                }
            }
            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {

            }
        });

    }

    private void transaction() {
        showLoader();
        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }
        AndroidNetworking.get(TRANSACTION)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if(encodedUrl.contains("https://itpl.iserveu.tech")){
                                encodedUrl = encodedUrl.replace("itpl","dmt");
                            }
                            System.out.println(">>>>-----"+encodedUrl);
                            encriptedTransaction(encodedUrl);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
    //Edited By Rajesh
    public void encriptedTransaction(String encodedUrl){

        final TransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(TransactionAPI.class);
        transactionAPI.getTransectionReport(session.getUserToken(),task, encodedUrl).enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                if (response.isSuccessful()){
                    hideLoader();
                    //=================================================================================================================23691,23566
                    paymentMode.clear();
                    paymentMode1.clear();
                    paymentMode2.clear();
                    beneName.clear();
                    beneName1.clear();
                    beneName2.clear();
                    beneName3.clear();
                    mActionsListener.loadWallet(session.getUserToken(), mob);
                    beneName.add("  NONE");
                    //==============================================================================//
                    beneName1.add("");
                    beneName2.add("");
                    beneName3.add("");
                    Log.d("STATUS", "TransactionSuccessful"+response.body());
                   // wallet_info.setVisibility(GONE);

                    transactionDetailsList = new ArrayList<TransactionDetails>();
                    TransactionDetails transactionDetails = new TransactionDetails();
                    transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                    transactionDetails.setTracking_no(response.body().getTxnId());
                    transactionDetails.setTransaction_type(transactionMode);
                    transactionDetailsList.add(transactionDetails);

                    transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);


                }else {
                    hideLoader();
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    showPaymentFailedDialog(message.getMessage());
                }
            }
            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {

            }
        });

    }


/*
    private void transactionSuccessDialog(String txnId, String statusDesc,final  ArrayList<TransactionDetails> transactionDetailsList) {
        final Dialog dialog = new Dialog(FundTransfer.this);
        dialog.setContentView(R.layout.transaction_success_dialog);
        dialog.setCancelable(false);
        final TextView tv = dialog.findViewById(R.id.tv);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);
        final TextView trackingId = dialog.findViewById(R.id.tracking_no_tv);
        if(txnId==null ||txnId.trim().length()==0){
            tv.setBackgroundColor(getResources().getColor(R.color.red));
        }else{
            tv.setBackgroundColor(getResources().getColor(R.color.green));
        }
        tv.setText(statusDesc);
        trackingId.setText(txnId);
        beneMobileTv.setText(mob);
        transactionAmountTv.setText(transaction_amount_str);
        transactionTypeTv.setText(transactionMode);

        beneNameTv.setText(bene_name_str);
        beneAccountTv.setText(bene_acc_no_str);
        bankNameTv.setText(bank_name_str);
//===============================================================================================================================================//

        Button dialogButtonOK = (Button) dialog.findViewById(R.id.dialog_button_no);
        Button dialogbuttonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);

        dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogbuttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(FundTransfer.this,CreatePDFActivity.class);
                intent.putExtra("data", transactionDetailsList);
                intent.putExtra("acc_no", bene_acc_no_str);
                intent.putExtra("bene_name", bene_name_str);
                intent.putExtra("bank_name", bank_name_str);
                intent.putExtra("bene_mob_no", mob);

                startActivity(intent);

            }
        });

        Button dialog_button_print = dialog.findViewById(R.id.dialog_button_print);
        dialog_button_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                BluetoothDevice mBtDevice = btAdapter.getBondedDevices().iterator().next();   // Get first paired device
                final BluetoothPrinter mPrinter = new BluetoothPrinter(mBtDevice);
                mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {
                    @Override
                    public void onConnected() {
                        mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                        mPrinter.printText("Transaction Report");
                        mPrinter.addNewLine();
                        mPrinter.printText("Bene Name: "+bene_name_str);
                        mPrinter.addNewLine();
                        mPrinter.printText("Bene Account No: "+bene_acc_no_str);
                        mPrinter.addNewLine();
                        mPrinter.printText("Bank Name :"+bank_name_str);
                        mPrinter.addNewLine();
                        mPrinter.printText("Transaction Amount :"+transaction_amount_str);
                        mPrinter.addNewLine();
                        mPrinter.printText("API Tid :"+transaction_amount_str);
                        mPrinter.addNewLine();
                        mPrinter.printText("Transaction Type :"+transactionMode);

                        mPrinter.addNewLine();
                        mPrinter.addNewLine();
                        mPrinter.addNewLine();
                        mPrinter.addNewLine();
                        mPrinter.addNewLine();


                        mPrinter.finish();
                    }

                    @Override
                    public void onFailed() {
                        Log.d("BluetoothPrinter", "Conection failed");

                    }
                });
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    }
*/

    private void transactionSuccessDialog(final String txnId, String statusDesc,final  ArrayList<TransactionDetails> transactionDetailsList) {
        wallet_info.setVisibility(GONE);
        final Dialog dialog = new Dialog(FundTransfer.this);
        dialog.setContentView(R.layout.transaction_success_dialog);
        dialog.setCancelable(false);
        final TextView tv = dialog.findViewById(R.id.tv);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);
        final TextView trackingId = dialog.findViewById(R.id.tracking_no_tv);
        if(txnId==null ||txnId.trim().length()==0){
            tv.setBackgroundColor(getResources().getColor(R.color.red));
        }else{
            tv.setBackgroundColor(getResources().getColor(R.color.green));
        }
        tv.setText(statusDesc);
        trackingId.setText(txnId);
        beneMobileTv.setText(mob);
        transactionAmountTv.setText(transaction_amount_str);
        transactionTypeTv.setText(transactionMode);

        beneNameTv.setText(bene_name_str);
        beneAccountTv.setText(bene_acc_no_str);
        bankNameTv.setText(bank_name_str);
//===============================================================================================================================================//

        Button dialogButtonOK = (Button) dialog.findViewById(R.id.dialog_button_no);
        Button dialogPrint = (Button) dialog.findViewById(R.id.dialog_button_print);

        dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        Button dialogbuttonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);
        dialogbuttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.BRAND_NAME.trim().length()!=0) {
                    dialog.dismiss();
                    Intent intent = new Intent(FundTransfer.this, CreatePDFActivity.class);
                    intent.putExtra("data", transactionDetailsList);
                    intent.putExtra("acc_no", bene_acc_no_str);
                    intent.putExtra("bene_name", bene_name_str);
                    intent.putExtra("bank_name", bank_name_str);
                    intent.putExtra("bene_mob_no", mob);
                    startActivity(intent);
                }else{
                    showBrandSetAlert();
                }


            }
        });
        dialogPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Constants.BRAND_NAME.trim().length()!=0) {
                    BluetoothDevice bluetoothDevice = Constants.bluetoothDevice;
                    if (bluetoothDevice != null) {
                        callBluetoothFunction(bene_name_str, mob, bene_acc_no_str, bank_name_str, transaction_amount_str, txnId, transactionMode, bluetoothDevice);
                    } else {
                        Intent in = new Intent(FundTransfer.this, MainActivity.class);
                        startActivity(in);
                        Toast.makeText(FundTransfer.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    showBrandSetAlert();
                }

            }
        });
    }

    private void showBrandSetAlert(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(FundTransfer.this);
        builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
        builder1.setTitle("Warning!!!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "GOT IT",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void callBluetoothFunction(final String bene_name,final String bene_mob,final String bene_acc,final String bank_name,final String amount,final String tid,final String type,BluetoothDevice bluetoothDevice) {


        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(Constants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                mPrinter.printText("--------Transaction Report---------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.printText("Bene Name: "+bene_name);
                mPrinter.addNewLine();
                mPrinter.printText("Bene Mobile No.: "+bene_mob);
                mPrinter.addNewLine();
                mPrinter.printText("Bene Account No.: "+bene_acc);
                mPrinter.addNewLine();
                mPrinter.printText("Bank Name.: "+bank_name);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Amount: "+amount);
                mPrinter.addNewLine();
                mPrinter.printText("API Tid: "+tid);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Type: "+type);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(Constants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");
                Toast.makeText(FundTransfer.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });

    }


/*
    private void showPaymentSuccessDialog(TransactionResponse body) {
        final Dialog dialog = new Dialog(FundTransfer.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.transaction_successful_custom_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        Window window = dialog.getWindow();

        final TextView transaction_status = (TextView) dialog.findViewById(R.id.transaction_status);
        final TextView trandsaction_id = (TextView)dialog.findViewById(R.id.trandsaction_id);
        final Button dialogButtonConfirm = (Button)dialog.findViewById(R.id.dialogButtonConfirm);
        transaction_status.setText(body.getStatusDesc());
        trandsaction_id.setText("Transaction Id: "+body.getTxnId());
        dialogButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        */
/*AlertDialog.Builder builder1 = new AlertDialog.Builder(FundTransfer.this);
        builder1.setMessage("Transaction Id:  "+body.getTxnId() );
        builder1.setTitle(body.getStatusDesc());
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                       // onBackPressed();
                    }
                });

       *//*
     */
/* builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });*//*
     */
/*

        AlertDialog alert11 = builder1.create();
        alert11.show();*//*

    }
*/


    private void showPaymentFailedDialog(String message) {
        final Dialog dialog = new Dialog(FundTransfer.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.transaction_successful_custom_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        Window window = dialog.getWindow();

        final TextView transaction_status = (TextView) dialog.findViewById(R.id.transaction_status);
        final TextView trandsaction_id = (TextView)dialog.findViewById(R.id.trandsaction_id);
        final Button dialogButtonConfirm = (Button)dialog.findViewById(R.id.dialogButtonConfirm);
        transaction_status.setText("Transaction failed");
        transaction_status.setBackgroundColor(getResources().getColor(R.color.color_report_red));
        trandsaction_id.setText(message);
        dialogButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        /*AlertDialog.Builder builder1 = new AlertDialog.Builder(FundTransfer.this);
        builder1.setMessage("Transaction Id:  "+body.getTxnId() );
        builder1.setTitle(body.getStatusDesc());
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                       // onBackPressed();
                    }
                });

       *//* builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });*//*

        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }


    private void ifcsFind() {

        for(BankListModel bankl:stringArrayBankNames){

            if(bankl.getBANKNAME().equalsIgnoreCase(bankName)){
//                        findIfscCode.setText(bankl.getBANKCODE());
                if (bankl.getFLAG()!=null){
                    if (bankl.getFLAG().equalsIgnoreCase("u")){
                        findIfscCode.setText(bankl.getBANKCODE());
                        findIfscCode.setEnabled(false);
                    }else if (bankl.getFLAG().equalsIgnoreCase("4")){
                        String first4 = "";
                        try {

                            first4 = getAccountNo.substring(0,4);
                            // first4 = enterAccountNo.getText().toString().substring(0,4);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        Log.d("CHAR", "ACCOUNT NO :"+mString);
                        findIfscCode.setText(bankl.getBANKCODE()+first4);
                        findIfscCode.setEnabled(false);
                    }else if (bankl.getFLAG().equalsIgnoreCase("3")){

                        String first3 = "";
                        try {

                            first3 = getAccountNo.substring(0,3);
                            //first3 = enterAccountNo.getText().toString().substring(0,3);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        findIfscCode.setText(bankl.getBANKCODE()+first3);
                        findIfscCode.setEnabled(false);
                    }else if (bankl.getFLAG().equalsIgnoreCase("6")){
                        String first6 = "";
                        try {
                            first6 = getAccountNo.substring(0,6);
                            // first6 = enterAccountNo.getText().toString().substring(0,6);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        findIfscCode.setText(bankl.getBANKCODE()+first6);
                        findIfscCode.setEnabled(false);
                    }}else /*if(bankl.getFLAG().equalsIgnoreCase(null))*/{
                    findIfscCode.setText("no IFSC");
                    findIfscCode.setEnabled(true);
                }
            }
        }
    }

//    private void viewFeatures(String jj){}



//    @Override
//    public void showBeniName(String beneName,String accountNo,String bankName,String ifscCode) {
//        Log.d("BENENAME", "showBeneName"+beneName);
//
////        account_No=accountNo;
////        bank_Name=bankName;
////        ifsc_Code=ifscCode;
//
//        list.add(beneName);
//        ArrayAdapter<String> adp = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
//        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        fundtransferBeneName.setAdapter(adp);
//
//    }

    @Override
    public void showWallet(String status , String customerId, final long id, Boolean flag, Double balance, final Boolean imps, final Boolean neft, final Boolean verificationFlag) {

        customerIds=customerId;
        if(fundTransferMobileNo.getText().toString().length()==10 && walletInfoStatus){
           //rajesh ....change 27_08_2019
            walletInfoStatus =false;
            wallet_info.setVisibility(View.GONE);
        }else{
            walletInfoStatus =true;
            wallet_info.setVisibility(GONE);
        }

        Log.d("WALLET", "showWallet123"+id+"//"+flag);
        if (status.equals("0") && id==1 && flag){
            Log.d("WALLET", "showWallet123888"+"WALLET1");

            if (imps){
                paymentMode.add("IMPS");
            }
            if (neft){
                paymentMode.add("NEFT");
            }
//            pipeNo= String.valueOf(id);
            wallet1.setVisibility(View.VISIBLE);
            wallet1Balance.setText(balance.toString());

            wallet1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    wallet1.setBackground(getResources().getDrawable(R.drawable.button_selector));
                    wallet1.setBackground(getResources().getDrawable(R.drawable.button_select_bg));
                    wallet2.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    wallet3.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    selectBenificiaryLinear.setVisibility(View.VISIBLE);
                    wallet_info.setVisibility(GONE);


                   /* if (imps){
                        paymentMode.add("IMPS");
                    }
                    if (neft){
                        paymentMode.add("NEFT");
                    }*/


                    //selectBenificiaryAmountRelativelayout.setVisibility(View.VISIBLE);
                    // submitButtonFundTransfer.setVisibility(View.VISIBLE);

                    addBenificiaryRelativelayout.setVisibility(GONE);
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    submitButtonFundTransfer.setVisibility(GONE);
                    fundtransferBeneName.setSelection(0);

                    wallet1.setEnabled(false);
                    wallet2.setEnabled(true);
                    wallet3.setEnabled(true);
                    walletBooolen=false;

                    adpcc = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, paymentMode);
                    adpcc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fundTransferTransactionType.setAdapter(adpcc);
                    pipeNo= String.valueOf(id);
                    verificationflags = verificationFlag;
                    Log.d("Test", "Transaction: "+"verificationflagpipeno" +pipeNo);
                    Log.d("Test", "Transaction: "+"verificationflagpipenooo" +verificationflags);
                }
            });

        }
        //Wallet 2 Function (edited by rajesh)
        else if (status.equals("0") && id==2 && Constants.USER_WALLET_2_ACTIVE_STATUS){

            w2.setVisibility(View.VISIBLE);
            w3.setVisibility(View.VISIBLE);
            w4.setVisibility(View.VISIBLE);
            w5.setVisibility(View.VISIBLE);
            w6.setVisibility(View.VISIBLE);




            Log.d("WALLET", "showWallet1239999"+"WALLET2");

            if (imps){
                paymentMode1.add("IMPS");
            }
            if (neft){
                paymentMode1.add("NEFT");
            }

            if(!imps && !neft){
                paymentMode1.add("IMPS");
            }
//            pipeNo= String.valueOf(id);
            wallet2.setVisibility(View.VISIBLE);
            wallet2Balance.setText(balance.toString());

            wallet2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wallet2.setBackground(getResources().getDrawable(R.drawable.button_select_bg));
                    wallet1.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    wallet3.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    selectBenificiaryLinear.setVisibility(View.VISIBLE);
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    submitButtonFundTransfer.setVisibility(GONE);

                    wallet_info.setVisibility(GONE);


                    addBenificiaryRelativelayout.setVisibility(GONE);
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    submitButtonFundTransfer.setVisibility(GONE);
                    fundtransferBeneName.setSelection(0);

                    wallet2.setEnabled(false);
                    wallet1.setEnabled(true);
                    wallet3.setEnabled(true);
                    walletBooolen=true;



                    adpcc = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, paymentMode1);
                    adpcc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fundTransferTransactionType.setAdapter(adpcc);
                    pipeNo= String.valueOf(id);
                    verificationflags = verificationFlag;
                }
            });

        }

        //Wallet 3 Function (edited by uttam)
        else if (status.equals("0") && id==3 && Constants.USER_WALLET_3_ACTIVE_STATUS){

            w2.setVisibility(View.VISIBLE);
            w3.setVisibility(View.VISIBLE);
            w4.setVisibility(View.VISIBLE);
            w5.setVisibility(View.VISIBLE);
            w6.setVisibility(View.VISIBLE);




            Log.d("WALLET", "showWallet1239999"+"WALLET2");

            if (imps){
                paymentMode2.add("IMPS");
            }
            if (neft){
                paymentMode2.add("NEFT");
            }
            if(!imps && !neft){
                paymentMode2.add("IMPS");
                paymentMode2.add("NEFT");
            }
//            pipeNo= String.valueOf(id);
            wallet3.setVisibility(View.VISIBLE);
            wallet3Balance.setText(balance.toString());

            wallet3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wallet3.setBackground(getResources().getDrawable(R.drawable.button_select_bg));
                    wallet2.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                    wallet1.setBackground(getResources().getDrawable(R.drawable.button_deselect_bg));
                     selectBenificiaryLinear.setVisibility(View.VISIBLE);
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    submitButtonFundTransfer.setVisibility(GONE);

                    wallet_info.setVisibility(GONE);


                    addBenificiaryRelativelayout.setVisibility(GONE);
                    selectBenificiaryAmountRelativelayout.setVisibility(GONE);
                    submitButtonFundTransfer.setVisibility(GONE);
                    fundtransferBeneName.setSelection(0);

                    wallet2.setEnabled(true);
                    wallet1.setEnabled(true);
                    wallet3.setEnabled(false);
                    walletBooolen=true;



                    adpcc = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, paymentMode2);
                    adpcc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fundTransferTransactionType.setAdapter(adpcc);
                    pipeNo= String.valueOf(id);
                    verificationflags = verificationFlag;
                }
            });

        }

      /* else if (status.equals("0") && id==2 && flag){
            Log.d("WALLET", "showWallet1239999"+"WALLET2");
            if (imps){
                paymentMode1.add("IMPS");
            }
            if (neft){
                paymentMode1.add("NEFT");
            }
//            pipeNo= String.valueOf(id);
            wallet2.setVisibility(View.VISIBLE);
            wallet2Balance.setText(balance.toString());

            wallet2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wallet2.setBackground(getResources().getDrawable(R.color.colorBlue));
                    wallet1.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
                    wallet3.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
                    selectBenificiaryLinear.setVisibility(View.VISIBLE);
                    wallet2.setEnabled(false);
                    wallet1.setEnabled(true);
                    wallet3.setEnabled(true);

                    adpcc = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, paymentMode);
                    adpcc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fundTransferTransactionType.setAdapter(adpcc);
                    pipeNo= String.valueOf(id);
                    verificationflags = verificationFlag;
                }
            });

        }*/
       /* else if (status.equals("0") && id==3 && flag){
            Log.d("WALLET", "showWallet1237777"+"WALLET3");
            wallet3.setVisibility(View.VISIBLE);
            wallet3Balance.setText(balance.toString());
            if (imps){
                paymentMode2.add("IMPS");
            }
            if (neft){
                paymentMode2.add("NEFT");
            }

            wallet3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wallet3.setBackground(getResources().getDrawable(R.color.colorBlue));
                    wallet2.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
                    wallet1.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
                    selectBenificiaryLinear.setVisibility(View.VISIBLE);
                    wallet3.setEnabled(false);
                    wallet2.setEnabled(true);
                    wallet1.setEnabled(true);

                    adpcc = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, paymentMode);
                    adpcc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fundTransferTransactionType.setAdapter(adpcc);
                    pipeNo= String.valueOf(id);
                    verificationflags = verificationFlag;
                    if (verificationflags == true){
                        pipeNoo = "3";
                    }
                }
            });
        }*/
        else{
            w2.setVisibility(GONE);
            w3.setVisibility(GONE);
            w4.setVisibility(GONE);
            w5.setVisibility(GONE);
            w6.setVisibility(GONE);
        }

    }

    @Override
    public void showBeniName(final BeneListModel beneListModel) {

        if (beneListModel.getBeneName() == null)
        {
            //ArrayAdapter adp = new ArrayAdapter(this, android.R.layout.simple_spinner_item, beneName);
            //adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
           // fundtransferBeneName.setAdapter(adp);

            beneName.clear();
            beneName1.clear();
            beneName2.clear();
            beneName3.clear();


            beneName.add(" NONE");
            beneName1.add("");
            beneName2.add("");
            beneName3.add("");

            //beneNameSingle.add(beneListModel);
            CustomBeneAdapter customAdapter = new CustomBeneAdapter(FundTransfer.this, beneName, beneName1, beneName2,beneName3,fundTransferMobileNo.getText().toString());
            fundtransferBeneName.setAdapter(customAdapter);




        }else {
            beneName.add((beneListModel.getBeneName() == null) ? "" : beneListModel.getBeneName());

            beneName1.add(beneListModel.getAccountNo()/*.toString()*/);
            beneName2.add(beneListModel.getBankName()/*.toString()*/);

            // beneName3 = new ArrayList<String>();
            beneName3.add(beneListModel.getId());

            beneNameSingle.add(beneListModel);
            CustomBeneAdapter customAdapter = new CustomBeneAdapter(FundTransfer.this, beneName, beneName1, beneName2,beneName3,fundTransferMobileNo.getText().toString());
            fundtransferBeneName.setAdapter(customAdapter);
        }

    }

    public String loadJSONFromAssetBank() {
        String json = null;
        try {
            InputStream is = getAssets().open("banklists");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    private void showCustomDialog() throws IOException {
        // custom dialog
        final Dialog dialog = new Dialog(FundTransfer.this);
        dialog.setContentView(R.layout.find_ifsc_custom_dialog);
        TextView etBankNameDialog = dialog.findViewById(R.id.get_bank_name_dialog);
        Spinner getBranchNameDialog = dialog.findViewById(R.id.get_branch_name_dialog);
        Log.d("STATUS", "noStatus84650101020202"+ifscBranch);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,ifscBranch);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        getBranchNameDialog.setAdapter(aa);

        getBranchNameDialog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                /*String*/ kkk = ifscBranch.get(i).getIfscCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        String bankName111=jjj;
        etBankNameDialog.setText(bankName);

        Button dialogButtonConfirm = (Button) dialog.findViewById(R.id.dialogButtonConfirm);
        dialogButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findIfscCode.setText(kkk);
                dialog.dismiss();
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonCancel);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void findIfscCall() throws IOException {

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("number",bankName+".json");

            AndroidNetworking.post(BANK_IFSC)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                /*if(encodedUrl.contains("https://nginx.iserveu.tech")){
                                    encodedUrl = encodedUrl.replace("nginx","fino1");
                                }*/
                                System.out.println(">>>>-----"+encodedUrl);
                                encriptedFindIFSC(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    //Edited By Rajesh
    public void encriptedFindIFSC(String encodedUrl){

        FindIfscBranchAPI findIfscBranchAPI = this.apiFundTransferService.getClient().create(FindIfscBranchAPI.class);
        Call<List<IfscListModel>> respuesta = findIfscBranchAPI.getStatus(session.getUserToken(),  encodedUrl);

        respuesta.enqueue(new Callback<List<IfscListModel>>() {
            @Override
            public void onResponse(Call<List<IfscListModel>> call, Response<List<IfscListModel>> response) {
                Log.d("STATUS", "noStatus8465"+response.body());

                ifscBranch = response.body();
                Log.d("STATUS", "noStatus84650101"+ifscBranch);
            }

            @Override
            public void onFailure(Call<List<IfscListModel>> call, Throwable t) {
                Log.d("STATUS", "noStatus84650001"+t);
            }
        });
    }



    @Override
    public void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(FundTransfer.this);
        }
        loadingView.show();

    }

    @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }

    @Override
    public void showMessage(int message){
        if (message==0) {
            enrollRelativeLayout.setVisibility(View.VISIBLE);
//            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }else{
            verifyOtpDialog();
        }
    }

    public void clear(){

        fundtransferBeneName.setSelection(0);
        addedBeneDetailsRelative.setVisibility(GONE);
        searchBankNameSpinner.setText("");
        enterAccountNo.setText("");
        findIfscCode.setText("");
        enterBeneName.setText("");
        enterAmount.setText("");
//        paymentMode.add(null);

//        customerIds="";
//        pipeNo="";
        beneid="";

    }

    public void UpdateList(){

        System.out.println("Rajesh");
        fundtransferBeneName.onDetachedFromWindow();
        beneName.clear();
        beneName1.clear();
        beneName2.clear();
        beneName3.clear();

        beneName.add("  NONE");
        beneName1.add("");
        beneName2.add("");
        beneName3.add("");
        mActionsListener.loadWallet(session.getUserToken(), mob);

        wallet_info.setVisibility(GONE);

    }




}
