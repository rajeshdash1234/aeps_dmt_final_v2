package com.iserveu.aeps.fundtransfer;

public class IfscListModel {

    @Override
    public String toString() {
        return branchName;
    }

    private String branchName;

    private String ifscCode;

    public String getBranchName ()
    {
        return branchName;
    }

    public void setBranchName (String branchName)
    {
        this.branchName = branchName;
    }

    public String getIfscCode ()
    {
        return ifscCode;
    }

    public void setIfscCode (String ifscCode)
    {
        this.ifscCode = ifscCode;
    }


}
