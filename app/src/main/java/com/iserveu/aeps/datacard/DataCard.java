package com.iserveu.aeps.datacard;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.iserveu.aeps.R;
import com.iserveu.aeps.fundtransferreport.MyErrorMessage;
import com.iserveu.aeps.utils.APIService;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_RECHARGE_URL;

public class DataCard extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private APIService apiService;
    Session session;
    LoadingView loadingView;

    Button datacard_button;
    SearchableSpinner datacard_select_operator;
    EditText datacard_datacardno,datacard_amount;
    String datacardnoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,latitudeString,
            circleCodeString,longitudeString;

    Toolbar toolbar;

    String[] operatorDatacard = {"VODAFONE DATACARD","Airtel DATACARD","TATA PHOTON PLUS DATACARD","RELIANCE DATACARD","RELIANCE BROADBAND DATACARD",
            "MTS DATACARD","BSNL DATACARD","MTNL MUMBAI DATACARD","IDEA DATACARD","TataIndicom DATACARD","TATADocomo DATACARD","AIRCEL DATACARD"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_card);

        session = new Session(DataCard.this);

        rechargetypeString="";
        stdCodeString="";
        pincodeString="";
        accountNoString="";
        latitudeString="";
        circleCodeString="";
        longitudeString="";

        datacard_datacardno = findViewById(R.id.datacard_datacardno);
        datacard_amount = findViewById(R.id.datacard_amount);
        datacard_button = findViewById(R.id.datacard_button);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        datacard_datacardno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    datacardnoString = null;
                }else {
                    datacardnoString = datacard_datacardno.getText().toString();
                }
            }
        });

        datacard_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    amountString = null;
                }else {
                    amountString = datacard_amount.getText().toString();
                }
            }
        });
        datacard_select_operator = (SearchableSpinner) findViewById(R.id.datacard_select_operator);
        datacard_select_operator.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorDatacard);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        datacard_select_operator.setAdapter(aa);

        datacard_button.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (amountString==null|| datacardnoString==null){
            Toast.makeText(getApplicationContext(),"Enter CustomerId OR Enter Amount" , Toast.LENGTH_LONG).show();
        }else {
            rechargeDatacard();
//            Toast.makeText(getApplicationContext(),"Success" , Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        operatorString = operatorDatacard[i];
        //Toast.makeText(getApplicationContext(),operatorString , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void rechargeDatacard() {
        showLoader();
        final DataCardRequest dataCardRequest = new DataCardRequest(stdCodeString,pincodeString,amountString,datacardnoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,longitudeString);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp123456600"+dataCardRequest);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp12345660011"+session.getUserToken());
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566001177"+ Constants.BASE_URL+"getallrechargehere");

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            //performEncodedRecharge(dataCardRequest,encodedUrl);
                            proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
    public void proceedRecharge(String url){

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode",stdCodeString);
            obj.put("amount",amountString);
            obj.put("mobileNumber",datacardnoString);
            obj.put("accountNo",accountNoString);
            obj.put("circleCode",circleCodeString);
            obj.put("operatorCode",operatorString);
            obj.put("rechargeType",rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization",session.getUserToken())
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if(obj.getString("status").equalsIgnoreCase("0")){
                                    SuccessDialog(obj.getString("statusDesc"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message=gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            //transaction failed
                            ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                            //  anError.getResponse();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
  /*  public void performEncodedRecharge(DataCardRequest dataCardRequest, String encodedUrl){
        final DataCardApi dataCardApi = this.apiService.getClient().create(DataCardApi.class);

        dataCardApi.getDataCard(session.getUserToken(),dataCardRequest, encodedUrl).enqueue(new Callback<DataCardResponse>() {
            @Override
            public void onResponse(Call<DataCardResponse> call, Response<DataCardResponse> response) {
                Log.d("STATUS", "Transaction"+response.body());
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566"+response.body());
                    hideLoader();
                    SuccessDialog(response.body().getStatusDesc());

                }else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyOtp1234577"+response.body());
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DataCardResponse> call, Throwable t) {
                hideLoader();
                Log.d("STATUS", "TransactionFailedVerifyOtp1234588"+t);
                ErrorDialog("RECHARGE FAILED! undefined");
            }
        });

    }
*/
    public void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(DataCard.this);
        }
        loadingView.show();

    }


    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }

    private void ErrorDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(DataCard.this);
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void SuccessDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(DataCard.this);
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        onBackPressed();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
