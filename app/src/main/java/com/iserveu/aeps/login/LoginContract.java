package com.iserveu.aeps.login;

import android.content.Context;

import com.iserveu.aeps.dashboard.UserInfoModel;

import java.util.ArrayList;

/**
 * LoginContract class handles the communication between LoginView and Presenter
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 */
public class LoginContract {


    /**
     * View interface sends login status to LoginActivity
     */
    public interface View {

        /**
         * checkLoginStatus() checks  whether login is a failure or success. Status "0" is failure and Status "1" is success
         */
        void checkLoginStatus(String status, String message, String token, String nextFreshnessFactor);
        /**
         * checkEmptyFields() validates whether username and password are empty
         */
        void showLoginFeature(ArrayList<UserInfoModel.userFeature> userFeatures, String token);

        void checkEmptyFields();
        void showLoader();
        void hideLoader();

    }

    /**
     * UserActionsListener interface checks the Click of Login Button of LoginActivity
     */
    interface UserActionsListener {
        void performLogin(String username, String password, Context context);
        void loadFeature(String token);
        void getLoginDetails(String token);

    }


}
