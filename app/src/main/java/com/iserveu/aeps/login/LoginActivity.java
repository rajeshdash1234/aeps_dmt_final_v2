package com.iserveu.aeps.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.iserveu.aeps.R;
import com.iserveu.aeps.dashboard.UserInfoModel;
import com.iserveu.aeps.main2activity.Main2Activity;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.SharePreferenceClass;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;

import java.util.ArrayList;
import java.util.List;

import static com.iserveu.aeps.utils.Constants.USER_VERIFICATION_STATUS;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {


    private LoginPresenter mActionsListener;

    private EditText username,password;
    private Button login_button;
    LoadingView loadingView;
    Session session;
    SharePreferenceClass sf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_login );
        sf = new SharePreferenceClass(this);
        sf.setValue_string(USER_VERIFICATION_STATUS,"userverified");
        mActionsListener = new LoginPresenter(this);
        login_button=findViewById ( R.id.login_button );
        username=findViewById ( R.id.username );
        password=findViewById ( R.id.password );
        // Setup field validators.
        session = new Session(LoginActivity.this);

        login_button.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
//                pd.show ();
                mActionsListener.performLogin(username.getText().toString().trim(),password.getText().toString().trim(), LoginActivity.this);
            }
        } );
    }

    @Override
    public void checkLoginStatus(String status, String message,String token,String nextFreshnessFactor) {

        if(token !=null && !token.matches("") && status !=null && status.matches("0")) {

            session.setFreshnessFactor(nextFreshnessFactor);
            session.setLoggedIn(true);
         //   Intent in = new Intent(LoginActivity.this, DashboardActivity.class);
            Intent in = new Intent(LoginActivity.this, Main2Activity.class);
            startActivity(in);
            finish();
//            Toast.makeText(LoginActivity.this, "freshnessfactor : "+session.getFreshnessFactor(), Toast.LENGTH_SHORT).show();
           // mActionsListener.loadFeature(session.getUserToken());
        }else if(status != null && status.matches("-1")){
            session.setLoggedIn(true);
            Intent in = new Intent(LoginActivity.this, Main2Activity.class);
          //  Intent in = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(in);
            finish();
        }else {
            Toast.makeText(LoginActivity.this,getResources().getString(R.string.invalid_username_password),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void showLoginFeature(ArrayList<UserInfoModel.userFeature> userFeatures, String token) {

        if(userFeatures != null && token != null && !token.matches("")) {
            session.setUserToken(token);
            session.setUsername(username.getText().toString().trim());
            session.setPassword(password.getText().toString().trim());

            String aepsFeatureCode = "27";
            String matmFeatureCode = "33";
            String aeps2FeatureCode = "38";
            /*String aepsFeatureCode = "30";
            String matmFeatureCode = "40";
            String aeps2FeatureCode = "38";*/

            ArrayList<String> featureCode = new ArrayList<>();
            featureCode.add(aepsFeatureCode);
            featureCode.add(matmFeatureCode);
            featureCode.add(aeps2FeatureCode);
            featureCode.add("30");


            List<UserInfoModel.userFeature> temp_feature = Util.removeDuplicates(userFeatures);
            mActionsListener.getLoginDetails(token);
           /* for (int i = 0; i < temp_feature.size(); i++) {

                if (temp_feature.get(i).getId().equalsIgnoreCase(matmFeatureCode)) {
                    hideLoader();
                    Intent in = new Intent(LoginActivity.this, MicroAtmActivity.class);
                    startActivity(in);
                    break;
                }
                if(temp_feature.get(i).getId().equalsIgnoreCase(aeps2FeatureCode)){
                    Intent in = new Intent(LoginActivity.this, Aeps2Activity.class);
                    startActivity(in);
                    break;
                }
                if (temp_feature.get(i).getId().equalsIgnoreCase("30")) {
                    mActionsListener.getLoginDetails(token);
                    break;
                }

                if (temp_feature.get(i).getId().equalsIgnoreCase(aepsFeatureCode)) {
                    mActionsListener.getLoginDetails(token);
                    break;
                }
            }*/
        }else{
            Util.showAlert(this,getResources().getString(R.string.fail_error),getResources().getString(R.string.severerror));
        }
    }


    @Override
    public void checkEmptyFields() {
        Toast.makeText(LoginActivity.this,getResources().getString(R.string.empty_fields),Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoader() {
        if (loadingView ==null){
            loadingView = Util.showProgress(LoginActivity.this);
        }
        loadingView.show();
    }

    @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }
    }
}
