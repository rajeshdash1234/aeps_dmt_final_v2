package com.iserveu.aeps.main2activity;

import android.animation.ObjectAnimator;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iserveu.aeps.R;
import com.iserveu.aeps.aeps2.Aeps2Activity;
import com.iserveu.aeps.bbps.Bbps;
import com.iserveu.aeps.dashboard.BalanceContract;
import com.iserveu.aeps.dashboard.BalancePresenter;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.dashboard.UserInfoModel;
import com.iserveu.aeps.datacard.DataCard;
import com.iserveu.aeps.dth.Dth;
import com.iserveu.aeps.fundtransfer.FundTransfer;
import com.iserveu.aeps.fundtransferreport.FundTransferReport;
import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.login.LoginContract;
import com.iserveu.aeps.login.LoginPresenter;
import com.iserveu.aeps.microatm.MicroAtmActivity;
import com.iserveu.aeps.postpaid.PostPaid;
import com.iserveu.aeps.prepaid.Prepaid;
import com.iserveu.aeps.rechargereport.RechargeReport;
import com.iserveu.aeps.settings.SettingsActivity;
import com.iserveu.aeps.special.Special;
import com.iserveu.aeps.utility.Utility;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.SharePreferenceClass;
import com.iserveu.aeps.utils.Util;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Main2Activity extends AppCompatActivity implements BalanceContract.View, LoginContract.View{

    RelativeLayout fundtransfer_relative_layout,aeps_relative_layout,fundtransfer_report_relative_layout;
    RelativeLayout relative_layout_prepaid,relative_layout_postpaid,relative_layout_special,relative_layout_datacard,relative_layout_dth,relative_layout_utility,relative_layout_bbps,relative_layout_recharge_report;
    TextView balanceMoney,customer_name;
    private LoginPresenter loginPresenter;
    private BalancePresenter mActionsListener;
    Session session;
    private ImageView refresh_Payment,action_logout,action_settings;
    private CircleImageView customer_image;
    private RelativeLayout recharge_relative_layout;
    TextView empty_view;
    public static ArrayList<String> feature_array_list ;
    /** We will write our message to the socket **/
    BluetoothSocket socket = null;
    BluetoothAdapter bluetoothAdapter = null;


    /** The Bluetooth is an external device, which will receive our message **/
    BluetoothDevice blueToothDevice = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        loginPresenter = new LoginPresenter(Main2Activity.this);
        session = new Session(Main2Activity.this);
        mActionsListener = new BalancePresenter(Main2Activity.this);
        balanceMoney = findViewById ( R.id. balanceMoney );
        fundtransfer_relative_layout = findViewById(R.id.fundtransfer_relative_layout);
        fundtransfer_report_relative_layout = findViewById(R.id.fundtransfer_report_relative_layout);
        aeps_relative_layout = findViewById(R.id.aeps_relative_layout);
        recharge_relative_layout = (RelativeLayout)findViewById(R.id.recharge_relative_layout);
        empty_view = findViewById(R.id.empty_view);
        relative_layout_prepaid = findViewById(R.id.relative_layout_prepaid);
        relative_layout_postpaid = findViewById(R.id.relative_layout_postpaid);
        relative_layout_special = findViewById(R.id.relative_layout_special);
        relative_layout_datacard = findViewById(R.id.relative_layout_datacard);
        relative_layout_dth = findViewById(R.id.relative_layout_dth);
        relative_layout_utility =findViewById(R.id.relative_layout_utility);
        relative_layout_bbps =findViewById(R.id.relative_layout_bbps);
        relative_layout_recharge_report = findViewById(R.id.relative_layout_recharge_report);




        fundtransfer_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransfer.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        refresh_Payment = (ImageView)findViewById(R.id.refresh_Payment);
        refresh_Payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rorate_Clockwise(refresh_Payment);
                mActionsListener.loadBalance(session.getUserToken());
                System.out.println(">>>>>------Rajesh");
            }
        });

        relative_layout_prepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Prepaid.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }

            }
        });
        relative_layout_postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), PostPaid.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_special.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Special.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_datacard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), DataCard.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_dth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Dth.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_utility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Utility.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_bbps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Bbps.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });

        relative_layout_recharge_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), RechargeReport.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });




        customer_name = (TextView)findViewById(R.id.customer_name);
        customer_name.setText("Hello, "+Constants.USER_NAME);

        customer_image = (CircleImageView)findViewById(R.id.customer_image);
        loadProfileImage();

        action_logout = (ImageView) findViewById(R.id.action_logout);
        action_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // onBackPressed();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(Main2Activity.this);
                builder1.setMessage("Do you want to logout this app.");
                builder1.setTitle("Log out");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                Intent intent = new Intent(Main2Activity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        action_settings = (ImageView) findViewById(R.id.action_settings);
        action_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // onBackPressed();
                Intent in = new Intent(Main2Activity.this, SettingsActivity.class);
                startActivity(in);
                finish();
            }
        });
        aeps_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    if(feature_array_list.contains("27")){
                        startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                    }else if(!feature_array_list.contains("27") && feature_array_list.contains("33") && !feature_array_list.contains("38")){
                        startActivity(new Intent(getApplicationContext(), MicroAtmActivity.class));
                    }else if(!feature_array_list.contains("27") && !feature_array_list.contains("33") && feature_array_list.contains("38")){
                        startActivity(new Intent(getApplicationContext(), Aeps2Activity.class));
                    }



                    //  startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }

            }
        });

        fundtransfer_report_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransferReport.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadProfileImage(){
        String image_url ="https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/ADMIN_PROFILE%2F"+Constants.ADMIN_NAME+"%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";
        Glide.with(Main2Activity.this)
                .load(image_url)
                .apply(RequestOptions.placeholderOf(R.drawable.user_icon).error(R.drawable.user_icon))
                .into(customer_image);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActionsListener.loadBalance(session.getUserToken());

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Main2Activity.this);
        builder1.setMessage("Do you want to exit from this app.");
        builder1.setTitle("Exit");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {
        boolean feature_avalaible = false;
        String aeps = "27";
        String dmt = "30";
        String matmFeatureCode = "33";
        String aeps2FeatureCode = "38";
        String wallet2 = "41";
        String wallet3 = "42";
        ArrayList<String> featureCode = new ArrayList<>();
        featureCode.add(aeps);
        featureCode.add(dmt);
        featureCode.add("11");
        featureCode.add("4");
        featureCode.add("14");
        featureCode.add("15");
        featureCode.add("10");
        featureCode.add(wallet2);
        featureCode.add(wallet3);
        featureCode.add(matmFeatureCode);
        featureCode.add(aeps2FeatureCode);


        List<UserInfoModel.userFeature> temp_feature = Util.removeDuplicates(userFeatures);
        feature_array_list = new ArrayList<>();
        for (int i = 0; i < temp_feature.size(); i++) {
            if(featureCode.contains(temp_feature.get(i).getId())) {
                feature_array_list.add(temp_feature.get(i).getId());
                if(aeps.equalsIgnoreCase(temp_feature.get(i).getId())){
                    aeps_relative_layout.setVisibility(View.VISIBLE);
//                    aepsTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aepsFeatureCode);
                    loginPresenter.performLogin(session.getUserName(),session.getPassword(),Main2Activity.this);
                    feature_avalaible = true;
                }
                if(matmFeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                    aeps_relative_layout.setVisibility(View.VISIBLE);
//                    aepsTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aepsFeatureCode);
                    loginPresenter.performLogin(session.getUserName(),session.getPassword(),Main2Activity.this);
                    feature_avalaible = true;
                }
                if(aeps2FeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                    aeps_relative_layout.setVisibility(View.VISIBLE);
//                    aepsTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aepsFeatureCode);
                    loginPresenter.performLogin(session.getUserName(),session.getPassword(),Main2Activity.this);
                    feature_avalaible = true;
                }
                if(dmt.equalsIgnoreCase(temp_feature.get(i).getId())){
                    fundtransfer_relative_layout.setVisibility(View.VISIBLE);
                    fundtransfer_report_relative_layout.setVisibility(View.VISIBLE);
//                    matmTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(matmFeatureCode);
                    feature_avalaible = true;
                }
                if(temp_feature.get(i).getId().equalsIgnoreCase("4") || temp_feature.get(i).getId().equalsIgnoreCase("10")
                        || temp_feature.get(i).getId().equalsIgnoreCase("14") || temp_feature.get(i).getId().equalsIgnoreCase("15")
                        || temp_feature.get(i).getId().equalsIgnoreCase("11")){
                    recharge_relative_layout.setVisibility(View.VISIBLE);
                    feature_avalaible = true;
                }

                if(wallet2.equalsIgnoreCase(temp_feature.get(i).getId())&& temp_feature.get(i).getActive() == true){
                    Constants.USER_WALLET_2_ACTIVE_STATUS = true;
                }else{
                    Constants.USER_WALLET_2_ACTIVE_STATUS = false;
                }
                if(wallet3.equalsIgnoreCase(temp_feature.get(i).getId()) && temp_feature.get(i).getActive() == true){
                    Constants.USER_WALLET_3_ACTIVE_STATUS = true;
                }else{
                    Constants.USER_WALLET_3_ACTIVE_STATUS = false;
                }

            } else {

                Log.v("radha", "not contains : "+userFeatures.get(i).getId());

            }
        }

        if(feature_avalaible){
            empty_view.setVisibility(View.GONE);
        }else{
            empty_view.setVisibility(View.VISIBLE);
        }
        checkBluetoothConnection();

    }

    @Override
    public void showBalance(String balance) {
        balanceMoney.setText(balance);


    }

    @Override
    public void emptyLogin() {

    }

    @Override
    public void checkLoginStatus(String status, String message, String token, String nextFreshnessFactor) {

    }

    @Override
    public void showLoginFeature(ArrayList<UserInfoModel.userFeature> userFeatures, String token) {
        loginPresenter.getLoginDetails(token);
    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }
    public void rorate_Clockwise(View view) {
        ObjectAnimator rotate = ObjectAnimator.ofFloat(view, "rotation", 0f, 180f);
        rotate.setRepeatCount(5);
        rotate.setDuration(500);
        rotate.start();
    }


    public void checkBluetoothConnection(){
        SharePreferenceClass sharePreferenceClass = new SharePreferenceClass(Main2Activity.this);
        // blueToothDevice = sharePreferenceClass.getBluetoothInstance();

        //  if(blueToothDevice!=null) {
        try{
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            // if the BluetoothAdapter.getDefaultAdapter(); returns null then the
            // device does not have bluetooth hardware. Currently the emulator
            // does not support bluetooth so this will this condition will be true.
            // i.e. This code only runs on a hardware device an not on the emulator.
            if (bluetoothAdapter == null) {
                Log.e(this.toString(), "Bluetooth Not Available.");
                return;
            }
            String address = sharePreferenceClass.getBluetoothInstance();
            // This will find the remote device given the bluetooth hardware
            // address.
            // @todo: Change the address to the your device address
            if(address!=null) {
                blueToothDevice = bluetoothAdapter.getRemoteDevice(address);

                for (Integer port = 1; port <= 3; port++) {
                    simpleComm(Integer.valueOf(port));
                }

                sharePreferenceClass.setBluetoothInstance(blueToothDevice);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        // }
    }

    protected void simpleComm(Integer port) {
        // byte [] inputBytes = null;

        // The documents tell us to cancel the discovery process.
        bluetoothAdapter.cancelDiscovery();

        Log.d(this.toString(), "Port = " + port);
        try {
            // This is a hack to access "createRfcommSocket which is does not
            // have public access in the current api.
            // Note: BlueToothDevice.createRfcommSocketToServiceRecord (UUID
            // uuid) does not work in this type of application. .
            Method m = blueToothDevice.getClass().getMethod(
                    "createRfcommSocket", new Class[] { int.class });
            socket = (BluetoothSocket) m.invoke(blueToothDevice, port);

            // debug check to ensure socket was set.
            assert (socket != null) : "Socket is Null";

            // attempt to connect to device
            socket.connect();
            try {
                Log.d(this.toString(),
                        "************ CONNECTION SUCCEES! *************");

                // Grab the outputStream. This stream will send bytes to the
                // external/second device. i.e it will sent it out.
                // Note: this is a Java.io.OutputStream which is used in several
                // types of Java programs such as file io, so you may be
                // familiar with it.
                OutputStream outputStream = socket.getOutputStream();

                // Create the String to send to the second device.
                // Most devices require a '\r' or '\n' or both at the end of the
                // string.
                // @todo set your message
                String message = "---";

                // Convert the message to bytes and blast it through the
                // bluetooth
                // to the second device. You may want to use:
                // public byte[] getBytes (Charset charset) for proper String to
                // byte conversion.
                outputStream.write(message.getBytes());

            } finally {
                // close the socket and we are done.
                socket.close();
            }
            // IOExcecption is thrown if connect fails.
        } catch (IOException ex) {
            Log.e(this.toString(), "IOException " + ex.getMessage());
            // NoSuchMethodException IllegalAccessException
            // InvocationTargetException
            // are reflection exceptions.
        } catch (NoSuchMethodException ex) {
            Log.e(this.toString(), "NoSuchMethodException " + ex.getMessage());
        } catch (IllegalAccessException ex) {
            Log.e(this.toString(), "IllegalAccessException " + ex.getMessage());
        } catch (InvocationTargetException ex) {
            Log.e(this.toString(),
                    "InvocationTargetException " + ex.getMessage());
        }
    }

}
