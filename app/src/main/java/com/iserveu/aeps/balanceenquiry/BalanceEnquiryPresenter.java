package com.iserveu.aeps.balanceenquiry;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.utils.AEPSAPIService;
import com.iserveu.aeps.utils.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_BALANCE_URL;


/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class BalanceEnquiryPresenter implements BalanceEnquiryContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private BalanceEnquiryContract.View balanceEnquiryContractView;
    private AEPSAPIService aepsapiService;
    private Session session;
    /**
     * Initialize LoginPresenter
     */
    public BalanceEnquiryPresenter(BalanceEnquiryContract.View balanceEnquiryContractView) {
        this.balanceEnquiryContractView = balanceEnquiryContractView;
    }


   @Override
    public void performBalanceEnquiry(final String token, final BalanceEnquiryRequestModel balanceEnquiryRequestModel) {
       /* if (balanceEnquiryRequestModel!=null && balanceEnquiryRequestModel.getAadharNo() !=null && !balanceEnquiryRequestModel.getAadharNo().matches("") &&
                balanceEnquiryRequestModel.getCi() !=null && !balanceEnquiryRequestModel.getCi().matches("") &&
                balanceEnquiryRequestModel.getDc() !=null && !balanceEnquiryRequestModel.getDc().matches("") &&
                balanceEnquiryRequestModel.getDpId() !=null && !balanceEnquiryRequestModel.getDpId().matches("") &&
                balanceEnquiryRequestModel.getEncryptedPID() !=null && !balanceEnquiryRequestModel.getEncryptedPID().matches("") &&
                balanceEnquiryRequestModel.getFreshnessFactor() !=null && !balanceEnquiryRequestModel.getFreshnessFactor().matches("") &&
                balanceEnquiryRequestModel.gethMac() !=null && !balanceEnquiryRequestModel.gethMac().matches("") &&
                balanceEnquiryRequestModel.getIin() !=null && !balanceEnquiryRequestModel.getIin().matches("") &&
                balanceEnquiryRequestModel.getMcData() !=null && !balanceEnquiryRequestModel.getMcData().matches("") &&
                balanceEnquiryRequestModel.getMi() !=null && !balanceEnquiryRequestModel.getMi().matches("") &&
                balanceEnquiryRequestModel.getMobileNumber() !=null && !balanceEnquiryRequestModel.getMobileNumber().matches("") &&
                balanceEnquiryRequestModel.getRdsId() !=null && !balanceEnquiryRequestModel.getRdsId().matches("") &&
                balanceEnquiryRequestModel.getRdsVer() !=null && !balanceEnquiryRequestModel.getRdsVer().matches("") &&
                balanceEnquiryRequestModel.getsKey() !=null && !balanceEnquiryRequestModel.getsKey().matches("")
                ) {*/
            balanceEnquiryContractView.showLoader();

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            AndroidNetworking.get(GET_AEPS_BALANCE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            encryptBalanceEnquiry(token,balanceEnquiryRequestModel,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        /*} else {
            balanceEnquiryContractView.hideLoader();
            balanceEnquiryContractView.checkEmptyFields();
        }*/
    }


    public void encryptBalanceEnquiry(String token, BalanceEnquiryRequestModel balanceEnquiryRequestModel, String encodedUrl){
        BalanceEnquiryAPI balanceEnquiryAPI =this.aepsapiService.getClient().create(BalanceEnquiryAPI.class);

        balanceEnquiryAPI.checkBalanceEnquiry(token,balanceEnquiryRequestModel,encodedUrl).enqueue(new Callback<BalanceEnquiryResponse>() {
            @Override
            public void onResponse(Call<BalanceEnquiryResponse> call, Response<BalanceEnquiryResponse> response) {

                if(response.isSuccessful()) {
                    if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {
                        //message = "Login Successful";
                        Log.v("laxmi","hf"+response.body().getReferenceNo());
                        Log.v("laxmi","hf"+response.body().getApiComment());
                        balanceEnquiryContractView.hideLoader();
                        balanceEnquiryContractView.checkBalanceEnquiryStatus(response.body().getStatus(), response.body().getStatusDesc(),response.body());
                    }else{
                        balanceEnquiryContractView.hideLoader();
                        balanceEnquiryContractView.checkBalanceEnquiryStatus("", "Balance Enquiry Failed",null);
                    }

                }else{
                    balanceEnquiryContractView.hideLoader();
                    balanceEnquiryContractView.checkBalanceEnquiryStatus("", "Balance Enquiry Failed",null);
                }
            }

            @Override
            public void onFailure(Call<BalanceEnquiryResponse> call, Throwable t) {
                balanceEnquiryContractView.hideLoader();
                balanceEnquiryContractView.checkBalanceEnquiryStatus("", "Balance Enquiry Failed",null);
            }
        });
    }
}
