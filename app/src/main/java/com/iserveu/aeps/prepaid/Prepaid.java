package com.iserveu.aeps.prepaid;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.iserveu.aeps.R;
import com.iserveu.aeps.fundtransferreport.MyErrorMessage;
import com.iserveu.aeps.utils.APIService;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_RECHARGE_URL;

public class Prepaid extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private APIService apiService;
    Session session;
    LoadingView loadingView;

    Button prepaid_button;
    SearchableSpinner prepaid_select_operator;
    EditText prepaid_mobileno,prepaid_amount;
    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,latitudeString,circleCodeString,longitudeString;

        String[] operator = { "AIRCEL","AIRTEL","BSNL","IDEA","JIO","LOOP","MTNL DELHI","MTNL MUMBAI","MTS",
                "RELIANCE CDMA","RELIANCE GSM","TATA DOCOMO","TELENOR","VIDEOCON","VIRGIN CDMA","VIRGIN GSM","VODAFONE",
                "TataIndicom","T24(Flexi)"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepaid);

        session = new Session(Prepaid.this);

        rechargetypeString="Rr";
        stdCodeString="";
        pincodeString="";
        accountNoString="";
        latitudeString="";
        circleCodeString="";
        longitudeString="";

        prepaid_mobileno = findViewById(R.id.prepaid_mobileno);
        prepaid_amount = findViewById(R.id.prepaid_amount);
        prepaid_button = findViewById(R.id.prepaid_button);


        prepaid_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 10){
                    mobilenoString = prepaid_mobileno.getText().toString();
                }else {
                    mobilenoString = null;
                }
            }
        });

        prepaid_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                amountString = prepaid_amount.getText().toString();
//                if ((operatorString.equals("AIRTEL")||operatorString.equals("VODAFONE")||operatorString.equals("IDEA"))&&(amountString.equals("10")||amountString.equals("20")||amountString.equals("30")||amountString.equals("50"))){
////                    prepaid_button.setVisibility(View.INVISIBLE);
//                                        prepaid_button.setEnabled(false);
//                    prepaid_button.setBackgroundColor(getResources().getColor(R.color.colorGrey));
//                }else{
//                    prepaid_button.setEnabled(true);
//                    prepaid_button.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
//                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    amountString = null;
                }else {
                    amountString = prepaid_amount.getText().toString();
                    if ((operatorString.equals("AIRTEL")||operatorString.equals("VODAFONE")||operatorString.equals("IDEA"))&&(amountString.equals("10")||amountString.equals("20")||amountString.equals("30")||amountString.equals("50"))){
//                    prepaid_button.setVisibility(View.INVISIBLE);
                        prepaid_button.setEnabled(false);
                        prepaid_button.setBackgroundColor(getResources().getColor(R.color.colorGrey));
                    }else{
                        prepaid_button.setEnabled(true);
                        prepaid_button.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
                    }
                }
            }
        });
        prepaid_select_operator = (SearchableSpinner) findViewById(R.id.prepaid_select_operator);
        prepaid_select_operator.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operator);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        prepaid_select_operator.setAdapter(aa);

        prepaid_button.setOnClickListener(this);



    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        operatorString = operator[i];
        /*if ((operatorString.equals("AIRTEL")||operatorString.equals("VODAFONE")||operatorString.equals("IDEA"))&&(amountString.equals("10")||amountString.equals("20")||amountString.equals("30")||amountString.equals("50"))){

            prepaid_button.setEnabled(false);
            prepaid_button.setBackgroundColor(getResources().getColor(R.color.colorGrey));
        }else{
            prepaid_button.setEnabled(true);
            prepaid_button.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
        }*/
//        Toast.makeText(getApplicationContext(),s , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void rechargePrepaid() {
        showLoader();
       final PrepaidRequest prepaidRequest = new PrepaidRequest(stdCodeString,pincodeString,amountString,mobilenoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,longitudeString);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp123456600"+prepaidRequest);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp12345660011"+session.getUserToken());
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566001177"+ Constants.BASE_URL+"getallrechargehere");

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            //performEncodedRecharge(prepaidRequest,encodedUrl);
                            proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }


    public void proceedRecharge(String url){

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode",stdCodeString);
            obj.put("amount",amountString);
            obj.put("mobileNumber",mobilenoString);
            obj.put("accountNo",accountNoString);
            obj.put("circleCode",circleCodeString);
            obj.put("operatorCode",operatorString);
            obj.put("rechargeType",rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization",session.getUserToken())
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if(obj.getString("status").equalsIgnoreCase("0")){
                                    SuccessDialog(obj.getString("statusDesc"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message=gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            //transaction failed
                            ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                           //  anError.getResponse();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void performEncodedRecharge(PrepaidRequest prepaidRequest, String encodedUrl){

        final PrepaidApi prepaidApi = this.apiService.getClient().create(PrepaidApi.class);  //Constants.BASE_URL+"getallrechargehere"
        prepaidApi.getPrepaid(session.getUserToken(),prepaidRequest, encodedUrl).enqueue(new Callback<PrepaidResponse>() {
            @Override
            public void onResponse(Call<PrepaidResponse> call, Response<PrepaidResponse> response) {
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566"+response.body());
                    hideLoader();
                    SuccessDialog(response.body().getStatusDesc());

                }else {
                    hideLoader();
                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                }
            }

            @Override
            public void onFailure(Call<PrepaidResponse> call, Throwable t) {
                hideLoader();
                Log.d("STATUS", "TransactionFailedVerifyOtp1234588"+t);
                ErrorDialog("RECHARGE FAILED! undefined");
            }
        });



    }
    @Override
    public void onClick(View view) {
        if (amountString==null|| mobilenoString==null){
            Toast.makeText(getApplicationContext(),"Enter 10 Digit MobileNo OR Enter Amount" , Toast.LENGTH_LONG).show();
        }else if ((operatorString.equals("AIRTEL")||operatorString.equals("VODAFONE")||operatorString.equals("IDEA"))&&(amountString.equals("10")||amountString.equals("20")||amountString.equals("30")||amountString.equals("50"))){

            ErrorDialog("This denomination is blocked by operator. Kindly try with a valid amount.");
        }
        else {
            //Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
            rechargePrepaid();
        }

    }

    private void ErrorDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Prepaid.this);
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void SuccessDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Prepaid.this);
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        onBackPressed();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(Prepaid.this);
        }
        loadingView.show();

    }
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }
}
