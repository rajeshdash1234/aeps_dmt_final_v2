package com.iserveu.aeps.prepaid;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface PrepaidApi {
    @POST()
    Call<PrepaidResponse> getPrepaid(@Header("Authorization") String token, @Body PrepaidRequest prepaidRequest, @Url String url);
}
