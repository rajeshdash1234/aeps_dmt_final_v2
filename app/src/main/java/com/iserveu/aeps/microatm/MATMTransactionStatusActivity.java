package com.iserveu.aeps.microatm;

import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iserveu.aeps.R;
import com.iserveu.aeps.bluetooth.BluetoothPrinter;
import com.iserveu.aeps.fundtransfer.FundTransfer;
import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.main2activity.Main2Activity;
import com.iserveu.aeps.settings.MainActivity;
import com.iserveu.aeps.transactionstatus.TransactionStatusActivity;
import com.iserveu.aeps.transactionstatus.TransactionStatusModel;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.SharePreferenceClass;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MATMTransactionStatusActivity extends AppCompatActivity {

    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton,printButton;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;
    SharePreferenceClass sharePreferenceClass;
    String txnstatus = "SUCCESS";
    String balance = "NA";
    String cardnumber = "NA";
    String dataandtime = "NA";
    String rferencesnumber = "NA";
    String amount = "0";
    String rrn = "NA";
    String terminal = "NA";
    String accountNumber = "NA";
    String transaction_type="NA";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matmtransaction_status);
        sharePreferenceClass = new SharePreferenceClass(MATMTransactionStatusActivity.this);

        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        printButton = findViewById(R.id.printButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(Constants.MICRO_ATM_TRANSACTION_STATUS_KEY) == null) {
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView.setText("Some Exception occurred \n Please try again after some time !!!");
        }else{
            MicroAtmTransactionModel microAtmTransactionModel = (MicroAtmTransactionModel) getIntent().getSerializableExtra(Constants.MICRO_ATM_TRANSACTION_STATUS_KEY);

            if (microAtmTransactionModel .getTxnStatus() !=null && microAtmTransactionModel.getTxnAmt() !=null ) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                printButton.setVisibility(View.VISIBLE);
                transaction_type ="Receipt for ATM WITHDRAW";
                if(microAtmTransactionModel.getTxnStatus() !=null && !microAtmTransactionModel.getTxnStatus().matches("")){
                    txnstatus = microAtmTransactionModel.getTxnStatus();
                }
                if(microAtmTransactionModel.getAvailableBalance() !=null && !microAtmTransactionModel.getAvailableBalance().matches("NA")){
                    balance = microAtmTransactionModel.getAvailableBalance();
                }

                if(microAtmTransactionModel.getCardNumber() != null && !microAtmTransactionModel.getCardNumber().matches("")){
                    cardnumber = microAtmTransactionModel.getCardNumber();
                }
                if(microAtmTransactionModel.getRnr()!= null && !microAtmTransactionModel.getRnr().matches("")){
                    rferencesnumber = microAtmTransactionModel.getRnr();
                }

                if(microAtmTransactionModel.getTransactionDateandTime() !=null && !microAtmTransactionModel.getTransactionDateandTime().matches("")){
                    dataandtime = microAtmTransactionModel.getTransactionDateandTime();
                }

                if(microAtmTransactionModel.getTxnAmt() !=null && !microAtmTransactionModel.getTxnAmt().matches("")){
                    amount = microAtmTransactionModel.getTxnAmt();
                }

                if(microAtmTransactionModel.getTerminalId() !=null && !microAtmTransactionModel.getTerminalId().matches("")){
                    terminal = microAtmTransactionModel.getTerminalId();
                }

                if(microAtmTransactionModel.getType() !=null && !microAtmTransactionModel.getType().matches("")){
                    detailsTextView.setText ( "Cash Withdrawal for " +cardnumber + " is sucessfully completed . \n \nTransaction Status : "+txnstatus+" \nReference No : "+ rferencesnumber+"\nAvailable Balance : "+ balance+"\nTransaction Amount : "+amount+ " \nTransaction Date and Time : "+dataandtime+" \nTerminal ID : "+terminal);
                }

            }else  if (microAtmTransactionModel.getBalanceEnquiryStatus() != null && microAtmTransactionModel.getAccountNo() != null){
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                printButton.setVisibility(View.VISIBLE);
                transaction_type ="Receipt for BALANCE ENQUIRY";

                String  balanceEnquiryStatus = "NA";
                if(microAtmTransactionModel.getBalanceEnquiryStatus() !=null && !microAtmTransactionModel.getBalanceEnquiryStatus().matches("")){
                    balanceEnquiryStatus = microAtmTransactionModel.getBalanceEnquiryStatus();
                }

                if(microAtmTransactionModel.getAvailableBalance() !=null && !microAtmTransactionModel.getAvailableBalance().matches("NA")){
                    balance = microAtmTransactionModel.getAvailableBalance();
                }

                if(microAtmTransactionModel.getCardNumber() != null && !microAtmTransactionModel.getCardNumber().matches("")){
                    cardnumber = microAtmTransactionModel.getCardNumber();
                }
                if(microAtmTransactionModel.getRnr()!= null && !microAtmTransactionModel.getRnr().matches("")){
                    rferencesnumber = microAtmTransactionModel.getRnr();
                }

                if(microAtmTransactionModel.getTransactionDateandTime() !=null && !microAtmTransactionModel.getTransactionDateandTime().matches("")){
                    dataandtime = microAtmTransactionModel.getTransactionDateandTime();
                }

                if(microAtmTransactionModel.getAccountNo() !=null && !microAtmTransactionModel.getAccountNo().matches("")){
                    accountNumber = microAtmTransactionModel.getAccountNo();
                }

                if(microAtmTransactionModel.getTerminalId() !=null && !microAtmTransactionModel.getTerminalId().matches("")){
                    terminal = microAtmTransactionModel.getTerminalId();
                }

                if(microAtmTransactionModel.getType() !=null && !microAtmTransactionModel.getType().matches("")){
                    detailsTextView.setText (balanceEnquiryStatus+" for " +cardnumber + ". \n \nAccount No : "+accountNumber+" \nReference No : "+ rferencesnumber+"\nAvailable Balance : "+ balance+" \nTransaction Date and Time : "+dataandtime+" \nTerminal ID : "+terminal);
                }
            }else if(microAtmTransactionModel.getStatusError() != null && microAtmTransactionModel.getErrormsg() != null &&
                    !microAtmTransactionModel.getStatusError().matches("") && !microAtmTransactionModel.getErrormsg().matches("") &&
                    microAtmTransactionModel.getStatusError().trim().equalsIgnoreCase("success")) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    detailsTextView.setText(microAtmTransactionModel.getErrormsg());
                    detailsTextView.setGravity(Gravity.CENTER);
                    printButton.setVisibility(View.GONE);
                }
            }else if(microAtmTransactionModel.getStatusError() != null && microAtmTransactionModel.getErrormsg() != null &&
                    !microAtmTransactionModel.getStatusError().matches("") && !microAtmTransactionModel.getErrormsg().matches("") &&
                    microAtmTransactionModel.getStatusError().trim().equalsIgnoreCase("failed")) {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                printButton.setVisibility(View.GONE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    failureDetailTextView.setText(microAtmTransactionModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again !!!");
                }
            }else{
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                printButton.setVisibility(View.GONE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    failureDetailTextView .setText (microAtmTransactionModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again after some time !!!");
                }
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Constants.BRAND_NAME.trim().length()!=0) {
                    BluetoothDevice bluetoothDevice = Constants.bluetoothDevice;
                    if (bluetoothDevice != null) {
                        callBluetoothFunction(bluetoothDevice, sharePreferenceClass.getAdminName());
                    } else {
                        Intent in = new Intent(MATMTransactionStatusActivity.this, MainActivity.class);
                        startActivity(in);

                        Toast.makeText(MATMTransactionStatusActivity.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    showBrandSetAlert();
                }
            }
        });
    }

    private void showBrandSetAlert(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MATMTransactionStatusActivity.this);
        builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
        builder1.setTitle("Warning!!!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "GOT IT",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void callBluetoothFunction(BluetoothDevice bluetoothDevice, final String admin_name) {

        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(Constants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                mPrinter.setBold(true);
                mPrinter.printText(transaction_type);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(false);
                mPrinter.printText("Date & Time: "+dateFormater(dataandtime));
                mPrinter.addNewLine();
                mPrinter.setBold(false);
                mPrinter.printText("Txn Amt: "+amount);
                mPrinter.addNewLine();
                mPrinter.setBold(false);
                mPrinter.printText("Terminal ID: "+terminal);
                mPrinter.addNewLine();
                mPrinter.setBold(false);
                mPrinter.printText("RRN: "+rferencesnumber);
                mPrinter.addNewLine();
                mPrinter.setBold(false);
                mPrinter.printText("Card Details: "+cardnumber);
                mPrinter.addNewLine();
                mPrinter.setBold(false);
                mPrinter.printText("Txn Status: "+txnstatus);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.printText("Note : Don't pay any charge for this txn");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(Constants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");

                Toast.makeText(MATMTransactionStatusActivity.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();

            }
        });

    }
    public String dateFormater(String date){
        String date_ = date;
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date newDate=spf.parse(date);
            spf= new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
            date_ = spf.format(newDate);
        }catch (Exception e){
            date_ = date;
            e.printStackTrace();
        }

        return date_;

    }

}
