package com.iserveu.aeps.aeps2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.finopaytech.finosdk.activity.DeviceSettingActivity;
import com.finopaytech.finosdk.encryption.AES_BC;
import com.finopaytech.finosdk.helpers.Utils;
import com.iserveu.aeps.R;
import com.iserveu.aeps.aeps2report.Aeps2ReportActivity;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.dashboard.UserInfoModel;
import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.main2activity.Main2Activity;
import com.iserveu.aeps.microatm.MicroAtmActivity;
import com.iserveu.aeps.microatm.MicroAtmContract;
import com.iserveu.aeps.microatm.MicroAtmPresenter;
import com.iserveu.aeps.microatm.MicroAtmRequestModel;
import com.iserveu.aeps.microatm.MicroAtmResponse;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.iserveu.aeps.utils.Util.showProgress;

public class Aeps2Activity extends AppCompatActivity implements View.OnClickListener,MicroAtmContract.View {

    public TextView labelTextiew;
    RadioGroup rgTransactionType;
    RadioButton rbCashWithdrawal;
    RadioButton rbBalanceEnquiry;
    EditText etAmount;
    Button btnProceed,fingerprintButton;
    String strTransType = "";
    String encData;
    String authentication;

    public MicroAtmPresenter microAtmPresenter;

    MicroAtmRequestModel microAtmRequestModel;

    LoadingView loadingView;
    Aeps2ResponesModel aeps2ResponesModel;

    Session session;
    TextView matmTabOption,aepsTabOption,aeps2Option;

    LinearLayout tabLayout;
    ArrayList<String> featurescodesresponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aeps2);

        setToolbar();
        microAtmPresenter = new MicroAtmPresenter(this);
        session = new Session(Aeps2Activity.this);
        matmTabOption = findViewById( R. id.matmTabOption);
       // matmTabOption.setVisibility(View.GONE);
        aepsTabOption = findViewById( R. id.aepsTabOption);
       // aepsTabOption.setVisibility(View.GONE);
        aeps2Option = findViewById( R. id.aeps2Option);
        aeps2Option.setVisibility(View.VISIBLE);
        tabLayout = findViewById ( R.id. tabLayout );


        if(Main2Activity.feature_array_list.contains("27") && Main2Activity.feature_array_list.contains("33")){
            aepsTabOption.setVisibility(View.VISIBLE);
            matmTabOption.setVisibility(View.VISIBLE);
            tabLayout.setWeightSum(3);
        }else if(!Main2Activity.feature_array_list.contains("27") && Main2Activity.feature_array_list.contains("33")){
            aepsTabOption.setVisibility(View.GONE);
            matmTabOption.setVisibility(View.VISIBLE);
            tabLayout.setWeightSum(2);
        }else if(Main2Activity.feature_array_list.contains("27") && !Main2Activity.feature_array_list.contains("33")){
            aepsTabOption.setVisibility(View.VISIBLE);
            matmTabOption.setVisibility(View.GONE);
            tabLayout.setWeightSum(2);
        }else if(!Main2Activity.feature_array_list.contains("27") && !Main2Activity.feature_array_list.contains("33")){
            aepsTabOption.setVisibility(View.GONE);
            matmTabOption.setVisibility(View.GONE);
            tabLayout.setWeightSum(1);
        }
        rgTransactionType = findViewById(R.id.rg_trans_type);
        rbCashWithdrawal = findViewById(R.id.rb_cw);
        rbBalanceEnquiry = findViewById(R.id.rb_be);
        btnProceed = findViewById(R.id.btn_proceed);
        etAmount = findViewById(R.id.et_amount);
        labelTextiew = findViewById(R.id.labelTextiew);
        fingerprintButton = findViewById(R.id.fingerprintButton);

        hideKeyboard();

        fingerprintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingIntent = new Intent(Aeps2Activity.this, DeviceSettingActivity.class);
                settingIntent.putExtra("IS_PAIR_DEVICE",true);
                startActivityForResult(settingIntent,3);
            }
        });

        microAtmPresenter.loadFeature(session.getUserToken());

        matmTabOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Aeps2Activity.this, MicroAtmActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(in);
                overridePendingTransition(0,0);
                finish();
            }
        });

        rgTransactionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {
                    case R.id.rb_cw:
                        etAmount.setClickable(true);
                        etAmount.setHint("Amount");
                        etAmount.setVisibility(View.VISIBLE);
                        labelTextiew.setVisibility(View.VISIBLE);
                        labelTextiew.setText("Enter Amount Below");
                        strTransType = "Cash Withdrawal";
                        etAmount.setText("");
                        etAmount.setEnabled(true);
                        etAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case R.id.rb_be:
                        strTransType = "Balance Enquiry";
                        etAmount.setVisibility(View.GONE);
                        labelTextiew.setVisibility(View.GONE);
                        etAmount.setClickable(false);
                        etAmount.setEnabled(false);
                        break;
                }
            }
        });

        btnProceed.setOnClickListener(this);

        aepsTabOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Aeps2Activity.this, DashboardActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(in);
                overridePendingTransition(0,0);
                finish();
            }
        });

    }

    private void setToolbar() {

        Toolbar mToolbar = findViewById ( R.id.toolbar );
        mToolbar.setTitle ( getResources ().getString ( R.string.home_title ));
        mToolbar.inflateMenu ( R.menu.main_menu );

        mToolbar.setOnMenuItemClickListener ( new Toolbar.OnMenuItemClickListener () {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()==R.id.action_report)
                {
                    Intent in = new Intent(Aeps2Activity.this,Aeps2ReportActivity.class);
                    startActivity(in);
                }
                else if(item.getItemId()== R.id.action_logout)
                {
                    if (session!=null){
                        session.clear();
                        Intent in = new Intent(Aeps2Activity.this,LoginActivity.class);
                        startActivity(in);
                        finish();
                    }
                }
                return false;
            }
        } );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:
                showLoader();
                if(Utils.isAEPSDevicePaired(Aeps2Activity.this) == true){
                    if (validate()) {
                        if(rbCashWithdrawal.isChecked()){
                            apiCalling();
                        } else if (rbBalanceEnquiry.isChecked()){
                            balanceEnquiryApiCalling();
                        }
                    }
                }else{
                    hideLoader();
                    Util.showAlert(this,getResources().getString(R.string.alert_error),getResources().getString(R.string.connection_fingerprint_device));
                }

                break;
        }
    }

    private boolean validate() {
        if ((!rbCashWithdrawal.isChecked()) && (!rbBalanceEnquiry.isChecked())) {
            showOneBtnDialog(this, "Info", "Please select Transaction Type!", false);
            return false;
        }
        if(rbCashWithdrawal.isChecked()) {
            if (etAmount.getText().toString().equals("")) {
                String msg = "";
                if (rbCashWithdrawal.isChecked()) {
                    msg = "Amount";
                }
                hideLoader();
                showOneBtnDialog(this, "Info", "Please enter " + msg + " !", false);
                return false;
            }
        }
        return true;
    }

    private void showOneBtnDialog(final Context mContext, String title, String msg, boolean cancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                hideKeyboard();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.show();
    }

    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String getServiceID() {
        String clientRefID = "";


        if (rbCashWithdrawal.isChecked())
            clientRefID = Constants.SERVICE_AEPS_CW;
        if (rbBalanceEnquiry.isChecked())
            clientRefID = Constants.SERVICE_AEPS_BE;

        return clientRefID;
    }


    public void apiCalling()
    {
        microAtmRequestModel = new MicroAtmRequestModel(etAmount.getText().toString(),getServiceID(),"mobile");
        microAtmPresenter.performRequestData(session.getUserToken(), microAtmRequestModel);

    }

    public void balanceEnquiryApiCalling()
    {
        microAtmRequestModel = new MicroAtmRequestModel("0",getServiceID(),"mobile");
        microAtmPresenter.performRequestData(session.getUserToken(), microAtmRequestModel);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null & resultCode == RESULT_OK) {
            aeps2ResponesModel = new Aeps2ResponesModel();
            if (requestCode == 1) {
                String response;
                if (data.hasExtra("ClientResponse")) {
                    hideLoader();
                    response = data.getStringExtra("ClientResponse");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String strDecryptResponse = AES_BC.getInstance().decryptDecode(Utils.replaceNewLine(response), Constants.CLIENT_REQUEST_ENCRYPTION_KEY);
                            Log.v("radha", "respones transaction : " + strDecryptResponse);
                            if(rbCashWithdrawal.isChecked()) {
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                aeps2ResponesModel.setAmount(jsonObject.getString("Amount"));
                                aeps2ResponesModel.setAdhaarNo(jsonObject.getString("AdhaarNo"));
                                aeps2ResponesModel.setTxnTime(jsonObject.getString("TxnTime"));
                                aeps2ResponesModel.setTxnDate(jsonObject.getString("TxnDate"));
                                aeps2ResponesModel.setBankName(jsonObject.getString("BankName"));
                                aeps2ResponesModel.setrRN(jsonObject.getString("RRN"));
                                aeps2ResponesModel.setStatus(jsonObject.getString("Status"));
                                aeps2ResponesModel.setCustomerMobile(jsonObject.getString("CustomerMobile"));
                                aeps2ResponesModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                aeps2ResponesModel.setLedgerBalance(jsonObject.getString("LedgerBalance"));
                                aeps2ResponesModel.setType(getServiceID());
                                aeps2ResponesModel.setTransactionType("cashwithdrawl");
                                Log.v("radha","cashwithdrawl enquiry : "+strDecryptResponse);
                            }else if(rbBalanceEnquiry.isChecked()){
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                aeps2ResponesModel.setAmount(jsonObject.getString("Amount"));
                                aeps2ResponesModel.setAdhaarNo(jsonObject.getString("AdhaarNo"));
                                aeps2ResponesModel.setTxnTime(jsonObject.getString("TxnTime"));
                                aeps2ResponesModel.setTxnDate(jsonObject.getString("TxnDate"));
                                aeps2ResponesModel.setBankName(jsonObject.getString("BankName"));
                                aeps2ResponesModel.setrRN(jsonObject.getString("RRN"));
                                aeps2ResponesModel.setStatus(jsonObject.getString("Status"));
                                aeps2ResponesModel.setCustomerMobile(jsonObject.getString("CustomerMobile"));
                                aeps2ResponesModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                aeps2ResponesModel.setLedgerBalance(jsonObject.getString("LedgerBalance"));
                                aeps2ResponesModel.setType(getServiceID());
                                aeps2ResponesModel.setTransactionType("balance");
                                Log.v("radha","balance enquiry : "+strDecryptResponse);
                            }

                        } catch (Exception e) {
                            aeps2ResponesModel = null;
                            Log.v("test", "error : " + e);
                        }
                    }
                } else if (data.hasExtra("ErrorDtls")) {
                    hideLoader();
                    response = data.getStringExtra("ErrorDtls");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String[] error_dtls = response.split("\\|");
                            String errorMsg = error_dtls[0];
                            aeps2ResponesModel.setErrormsg(errorMsg);
                        } catch (Exception exp) {
                            aeps2ResponesModel = null;
                            Log.v("test", "Error : " + exp.toString());
                        }
                    }
                } else {
                    aeps2ResponesModel = null;
                }
            }
            if (requestCode == 3) {
                String response;
                if (data.hasExtra("DeviceConnectionDtls")) {
                    response = data.getStringExtra("DeviceConnectionDtls");
                    String[] error_dtls = response.split("\\|");
                    String errorMsg = error_dtls[0];
                    String errorMsg2 = error_dtls[1];
                    aeps2ResponesModel.setErrormsg(errorMsg2);
                    aeps2ResponesModel.setStatusError(errorMsg);
                    Log.v("radha","device response : "+response);

                }
            }

            nextPage();
        }
    }

    public void nextPage(){

        Intent intentAtm = new Intent(Aeps2Activity.this,Aeps2TransactionStatusActivity.class);
        intentAtm.putExtra(Constants.AEPS_2_TRANSACTION_STATUS_KEY,aeps2ResponesModel);
        startActivity(intentAtm);

    }

   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK)
            return true;

        return true;
    }*/

    @Override
    public void checkRequestCode(String status, String message, MicroAtmResponse microAtmResponse) {
        if(status!= null && !status.matches("")) {
            authentication = microAtmResponse.getAuthentication();
            encData = microAtmResponse.getEncData();
            Intent intent = new Intent(getBaseContext(), com.finopaytech.finosdk.activity.MainTransactionActivity.class);
            intent.putExtra("RequestData", encData);
            intent.putExtra("HeaderData", authentication);
            intent.putExtra("ReturnTime", 5);// Application return time in second
            startActivityForResult(intent, 1);
        }else{
            Util.showAlert(this,getResources().getString(R.string.alert_error),message);
        }
    }

    @Override
    public void checkEmptyFields() {

    }



    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {
        String aepsFeatureCode = "27";
        String matmFeatureCode = "33";
        String aeps2FeatureCode = "38";
        /*String aepsFeatureCode = "30";
        String matmFeatureCode = "40";
        String aeps2FeatureCode = "38";*/
        ArrayList<String> featureCode = new ArrayList<>();
        featureCode.add(aepsFeatureCode);
        featureCode.add(matmFeatureCode);
        featureCode.add(aeps2FeatureCode);
        featurescodesresponse = new ArrayList<>();


        List<UserInfoModel.userFeature> temp_feature = Util.removeDuplicates(userFeatures);


        for (int i = 0; i < temp_feature.size(); i++) {

            if(featureCode.contains(temp_feature.get(i).getId())) {
                if(aepsFeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                   // aepsTabOption.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(aepsFeatureCode);
                }
                if(matmFeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                   // matmTabOption.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(matmFeatureCode);
                }
                if(aeps2FeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                   // aeps2Option.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(aeps2FeatureCode);
                }
                Log.v("radha", "contains : "+temp_feature.get(i).getId());
            } else {
                Log.v("radha", "not contains : "+temp_feature.get(i).getId());
            }
        }
        /*if(featurescodesresponse.size() == 2){
            tabLayout.setWeightSum(2);
        }else if(featurescodesresponse.size() == 3){
            tabLayout.setWeightSum(3);
        }else{
            tabLayout.setWeightSum(1);
        }*/
    }

    @Override
    public void showLoader() {
        if (loadingView ==null){
            loadingView = showProgress(this);
        }
        loadingView.show();
    }

    @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
    }
}
