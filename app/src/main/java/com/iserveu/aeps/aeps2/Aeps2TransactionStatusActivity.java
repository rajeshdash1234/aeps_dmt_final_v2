package com.iserveu.aeps.aeps2;

import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iserveu.aeps.R;
import com.iserveu.aeps.bluetooth.BluetoothPrinter;
import com.iserveu.aeps.fundtransfer.FundTransfer;
import com.iserveu.aeps.microatm.MATMTransactionStatusActivity;
import com.iserveu.aeps.settings.MainActivity;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.SharePreferenceClass;

public class Aeps2TransactionStatusActivity extends AppCompatActivity {

    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton,printButton;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;
    String txnstatus = "NA";
    String balance = "NA";
    String adhaarnumber = "NA";
    String rferencesnumber = "NA";
    String data = "NA";
    String amount = "NA";
    String time = "NA";
    String bankName = "NA";
    String mobileNumber = "NA";
    String ledgerBalance = "NA";
    SharePreferenceClass sharePreferenceClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aeps2_transaction_status);
        sharePreferenceClass = new SharePreferenceClass(Aeps2TransactionStatusActivity.this);

        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        printButton = findViewById(R.id.printButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(Constants.AEPS_2_TRANSACTION_STATUS_KEY) == null) {
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView.setText("Some Exception occured");
        }else{
            Aeps2ResponesModel aeps2ResponesModel = (Aeps2ResponesModel) getIntent().getSerializableExtra(Constants.AEPS_2_TRANSACTION_STATUS_KEY);

            if (aeps2ResponesModel .getTransactionType() !=null && aeps2ResponesModel.getTransactionType().matches("cashwithdrawl") ) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                printButton.setVisibility(View.VISIBLE);

                if(aeps2ResponesModel .getStatus() !=null && !aeps2ResponesModel .getStatus().matches("")){
                    txnstatus = aeps2ResponesModel.getStatus();
                }

                 if(aeps2ResponesModel.getAvailableBalance() !=null && !aeps2ResponesModel.getAvailableBalance().matches("NA")){
                    balance = aeps2ResponesModel.getAvailableBalance();
                }

                if(aeps2ResponesModel.getAdhaarNo() != null && !aeps2ResponesModel.getAdhaarNo().matches("")){
                    adhaarnumber = aeps2ResponesModel.getAdhaarNo();
                }
                if(aeps2ResponesModel.getrRN()!= null && !aeps2ResponesModel.getrRN().matches("")){
                    rferencesnumber = aeps2ResponesModel.getrRN();
                }

                if(aeps2ResponesModel.getTxnDate() !=null && !aeps2ResponesModel.getTxnDate().matches("")){
                    data = aeps2ResponesModel.getTxnDate();
                }

                if(aeps2ResponesModel.getAmount() !=null && !aeps2ResponesModel.getAmount().matches("")){
                    amount = aeps2ResponesModel.getAmount();
                }

                if(aeps2ResponesModel.getTxnTime() !=null && !aeps2ResponesModel.getTxnTime().matches("")){
                    time = aeps2ResponesModel.getTxnTime();
                }

                if(aeps2ResponesModel.getBankName() !=null && !aeps2ResponesModel.getBankName().matches("")){
                    bankName = aeps2ResponesModel.getBankName();
                }

                 if(aeps2ResponesModel.getCustomerMobile() !=null && !aeps2ResponesModel.getCustomerMobile().matches("")){
                    mobileNumber = aeps2ResponesModel.getCustomerMobile();
                }

                if(aeps2ResponesModel.getLedgerBalance() !=null && !aeps2ResponesModel.getLedgerBalance().matches("")){
                    ledgerBalance = aeps2ResponesModel.getLedgerBalance();
                }

                if(aeps2ResponesModel.getType() !=null && !aeps2ResponesModel.getType().matches("")){
                    detailsTextView.setText ( " Cash Withdrawal for customer account linked with aadhar card " + adhaarnumber + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + rferencesnumber + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount + "\n Transaction Date : " +data + "\n Transaction Time : " +time);
                }

            }else  if (aeps2ResponesModel .getTransactionType() !=null && aeps2ResponesModel.getTransactionType().matches("balance") ) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                printButton.setVisibility(View.VISIBLE);

                String txnstatus = "NA";
                if(aeps2ResponesModel .getStatus() !=null && !aeps2ResponesModel .getStatus().matches("")){
                    txnstatus = aeps2ResponesModel.getStatus();
                }

                String balance = "NA";
                if(aeps2ResponesModel.getAvailableBalance() !=null && !aeps2ResponesModel.getAvailableBalance().matches("NA")){
                    balance = aeps2ResponesModel.getAvailableBalance();
                }

                String adhaarnumber = "NA";
                if(aeps2ResponesModel.getAdhaarNo() != null && !aeps2ResponesModel.getAdhaarNo().matches("")){
                    adhaarnumber = aeps2ResponesModel.getAdhaarNo();
                }
                String rferencesnumber = "NA";
                if(aeps2ResponesModel.getrRN()!= null && !aeps2ResponesModel.getrRN().matches("")){
                    rferencesnumber = aeps2ResponesModel.getrRN();
                }

                String data = "NA";
                if(aeps2ResponesModel.getTxnDate() !=null && !aeps2ResponesModel.getTxnDate().matches("")){
                    data = aeps2ResponesModel.getTxnDate();
                }

                String amount = "NA";
                if(aeps2ResponesModel.getAmount() !=null && !aeps2ResponesModel.getAmount().matches("")){
                    amount = aeps2ResponesModel.getAmount();
                }

                String time = "NA";
                if(aeps2ResponesModel.getTxnTime() !=null && !aeps2ResponesModel.getTxnTime().matches("")){
                    time = aeps2ResponesModel.getTxnTime();
                }

                String bankName = "NA";
                if(aeps2ResponesModel.getBankName() !=null && !aeps2ResponesModel.getBankName().matches("")){
                    bankName = aeps2ResponesModel.getBankName();
                }

                String mobileNumber = "NA";
                if(aeps2ResponesModel.getCustomerMobile() !=null && !aeps2ResponesModel.getCustomerMobile().matches("")){
                    mobileNumber = aeps2ResponesModel.getCustomerMobile();
                }

                String ledgerBalance = "NA";
                if(aeps2ResponesModel.getLedgerBalance() !=null && !aeps2ResponesModel.getLedgerBalance().matches("")){
                    ledgerBalance = aeps2ResponesModel.getLedgerBalance();
                }

                if(aeps2ResponesModel.getType() !=null && !aeps2ResponesModel.getType().matches("")){
                    detailsTextView.setText ( " Cash Withdrawal for customer account linked with aadhar card " + adhaarnumber + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + rferencesnumber + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount + "\n Transaction Date : " +data + "\n Transaction Time : " +time);
                }
            }else if(aeps2ResponesModel.getStatusError() != null && aeps2ResponesModel.getErrormsg() != null &&
                    !aeps2ResponesModel.getStatusError().matches("") && !aeps2ResponesModel.getErrormsg().matches("") &&
                    aeps2ResponesModel.getStatusError().trim().equalsIgnoreCase("success")) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                printButton.setVisibility(View.GONE);

                if(aeps2ResponesModel.getErrormsg() != null && !aeps2ResponesModel.getErrormsg().matches("")){
                    detailsTextView.setText(aeps2ResponesModel.getErrormsg());
                    detailsTextView.setGravity(Gravity.CENTER);
                }
            }else if(aeps2ResponesModel.getStatusError() != null && aeps2ResponesModel.getErrormsg() != null &&
                    !aeps2ResponesModel.getStatusError().matches("") && !aeps2ResponesModel.getErrormsg().matches("") &&
                    aeps2ResponesModel.getStatusError().trim().equalsIgnoreCase("failed")) {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                printButton.setVisibility(View.GONE);

                if(aeps2ResponesModel.getErrormsg() != null && !aeps2ResponesModel.getErrormsg().matches("")){
                    failureDetailTextView.setText(aeps2ResponesModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again !!!");
                }
            }else {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                printButton.setVisibility(View.GONE);

                if(aeps2ResponesModel.getErrormsg() != null && !aeps2ResponesModel.getErrormsg().matches("")){
                    failureDetailTextView .setText (aeps2ResponesModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again after some time.");
                }
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Constants.BRAND_NAME.trim().length()!=0) {
                    BluetoothDevice bluetoothDevice = Constants.bluetoothDevice;
                    if (bluetoothDevice != null) {
                        callBluetoothFunction(bluetoothDevice);
                    } else {
                        Intent in = new Intent(Aeps2TransactionStatusActivity.this, MainActivity.class);
                        startActivity(in);

                        Toast.makeText(Aeps2TransactionStatusActivity.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    showBrandSetAlert();
                }
            }
        });
    }
    private void showBrandSetAlert(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Aeps2TransactionStatusActivity.this);
        builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
        builder1.setTitle("Warning!!!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "GOT IT",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void callBluetoothFunction(BluetoothDevice bluetoothDevice) {


        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(Constants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                mPrinter.printText("--------Transaction Report---------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.printText("Bank Name: "+bankName);
                mPrinter.addNewLine();
                mPrinter.printText("Reference No.: "+rferencesnumber);
                mPrinter.addNewLine();
                mPrinter.printText("Account Balance: "+balance);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Amount: "+amount);
                mPrinter.addNewLine();
                mPrinter.printText("Date & Time: "+data+" "+time);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(Constants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");
                Toast.makeText(Aeps2TransactionStatusActivity.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();

            }
        });

    }

}
