package com.iserveu.aeps.dashboard;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.iserveu.aeps.R;
import com.iserveu.aeps.aeps2.Aeps2Activity;
import com.iserveu.aeps.balanceenquiry.BalanceEnquiryContract;
import com.iserveu.aeps.balanceenquiry.BalanceEnquiryPresenter;
import com.iserveu.aeps.balanceenquiry.BalanceEnquiryRequestModel;
import com.iserveu.aeps.balanceenquiry.BalanceEnquiryResponse;
import com.iserveu.aeps.bankspinner.BankNameListActivity;
import com.iserveu.aeps.bankspinner.BankNameModel;
import com.iserveu.aeps.cashwithdrawal.CashWithDrawalContract;
import com.iserveu.aeps.cashwithdrawal.CashWithdrawalPresenter;
import com.iserveu.aeps.cashwithdrawal.CashWithdrawalRequestModel;
import com.iserveu.aeps.cashwithdrawal.CashWithdrawalResponse;
import com.iserveu.aeps.deposit.CashDepositContract;
import com.iserveu.aeps.deposit.CashDepositPresenter;
import com.iserveu.aeps.deposit.CashDepositRequestModel;
import com.iserveu.aeps.fingerprintmodel.DeviceInfo;
import com.iserveu.aeps.fingerprintmodel.MorphoDeviceInfo;
import com.iserveu.aeps.fingerprintmodel.MorphoPidData;
import com.iserveu.aeps.fingerprintmodel.Opts;
import com.iserveu.aeps.fingerprintmodel.PidData;
import com.iserveu.aeps.fingerprintmodel.PidOptions;
import com.iserveu.aeps.fingerprintmodel.uid.AuthReq;
import com.iserveu.aeps.fingerprintmodel.uid.AuthRes;
import com.iserveu.aeps.fingerprintmodel.uid.Meta;
import com.iserveu.aeps.fingerprintmodel.uid.Uses;
import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.login.LoginContract;
import com.iserveu.aeps.login.LoginPresenter;
import com.iserveu.aeps.main2activity.Main2Activity;
import com.iserveu.aeps.maskedittext.MaskedEditText;
import com.iserveu.aeps.microatm.MicroAtmActivity;
import com.iserveu.aeps.refund.RefundContract;
import com.iserveu.aeps.refund.RefundPresenter;
import com.iserveu.aeps.refund.RefundRequestModel;
import com.iserveu.aeps.settings.SettingsActivity;
import com.iserveu.aeps.signer.XMLSigner;
import com.iserveu.aeps.transaction.ReportActivity;
import com.iserveu.aeps.transactionstatus.TransactionStatusActivity;
import com.iserveu.aeps.transactionstatus.TransactionStatusModel;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.SharePreferenceClass;
import com.iserveu.aeps.utils.Util;
import com.moos.library.HorizontalProgressView;
import com.rw.loadingdialog.LoadingView;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.iserveu.aeps.utils.Util.showProgress;


public class DashboardActivity extends AppCompatActivity implements BalanceContract.View,BalanceEnquiryContract.View,
        CashDepositContract.View,CashWithDrawalContract.View,RefundContract.View,LoginContract.View {

    private LoginPresenter loginPresenter;

    LoadingView loadingView;
    private PidData pidData;
    private MorphoPidData morphoPidData;
    private Serializer serializer = null;
    private ArrayList<String> positions;
    String tag = "";
    private UsbDevice usbDevice;
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String morphodeviceid="SAGEM SA";
    String mantradeviceid="MANTRA";
    String morphoe2device="Morpho";
    UsbManager musbManager;
    LinearLayout tabLayout;
    private MaskedEditText aadharNumber,withdrawalAadharNumber,balanceAadharNumber,withdrawalAadharUID,balanceAadharVID;
    private TextView balanceMoney,cashDepositButton,cashWithdrawalButton,balanceEnquiryExpandButton,refundMoneyExpandButton
            ,withdrawalNote,fingerprintStrengthWithdrawal,fingerprintStrengthBalance,balanceNote,fingerprintStrengthDeposit
            ,depositNote,aepsTabOption,matmTabOption,aeps2Option;
    private ExpandableLayout cashDepositExpandableLayout,cashWithdrawalExpandableLayout,balanceEnquiryExpandableLayout,
            refundMoneyExpandableLayout;
    private EditText mobileNumber,bankspinner,amountEnter,withdrawalMobileNumber,
            withdrawalBankspinner,withdrawalAmountEnter,balanceMobileNumber,
            balanceBankspinner,apiTidNumber;
    private ImageView fingerprint,withdrawalFingerprint,balanceFingerprint;
    private HorizontalProgressView withdrawalBar,balanceEnqureyBar,depositBar;
    private Button submitButton,withdrawalSubmitButton,balanceSubmitButton,refundSubmitButton;



    private BalancePresenter mActionsListener;
    private BalanceEnquiryPresenter balanceEnquiryPresenter;
    private CashDepositPresenter cashDepositPresenter;
    private CashWithdrawalPresenter cashWithdrawalPresenter;
    private RefundPresenter refundPresenter;

    private boolean isStartDate = false;

    Session session;


    BalanceEnquiryRequestModel balanceEnquiryRequestModel;
    CashDepositRequestModel cashDepositRequestModel;
    CashWithdrawalRequestModel cashWithdrawalRequestModel;
    RefundRequestModel refundRequestModel;

    String bankIINNumber = "";
    ArrayList<String> featurescodesresponse;

    SharePreferenceClass sharePreferenceClass;
    RadioButton aadhar_no_rd,aadhar_uid_rd,bl_aadhar_no_rd,bl_aadhar_uid_rd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_home );
        serializer = new Persister();
        session = new Session(DashboardActivity.this);
        loginPresenter = new LoginPresenter(DashboardActivity.this);
        sharePreferenceClass = new SharePreferenceClass(DashboardActivity.this);

        setToolbar();

        aeps2Option = findViewById ( R.id. aeps2Option );
        matmTabOption = findViewById ( R.id. matmTabOption );
        aepsTabOption = findViewById ( R.id. aepsTabOption );
        tabLayout = findViewById ( R.id. tabLayout );

        aepsTabOption.setVisibility(View.VISIBLE);

        if(Main2Activity.feature_array_list.contains("33") && Main2Activity.feature_array_list.contains("38")){
            matmTabOption.setVisibility(View.VISIBLE);
            aeps2Option.setVisibility(View.VISIBLE);
             tabLayout.setWeightSum(3);
        }else if(!Main2Activity.feature_array_list.contains("33") && Main2Activity.feature_array_list.contains("38")){
            matmTabOption.setVisibility(View.GONE);
            aeps2Option.setVisibility(View.VISIBLE);
            tabLayout.setWeightSum(2);
        }else if(Main2Activity.feature_array_list.contains("33") && !Main2Activity.feature_array_list.contains("38")){
            matmTabOption.setVisibility(View.VISIBLE);
            aeps2Option.setVisibility(View.GONE);
            tabLayout.setWeightSum(2);
        }else if(!Main2Activity.feature_array_list.contains("33") && !Main2Activity.feature_array_list.contains("38")){
            matmTabOption.setVisibility(View.GONE);
            aeps2Option.setVisibility(View.GONE);
            tabLayout.setWeightSum(1);
        }
        balanceMoney = findViewById ( R.id. balanceMoney );
        withdrawalNote = findViewById ( R.id. withdrawalNote );
        fingerprintStrengthWithdrawal = findViewById ( R.id. fingerprintStrengthWithdrawal );
        fingerprintStrengthBalance = findViewById ( R.id. fingerprintStrengthBalance );
        balanceNote = findViewById ( R.id. balanceNote );
        fingerprintStrengthDeposit = findViewById ( R.id. fingerprintStrengthDeposit );
        depositNote = findViewById ( R.id. depositNote );
        depositNote.setVisibility ( View.GONE );
        fingerprintStrengthDeposit.setVisibility ( View.GONE );
        balanceNote.setVisibility ( View.GONE );
        fingerprintStrengthBalance.setVisibility ( View.GONE );
        withdrawalNote.setVisibility ( View.GONE );
        fingerprintStrengthWithdrawal.setVisibility ( View.GONE );

        cashDepositButton = findViewById ( R.id. cashDepositButton );
        cashDepositExpandableLayout = findViewById ( R.id. cashDepositExpandableLayout );
        aadharNumber = findViewById ( R.id. aadharNumber );
        mobileNumber = findViewById ( R.id. mobileNumber );
        bankspinner = findViewById ( R.id. bankspinner );
        amountEnter = findViewById ( R.id. amountEnter );
        fingerprint = findViewById ( R.id. fingerprint );
        submitButton = findViewById ( R.id. submitButton );
        depositBar = findViewById ( R.id. depositBar );
        depositBar.setVisibility ( View.GONE );

        cashWithdrawalButton = findViewById ( R.id. cashWithdrawalButton );
        cashWithdrawalExpandableLayout = findViewById ( R.id. cashWithdrawalExpandableLayout );
        withdrawalAadharNumber = findViewById ( R.id. withdrawalAadharNumber );
        withdrawalAadharUID = findViewById ( R.id. withdrawalAadharUID );
        withdrawalMobileNumber = findViewById ( R.id. withdrawalMobileNumber );
        withdrawalBankspinner = findViewById ( R.id. withdrawalBankspinner );
        withdrawalAmountEnter = findViewById ( R.id. withdrawalAmountEnter );
        withdrawalFingerprint = findViewById ( R.id. withdrawalFingerprint );
        withdrawalSubmitButton = findViewById ( R.id. withdrawalSubmitButton );
        aadhar_no_rd = findViewById(R.id.aadhar_no_rd);
        aadhar_uid_rd = findViewById(R.id.aadhar_uid_rd);
        withdrawalBar = findViewById ( R.id. withdrawalBar );
        withdrawalBar.setVisibility ( View.GONE );

        balanceEnquiryExpandButton = findViewById ( R.id. balanceEnquiryExpandButton );
        balanceEnquiryExpandableLayout = findViewById ( R.id. balanceEnquiryExpandableLayout );
        balanceAadharNumber = findViewById ( R.id. balanceAadharNumber );
        balanceAadharVID = findViewById ( R.id. balanceAadharVID );
        balanceBankspinner = findViewById ( R.id. balanceBankspinner );
        balanceFingerprint = findViewById ( R.id. balanceFingerprint );
        balanceSubmitButton = findViewById ( R.id. balanceSubmitButton );
        balanceMobileNumber = findViewById ( R.id. balanceMobileNumber );
        balanceEnqureyBar = findViewById ( R.id. balanceEnqureyBar );
        bl_aadhar_no_rd = findViewById(R.id.bl_aadhar_no_rd);
        bl_aadhar_uid_rd = findViewById(R.id.bl_aadhar_uid_rd);
        balanceEnqureyBar.setVisibility ( View.GONE );


        refundMoneyExpandButton = findViewById ( R.id. refundMoneyExpandButton );
        refundMoneyExpandableLayout = findViewById ( R.id. refundMoneyExpandableLayout );
        apiTidNumber = findViewById ( R.id. apiTidNumber );
        refundSubmitButton = findViewById ( R.id. refundSubmitButton );

        mActionsListener = new BalancePresenter(DashboardActivity.this);
        mActionsListener.loadBalance(session.getUserToken());

        balanceMobileNumber.addTextChangedListener(mWatcher);
        balanceAadharNumber.addTextChangedListener(mWatcher);
        balanceBankspinner.addTextChangedListener(mWatcher);


        amountEnter.addTextChangedListener(cashDepositWatcher);
        aadharNumber.addTextChangedListener(cashDepositWatcher);
        mobileNumber.addTextChangedListener(cashDepositWatcher);
        bankspinner.addTextChangedListener(cashDepositWatcher);

        withdrawalMobileNumber.addTextChangedListener(cashWithdrawalWatcher);
        withdrawalAadharNumber.addTextChangedListener(cashWithdrawalWatcher);
        withdrawalAmountEnter.addTextChangedListener(cashWithdrawalWatcher);
        withdrawalBankspinner.addTextChangedListener(cashWithdrawalWatcher);


        apiTidNumber.addTextChangedListener(refundWatcher);

        positions = new ArrayList<>();
        positions = new ArrayList<>();


        /*
         *
         * expandable layout
         *
         * if one layout expand then rest other layout present in the activity will get collapse
         */


        matmTabOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(DashboardActivity.this, MicroAtmActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(in);
                overridePendingTransition(0,0);

                finish();
            }
        });
        aeps2Option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DashboardActivity.this, Aeps2Activity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(in);
                overridePendingTransition(0,0);
                finish();
            }
        });
        cashDepositButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fingerprint.setEnabled ( true );
                fingerprint.setBackgroundColor ( Color.BLUE );
                releaseData();
                if (cashDepositExpandableLayout.isExpanded()) {
                    cashDepositExpandableLayout.collapse();
                    cashWithdrawalExpandableLayout.collapse();
                    balanceEnquiryExpandableLayout.collapse();
                    refundMoneyExpandableLayout.collapse();
                } else {
                    cashDepositExpandableLayout.expand();
                    cashWithdrawalExpandableLayout.collapse();
                    balanceEnquiryExpandableLayout.collapse();
                    refundMoneyExpandableLayout.collapse();

                }
            }
        });
        cashWithdrawalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawalFingerprint.setEnabled ( true );
                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                releaseData();
                if (cashWithdrawalExpandableLayout.isExpanded()) {
                    cashWithdrawalExpandableLayout.collapse();
                    cashDepositExpandableLayout.collapse();
                    balanceEnquiryExpandableLayout.collapse();
                    refundMoneyExpandableLayout.collapse();
                } else {
                    cashWithdrawalExpandableLayout.expand();
                    cashDepositExpandableLayout.collapse();
                    balanceEnquiryExpandableLayout.collapse();
                    refundMoneyExpandableLayout.collapse();
                }
            }
        });

        refundSubmitButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                if(apiTidNumber.getText ()==null|| apiTidNumber.getText ().toString ().trim().matches ( "" )){
                    apiTidNumber.setError ( getResources ().getString ( R.string.apitdierror ) );
                    return;
                }
                refundRequestModel = new RefundRequestModel(apiTidNumber.getText().toString().trim(),session.getFreshnessFactor());
                hideKeyboard();
                refundPresenter = new RefundPresenter(DashboardActivity.this);
                refundPresenter.performRefund(session.getUserToken(), refundRequestModel);

            }
        } );


        balanceEnquiryExpandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                balanceFingerprint.setEnabled ( true );
                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                releaseData();

                if (balanceEnquiryExpandableLayout.isExpanded()) {
                    balanceEnquiryExpandableLayout.collapse();
                    cashWithdrawalExpandableLayout.collapse();
                    cashDepositExpandableLayout.collapse();
                    refundMoneyExpandableLayout.collapse();
                } else {
                    balanceEnquiryExpandableLayout.expand();
                    cashDepositExpandableLayout.collapse();
                    cashWithdrawalExpandableLayout.collapse();
                    refundMoneyExpandableLayout.collapse();
                }
            }
        });
        refundMoneyExpandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseData();

                if (refundMoneyExpandableLayout.isExpanded()) {
                    refundMoneyExpandableLayout.collapse();
                    cashWithdrawalExpandableLayout.collapse();
                    cashDepositExpandableLayout.collapse();
                    balanceEnquiryExpandableLayout.collapse();
                } else {
                    refundMoneyExpandableLayout.expand();
                    cashDepositExpandableLayout.collapse();
                    cashWithdrawalExpandableLayout.collapse();
                    balanceEnquiryExpandableLayout.collapse();
                }
            }
        });


        bankspinner.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                Intent in = new Intent(DashboardActivity.this, BankNameListActivity.class);
                startActivityForResult(in, Constants.REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE);

            }
        } );

        withdrawalBankspinner.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                Intent in = new Intent(DashboardActivity.this, BankNameListActivity.class);
                startActivityForResult(in, Constants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE);

            }
        } );

        balanceBankspinner.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                Intent in = new Intent(DashboardActivity.this, BankNameListActivity.class);
                startActivityForResult(in, Constants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE);

            }
        } );


        fingerprint.setOnClickListener ( new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                showLoader ();
                fingerprint.setEnabled ( false );
                fingerprint.setBackgroundColor ( Color.GRAY );

                if(sharePreferenceClass.getUsbDevice() !=null) {
                    deviceSerialNumber = sharePreferenceClass.getUsbDevice().trim();
                    if (sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( mantradeviceid )) {
                        capture ();
                    } else if (sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( morphodeviceid )||sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( morphoe2device )) {
                        morophoCapture ();
                    }else{
                        Toast.makeText(DashboardActivity.this, getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    deviceConnectMessgae ();
                }

            }
        } );

        withdrawalFingerprint.setOnClickListener ( new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                showLoader ();
                withdrawalFingerprint.setEnabled ( false );
                withdrawalFingerprint.setBackgroundColor ( Color.GRAY );

                if(sharePreferenceClass.getUsbDevice() !=null) {
                    deviceSerialNumber = sharePreferenceClass.getUsbDevice().trim();
                    if (sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( mantradeviceid )) {
                        capture ();
                    } else if (sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( morphodeviceid )||sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( morphoe2device )) {
                        morophoCapture ();
                    }else{
                        Toast.makeText(DashboardActivity.this, getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    deviceConnectMessgae ();
                }
            }
        } );

        balanceFingerprint.setOnClickListener ( new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                showLoader ();
                balanceFingerprint.setEnabled ( false );
                balanceFingerprint.setBackgroundColor ( Color.GRAY );

                if(sharePreferenceClass.getUsbDevice() !=null) {
                    deviceSerialNumber = sharePreferenceClass.getUsbDevice().trim();
                    if (sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( mantradeviceid )) {
                        capture ();
                    } else if (sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( morphodeviceid )||sharePreferenceClass.getUsbDevice().trim ().equalsIgnoreCase ( morphoe2device )) {
                        morophoCapture ();
                    }else{
                        Toast.makeText(DashboardActivity.this, getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    deviceConnectMessgae ();
                }
            }
        } );

        aadharNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    aadharNumber.setError ( getResources ().getString ( R.string.aadhaarnumber ) );

                }

                if (s.length () > 0) {
                    aadharNumber.setError ( null );
                    String aadharNo = aadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber (aadharNo) == false) {
                        aadharNumber.setError ( getResources ().getString ( R.string.valid_aadhar_error ) );
                    }
                }
            }
        } );

        withdrawalAadharNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    withdrawalAadharNumber.setError ( getResources ().getString ( R.string.aadhaarnumber ) );

                }

                if (s.length () > 0) {
                    withdrawalAadharNumber.setError ( null );
                    String aadharNo = withdrawalAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber (aadharNo) == false) {
                        withdrawalAadharNumber.setError ( getResources ().getString ( R.string.valid_aadhar_error ) );
                    }
                }
            }
        } );

        withdrawalAadharUID.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    withdrawalAadharUID.setError ( getResources ().getString ( R.string.aadhaarnumber_vid ) );

                }

                if (s.length () > 0) {
                    withdrawalAadharUID.setError ( null );
                    String aadharNo = withdrawalAadharUID.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID (aadharNo) == false) {
                        withdrawalAadharUID.setError ( getResources ().getString ( R.string.valid_aadhar__uid_error ) );
                    }
                }
            }
        } );

        balanceAadharNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    balanceAadharNumber.setError ( getResources ().getString ( R.string.aadhaarnumber ) );

                }

                if (s.length () > 0) {
                    balanceAadharNumber.setError ( null );
                    String aadharNo = balanceAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber (aadharNo) == false) {
                        balanceAadharNumber.setError ( getResources ().getString ( R.string.valid_aadhar_error ) );
                    }
                }
            }
        } );

        balanceAadharVID.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    balanceAadharVID.setError ( getResources ().getString ( R.string.aadhaarnumber_vid ) );

                }

                if (s.length () > 0) {
                    balanceAadharVID.setError ( null );
                    String aadharNo = balanceAadharVID.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID (aadharNo) == false) {
                        balanceAadharVID.setError ( getResources ().getString ( R.string.valid_aadhar__uid_error ) );
                    }
                }
            }
        } );

        apiTidNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    apiTidNumber.setError ( getResources ().getString ( R.string.apitid ) );
                }
                if (s.length () > 0) {
                    apiTidNumber.setError ( null );
                }
            }
        } );

        bankspinner.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    bankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                }
                if (s.length () > 0) {
                    bankspinner.setError ( null );
                }
            }
        } );

        withdrawalBankspinner.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    withdrawalBankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                }
                if (s.length () > 0) {
                    withdrawalBankspinner.setError ( null );
                }
            }
        } );

        balanceBankspinner.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    balanceBankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                }
                if (s.length () > 0) {
                    balanceBankspinner.setError ( null );
                }
            }
        } );


        amountEnter.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    amountEnter.setError ( getResources ().getString ( R.string.amount_error ) );
                }
                if (s.length () > 0) {
                    amountEnter.setError ( null );
                }
            }
        } );

        withdrawalAmountEnter.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 1) {
                    withdrawalAmountEnter.setError ( getResources ().getString ( R.string.amount_error ) );
                }
                if (s.length () > 0) {
                    withdrawalAmountEnter.setError ( null );
                }
            }
        } );


        mobileNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 10) {
                    mobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                }
                if (s.length () > 0) {
                    mobileNumber.setError ( null );
                    String x = s.toString ();
                    if (x.startsWith ( "0" )|| Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == false) {
                        mobileNumber.setError ( getResources ().getString ( R.string.mobilevaliderror ) );
                    }
                }
            }
        } );

        withdrawalMobileNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 10) {
                    withdrawalMobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                }
                if (s.length () > 0) {
                    withdrawalMobileNumber.setError ( null );
                    String x = s.toString ();
                    if (x.startsWith ( "0" )|| Util.isValidMobile ( withdrawalMobileNumber.getText ().toString ().trim () ) == false) {
                        withdrawalMobileNumber.setError ( getResources ().getString ( R.string.mobilevaliderror ) );
                    }
                }
            }
        } );

        balanceMobileNumber.addTextChangedListener ( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length () < 10) {
                    balanceMobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                }
                if (s.length () > 0) {
                    balanceMobileNumber.setError ( null );
                    String x = s.toString ();
                    if (x.startsWith ( "0" )|| Util.isValidMobile ( balanceMobileNumber.getText ().toString ().trim () ) == false) {
                        balanceMobileNumber.setError ( getResources ().getString ( R.string.mobilevaliderror ) );
                    }
                }
            }
        } );



        submitButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                String aadharNo = aadharNumber.getText().toString();
                if (aadharNo.contains("-")) {
                    aadharNo = aadharNo.replaceAll("-", "").trim();
                }
                if (Util.validateAadharNumber (aadharNo) == false) {
                    aadharNumber.setError( getResources ().getString ( R.string.valid_aadhar_error ) );
                    return;
                }
                if (mobileNumber.getText () == null || mobileNumber.getText ().toString ().trim ().matches ( "" )|| Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == false) {
                    mobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                    return;
                }
                String panaaadhaar = mobileNumber.getText ().toString ().trim ();
                if ( !panaaadhaar.contains (" ") && panaaadhaar.length () == 10  ){
                }else{
                    mobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                    return;
                }
                if(bankspinner.getText ()==null|| bankspinner.getText ().toString ().trim().matches ( "" )){
                    bankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                    return;
                }
                if(amountEnter.getText ()==null|| amountEnter.getText ().toString ().trim().matches ( "" )){
                    amountEnter.setError ( getResources ().getString ( R.string.amount_error ) );
                    return;
                }
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }
                    if (!pidData._Resp.errCode.equals ( "0" )) {
                        Log.e ( "Error: ", pidData._Resp.errInfo );
                    } else {
                        hideKeyboard ();
                        Util.showAlert ( DashboardActivity.this, getResources ().getString ( R.string.fail_error ), getResources ().getString ( R.string.cash_deposit_error ) );
                        tag = "1";
                        releaseData ();
                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }
                    if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                    } else {
                        hideKeyboard ();
                        Util.showAlert ( DashboardActivity.this, getResources ().getString ( R.string.fail_error ), getResources ().getString ( R.string.cash_deposit_error ) );
                        tag = "1";
                        releaseData ();
                    }
                }
            }
        } );


        withdrawalSubmitButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                String withdrawalaadharNo="";
                if(aadhar_no_rd.isChecked()) {
                    withdrawalaadharNo = withdrawalAadharNumber.getText().toString();
                    if (withdrawalaadharNo.contains("-")) {
                        withdrawalaadharNo = withdrawalaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(withdrawalaadharNo) == false) {
                        withdrawalAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }

                if(aadhar_uid_rd.isChecked()) {
                    withdrawalaadharNo = withdrawalAadharUID.getText().toString();
                    if (withdrawalaadharNo.contains("-")) {
                        withdrawalaadharNo = withdrawalaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(withdrawalaadharNo) == false) {
                        withdrawalAadharUID.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }



                if (withdrawalMobileNumber.getText () == null || withdrawalMobileNumber.getText ().toString ().trim ().matches ( "" )|| Util.isValidMobile ( withdrawalMobileNumber.getText ().toString ().trim () ) == false) {
                    withdrawalMobileNumber.setError(getResources().getString(R.string.mobileerror));
                    return;
                }
                String panaaadhaar = withdrawalMobileNumber.getText ().toString ().trim ();
                if ( !panaaadhaar.contains (" ") && panaaadhaar.length () == 10  ){
                }else{
                    withdrawalMobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                    return;
                }
                if(withdrawalBankspinner.getText ()==null|| withdrawalBankspinner.getText ().toString ().trim().matches ( "" )){
                    withdrawalBankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                    return;
                }
                if(withdrawalAmountEnter.getText ()==null|| withdrawalAmountEnter.getText ().toString ().trim().matches ( "" )){
                    withdrawalAmountEnter.setError ( getResources ().getString ( R.string.amount_error ) );
                    return;
                }
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                    }
                    if (!pidData._Resp.errCode.equals ( "0" )) {
                    } else {
                        hideKeyboard ();
                        tag = "2";
                        new AuthRequest ( withdrawalaadharNo, pidData ).executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR );

                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                    }
                    if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                    } else {
                        hideKeyboard ();
                        tag = "2";
                        new AuthRequestMorpho ( withdrawalaadharNo, morphoPidData ).executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR );
                    }
                }
            }
        } );

        balanceSubmitButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                showLoader ();
                String balanceaadharNo = "";
                if(bl_aadhar_no_rd.isChecked()) {
                    balanceaadharNo = balanceAadharNumber.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(balanceaadharNo) == false) {
                        balanceAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }

                if(bl_aadhar_uid_rd.isChecked()) {
                    balanceaadharNo = balanceAadharVID.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(balanceaadharNo) == false) {
                        balanceAadharVID.setError(getResources().getString(R.string.valid_aadhar__uid_error));
                        return;
                    }
                }

                if (balanceMobileNumber.getText () == null || balanceMobileNumber.getText ().toString ().trim ().matches ( "" )|| Util.isValidMobile ( balanceMobileNumber.getText ().toString ().trim () ) == false) {
                    balanceMobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                    return;
                }else{
                    String panaaadhaar = balanceMobileNumber.getText ().toString ().trim ();
                    if ( !panaaadhaar.contains (" ") && panaaadhaar.length () == 10  ){
                    }else{
                        balanceMobileNumber.setError ( getResources ().getString ( R.string.mobileerror ) );
                        return;
                    }
                }
                if(balanceBankspinner.getText ()==null|| balanceBankspinner.getText ().toString ().trim().matches ( "" )){
                    balanceBankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                    return;
                }
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                    }
                    if (!pidData._Resp.errCode.equals ( "0" )) {
                    } else {
                        hideKeyboard ();
                        tag = "3";
                        new AuthRequest ( balanceaadharNo, pidData ).executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR );
                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                    }
                    if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                    } else {
                        hideKeyboard ();
                        tag = "3";
                        new AuthRequestMorpho ( balanceaadharNo, morphoPidData ).executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR );
                    }
                }
            }
        } );

        aadhar_no_rd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    aadhar_uid_rd.setChecked(false);
                    withdrawalAadharNumber.setVisibility(View.VISIBLE);
                    withdrawalAadharUID.setVisibility(View.GONE);
                    withdrawalAadharNumber.setText("");
                    withdrawalAadharUID.setText("");
                }
            }
        });

        aadhar_uid_rd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    aadhar_no_rd.setChecked(false);
                    withdrawalAadharNumber.setVisibility(View.GONE);
                    withdrawalAadharUID.setVisibility(View.VISIBLE);
                    withdrawalAadharNumber.setText("");
                    withdrawalAadharUID.setText("");
                }
            }
        });

        bl_aadhar_no_rd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    bl_aadhar_uid_rd.setChecked(false);
                    balanceAadharNumber.setVisibility(View.VISIBLE);
                    balanceAadharVID.setVisibility(View.GONE);
                    balanceAadharNumber.setText("");
                    balanceAadharVID.setText("");
                }
            }
        });

        bl_aadhar_uid_rd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    bl_aadhar_no_rd.setChecked(false);
                    balanceAadharNumber.setVisibility(View.GONE);
                    balanceAadharVID.setVisibility(View.VISIBLE);
                    balanceAadharNumber.setText("");
                    balanceAadharVID.setText("");
                }
            }
        });
    }

    private void morphoMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(DashboardActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(DashboardActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.morpho))
                .setMessage(getResources().getString(R.string.install_morpho_message))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         * play store intent
                         */
                        final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private void mantraMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(DashboardActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(DashboardActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void rdserviceMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(DashboardActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(DashboardActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_install))
                .setMessage(getResources().getString(R.string.mantra_rd_service))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }


    private  void installcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");
        if(isAppInstalled) {
// This intent will help you to launch if the package is already installed
            if (serviceAppInstalled){
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.INFO");
                intent.setPackage ( "com.mantra.rdservice" );
                startActivityForResult ( intent, 1 );
            }else{
                rdserviceMessage ();

            }
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage ();
        }
    }
    private  void morphoinstallcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");
        if(isAppInstalled) {
//This intent will help you to launch if the package is already installed
            Intent intent1 = new Intent();
            intent1.setAction ( "in.gov.uidai.rdservice.fp.INFO" );
            intent1.setPackage ( "com.scl.rdservice" );
//            intent1.addFlags ( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivityForResult ( intent1, 3 );
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            morphoMessage ();
        }
    }

    /*
   app installation check
   */
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy ();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void devicecheck() {
        if(usbDevice == null) {
            deviceConnectMessgae ();
        }else {
            if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( mantradeviceid )) {
                installcheck ();
            } else if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphodeviceid )||usbDevice.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )) {
                morphoinstallcheck ();
            }
        }
    }
    private void deviceConnectMessgae (){
        hideLoader();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(DashboardActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(DashboardActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect))
                .setMessage(getResources().getString(R.string.setting_device))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        fingerprint.setEnabled ( true );
                        withdrawalFingerprint.setEnabled ( true );
                        balanceFingerprint.setEnabled ( true );
                        fingerprint.setBackgroundColor ( Color.BLUE);
                        withdrawalFingerprint.setBackgroundColor ( Color.BLUE);
                        balanceFingerprint.setBackgroundColor ( Color.BLUE);
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        hideLoader ();
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            // Toast.makeText(DashboardActivity.this, "No Devices Currently Connected" + usbconnted, Toast.LENGTH_LONG).show();
            deviceConnectMessgae ();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(device !=null && device.getManufacturerName () != null){
                        if(device.getManufacturerName ().equalsIgnoreCase ( mantradeviceid )||device.getManufacturerName ().equalsIgnoreCase ( morphodeviceid )||device.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )){
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName();
                        }
                    }
                }
            }
            devicecheck ();
        }
    }

    @Override
    public void showBalance(String balance) {
        balanceMoney.setText(balance);

    }

    @Override
    public void emptyLogin() {
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE) {
            hideLoader ();
            if(resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                balanceBankspinner.setText(bankIINValue.getBankName());
                bankIINNumber = bankIINValue.getIin();
                checkBalanceEnquiryValidation();
            }

            checkBalanceEnquiryValidation();

        }else if (requestCode == Constants.REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE) {
            hideLoader ();
            if(resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                bankspinner.setText(bankIINValue.getBankName());
                bankIINNumber = bankIINValue.getIin();
                checkCashDepositValidation();
            }
            checkCashDepositValidation();
        }
        else if (requestCode == Constants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE) {
            hideLoader ();
            if(resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                withdrawalBankspinner.setText(bankIINValue.getBankName());
                bankIINNumber = bankIINValue.getIin();
                checkWithdrawalValidation();

            }
            checkWithdrawalValidation();
        }else if (requestCode == Constants.BALANCE_RELOAD) {
            if(resultCode == 0) {
                mActionsListener = new BalancePresenter(DashboardActivity.this);
                mActionsListener.loadBalance(session.getUserToken());
                //loginPresenter.performLogin ( session.getUserName (),session.getPassword () );
            }
        }
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "DEVICE_INFO" );
                            String rdService = data.getStringExtra ( "RD_SERVICE_INFO" );
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.hide();
                        }
                    }
                    if (withdrawalMobileNumber.getText()!=null && !withdrawalMobileNumber.getText().toString().matches("")){
                        checkWithdrawalValidation();
                    }else if (balanceMobileNumber.getText()!=null && !balanceMobileNumber.getText().toString().matches("")){
                        checkBalanceEnquiryValidation();
                    }else if (mobileNumber.getText()!=null && !mobileNumber.getText().toString().matches("")){
                        checkCashDepositValidation();
                    }}
                break;
            case 2:
                if(loadingView!=null){
                    loadingView.hide();
                }
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );
                            if (result != null) {
                                pidData = serializer.read ( PidData.class, result );

                                if(Float.parseFloat ( pidData._Resp.qScore ) <=60){
                                    withdrawalBar.setVisibility ( View.VISIBLE );
                                    withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    withdrawalBar.setProgressTextMoved ( true );
                                    withdrawalBar.setEndColor ( getResources ().getColor ( R.color.red ) );
                                    withdrawalBar.setStartColor ( getResources ().getColor ( R.color.red ) );
                                    withdrawalNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );

                                    balanceEnqureyBar.setVisibility ( View.VISIBLE );
                                    balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    balanceEnqureyBar.setProgressTextMoved ( true );
                                    balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.red ) );
                                    balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.red ) );
                                    balanceNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthBalance.setVisibility ( View.VISIBLE );

                                    depositBar.setVisibility ( View.VISIBLE );
                                    depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    depositBar.setProgressTextMoved ( true );
                                    depositBar.setEndColor ( getResources ().getColor ( R.color.red ) );
                                    depositBar.setStartColor ( getResources ().getColor ( R.color.red ) );
                                    depositNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthDeposit.setVisibility ( View.VISIBLE );

                                }else if(Float.parseFloat ( pidData._Resp.qScore ) >=60 && Float.parseFloat ( pidData._Resp.qScore ) <=70){
                                    withdrawalBar.setVisibility ( View.VISIBLE );
                                    withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    withdrawalBar.setProgressTextMoved ( true );
                                    withdrawalBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
                                    withdrawalBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
                                    withdrawalNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );

                                    balanceEnqureyBar.setVisibility ( View.VISIBLE );
                                    balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    balanceEnqureyBar.setProgressTextMoved ( true );
                                    balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
                                    balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
                                    balanceNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthBalance.setVisibility ( View.VISIBLE );

                                    depositBar.setVisibility ( View.VISIBLE );
                                    depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    depositBar.setProgressTextMoved ( true );
                                    depositBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
                                    depositBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
                                    depositNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthDeposit.setVisibility ( View.VISIBLE );
                                }else{
                                    withdrawalBar.setVisibility ( View.VISIBLE );
                                    withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    withdrawalBar.setProgressTextMoved ( true );
                                    withdrawalBar.setEndColor ( getResources ().getColor ( R.color.green ) );
                                    withdrawalBar.setStartColor ( getResources ().getColor ( R.color.green ) );
                                    withdrawalNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );

                                    balanceEnqureyBar.setVisibility ( View.VISIBLE );
                                    balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    balanceEnqureyBar.setProgressTextMoved ( true );
                                    balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.green ) );
                                    balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.green ) );
                                    balanceNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthBalance.setVisibility ( View.VISIBLE );

                                    depositBar.setVisibility ( View.VISIBLE );
                                    depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
                                    depositBar.setProgressTextMoved ( true );
                                    depositBar.setEndColor ( getResources ().getColor ( R.color.green ) );
                                    depositBar.setStartColor ( getResources ().getColor ( R.color.green ) );
                                    depositNote.setVisibility ( View.VISIBLE );
                                    fingerprintStrengthDeposit.setVisibility ( View.VISIBLE );
                                }
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.device_is_not_ready_error));
                            }
                            if (pidData._Resp.errCode.equalsIgnoreCase("720")){
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo );
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "0" ) && Float.parseFloat ( pidData._Resp.qScore )>=60){
                                fingerprint.setEnabled ( false );
                                withdrawalFingerprint.setEnabled ( false );
                                balanceFingerprint.setEnabled ( false );
                                Util.showAlert ( DashboardActivity.this,getResources ().getString ( R.string.success ),getResources ().getString ( R.string.capture_success ) );
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "700" )){
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo + " " + ". Please Try Again !!!");
                            }else if(pidData._Resp.errCode.equalsIgnoreCase ( "730" )) {
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),"Capture stopped or Aborted. Please Try Again !!!");
                            }else if(Float.parseFloat ( pidData._Resp.qScore ) <=60){
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo+ " " +"with a score less then 60% " + ". Please Try Again !!!");
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),pidData._Resp.errInfo+ " " + ". Please Try Again !!!");
                            }

                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.hide();
                        }
                        fingerprint.setEnabled ( true );
                        fingerprint.setBackgroundColor ( Color.BLUE );
                        withdrawalFingerprint.setEnabled ( true );
                        withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                        balanceFingerprint.setEnabled ( true );
                        balanceFingerprint.setBackgroundColor ( Color.BLUE );
                        withdrawalBar.setVisibility ( View.GONE );
                        balanceEnqureyBar.setVisibility ( View.GONE );
                        withdrawalNote.setVisibility ( View.GONE );
                        fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                        fingerprintStrengthBalance.setVisibility ( View.GONE );
                        balanceNote.setVisibility ( View.GONE );
                        depositBar.setVisibility ( View.GONE );
                        depositNote.setVisibility ( View.GONE );
                        fingerprintStrengthDeposit.setVisibility ( View.GONE );
                        Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),getResources().getString(R.string.scanning_error));
                    }
                    if (withdrawalMobileNumber.getText()!=null && !withdrawalMobileNumber.getText().toString().matches("")){
                        checkWithdrawalValidation();
                    }else if (balanceMobileNumber.getText()!=null && !balanceMobileNumber.getText().toString().matches("")){
                        checkBalanceEnquiryValidation();
                    }else if (mobileNumber.getText()!=null && !mobileNumber.getText().toString().matches("")){
                        checkCashDepositValidation();
                    }
                }
                break;

            case 3:
                if (resultCode == RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "DEVICE_INFO" );
                            String rdService = data.getStringExtra ( "RD_SERVICE_INFO" );
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.hide();
                        }
                    }
                    if (withdrawalMobileNumber.getText()!=null && !withdrawalMobileNumber.getText().toString().matches("")){
                        checkWithdrawalValidation();
                    }else if (balanceMobileNumber.getText()!=null && !balanceMobileNumber.getText().toString().matches("")){
                        checkBalanceEnquiryValidation();
                    }else if (mobileNumber.getText()!=null && !mobileNumber.getText().toString().matches("")){
                        checkCashDepositValidation();
                    }
                }
                break;
            case 4:
                if(loadingView!=null){
                    loadingView.hide();
                }
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );
                            if (result != null) {
                                morphoPidData = serializer.read ( MorphoPidData.class, result );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.device_is_not_ready_error));
                            }
                            if (morphoPidData._Resp.errCode.equalsIgnoreCase("720")){
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo );
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "0" )){
                                fingerprint.setEnabled ( false );
                                withdrawalFingerprint.setEnabled ( false );
                                balanceFingerprint.setEnabled ( false );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert ( DashboardActivity.this,getResources ().getString ( R.string.success ),getResources ().getString ( R.string.capture_success ) );
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "700" )){
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo + " " + ". Please Try Again !!!");
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "730" )) {
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),"Capture stopped or Aborted. Please Try Again !!!");
                            }else{
                                fingerprint.setEnabled ( true );
                                fingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalFingerprint.setEnabled ( true );
                                withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                                balanceFingerprint.setEnabled ( true );
                                balanceFingerprint.setBackgroundColor ( Color.BLUE );
                                withdrawalBar.setVisibility ( View.GONE );
                                balanceEnqureyBar.setVisibility ( View.GONE );
                                withdrawalNote.setVisibility ( View.GONE );
                                fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                                fingerprintStrengthBalance.setVisibility ( View.GONE );
                                balanceNote.setVisibility ( View.GONE );
                                depositBar.setVisibility ( View.GONE );
                                depositNote.setVisibility ( View.GONE );
                                fingerprintStrengthDeposit.setVisibility ( View.GONE );
                                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),morphoPidData._Resp.errInfo+ " " + ". Please Try Again !!!");
                            }
                        }
                    } catch (Exception e) {
                        if(loadingView!=null){
                            loadingView.hide();
                        }
                        fingerprint.setEnabled ( true );
                        fingerprint.setBackgroundColor ( Color.BLUE );
                        withdrawalFingerprint.setEnabled ( true );
                        withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
                        balanceFingerprint.setEnabled ( true );
                        balanceFingerprint.setBackgroundColor ( Color.BLUE );
                        withdrawalBar.setVisibility ( View.GONE );
                        balanceEnqureyBar.setVisibility ( View.GONE );
                        withdrawalNote.setVisibility ( View.GONE );
                        fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
                        fingerprintStrengthBalance.setVisibility ( View.GONE );
                        balanceNote.setVisibility ( View.GONE );
                        depositBar.setVisibility ( View.GONE );
                        depositNote.setVisibility ( View.GONE );
                        fingerprintStrengthDeposit.setVisibility ( View.GONE );
                        Util.showAlert(DashboardActivity.this,getResources().getString(R.string.fail_error),getResources().getString(R.string.scanning_error));

                    }
                    if (withdrawalMobileNumber.getText()!=null && !withdrawalMobileNumber.getText().toString().matches("")){
                        checkWithdrawalValidation();
                    }else if (balanceMobileNumber.getText()!=null && !balanceMobileNumber.getText().toString().matches("")){
                        checkBalanceEnquiryValidation ();
                    }else if (mobileNumber.getText()!=null && !mobileNumber.getText().toString().matches("")){
                        checkCashDepositValidation();
                    }
                }
                break;
        }
    }


    @Override
    public void checkBalanceEnquiryStatus(String status, String message, BalanceEnquiryResponse balanceEnquiryResponse) {
        String aadhar = balanceAadharNumber.getText().toString().trim();
        releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (balanceEnquiryResponse!=null){
            transactionStatusModel.setAadharCard(aadhar);
            transactionStatusModel.setBankName(balanceEnquiryResponse.getBankName());
            transactionStatusModel.setBalanceAmount(balanceEnquiryResponse.getBalance());
            transactionStatusModel.setReferenceNo(balanceEnquiryResponse.getReferenceNo());
            transactionStatusModel.setTransactionType("Balance Enquery");
            transactionStatusModel.setStatus (balanceEnquiryResponse.getStatus ());
            transactionStatusModel.setApiComment (balanceEnquiryResponse.getApiComment ());
            transactionStatusModel.setStatusDesc (balanceEnquiryResponse.getStatusDesc ());
            session.setFreshnessFactor ( balanceEnquiryResponse.getNextFreshnessFactor () );
        }else{
            transactionStatusModel = null;
            session.setFreshnessFactor ( null );

        }
        Intent intent = new Intent(DashboardActivity.this, TransactionStatusActivity.class);
        intent.putExtra(Constants.TRANSACTION_STATUS_KEY,transactionStatusModel);
        startActivityForResult (intent, Constants.BALANCE_RELOAD);
    }

    @Override
    public void checkCashWithdrawalStatus(String status, String message, CashWithdrawalResponse cashWithdrawalResponse) {
        String aadhar = withdrawalAadharNumber.getText().toString().trim();
        String amount = withdrawalAmountEnter.getText ().toString ().trim ();
        releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (cashWithdrawalResponse!=null){
            transactionStatusModel.setAadharCard(aadhar);
            transactionStatusModel.setBankName(cashWithdrawalResponse.getBankName());
            transactionStatusModel.setBalanceAmount(cashWithdrawalResponse.getBalance());
            transactionStatusModel.setReferenceNo(cashWithdrawalResponse.getReferenceNo());
            transactionStatusModel.setTransactionAmount(amount);
            transactionStatusModel.setTransactionType("Cash Withdrawal");
            transactionStatusModel.setStatus (cashWithdrawalResponse.getStatus ());
            transactionStatusModel.setApiComment (cashWithdrawalResponse.getApiComment ());
            transactionStatusModel.setStatusDesc (cashWithdrawalResponse.getStatusDesc ());
            session.setFreshnessFactor ( cashWithdrawalResponse.getNextFreshnessFactor () );
        }else{
            transactionStatusModel = null;
            session.setFreshnessFactor ( null );
        }
        Intent intent = new Intent(DashboardActivity.this, TransactionStatusActivity.class);
        intent.putExtra(Constants.TRANSACTION_STATUS_KEY,transactionStatusModel);
        startActivityForResult (intent, Constants.BALANCE_RELOAD);
    }

    @Override
    public void checkCashDepositStatus(String status, String message) {
        releaseData ();
        Util.showAlert(DashboardActivity.this,"",message);

    }

    @Override
    public void checkRefundStatus(String status, String message) {
        releaseData ();
        Util.showAlert(DashboardActivity.this,"",message);
    }

    @Override
    public void checkLoginStatus(String status, String message, String token, String nextFreshnessFactor) {
        if (status!=null && !status.matches("") && status.matches("0")){
            Session session = new Session(DashboardActivity.this);
            session.setUserToken(token);
            session.setFreshnessFactor(nextFreshnessFactor);
            session.setLoggedIn(true);
        }else{
            Util.showAlert(this,getResources().getString(R.string.alert_error),message);
        }
    }

    @Override
    public void showLoginFeature(ArrayList<UserInfoModel.userFeature> userFeatures, String token) {
        loginPresenter.getLoginDetails(token);
    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {
        String aepsFeatureCode = "27";
        String matmFeatureCode = "33";
        String aeps2FeatureCode = "38";
        /*String aepsFeatureCode = "30";
        String matmFeatureCode = "40";
        String aeps2FeatureCode = "38";*/

        /*ArrayList<String> featureCode = new ArrayList<>();
        featureCode.add(aepsFeatureCode);
        featureCode.add(matmFeatureCode);
        featureCode.add(aeps2FeatureCode);
        featurescodesresponse = new ArrayList<>();

        List<UserInfoModel.userFeature> temp_feature = Util.removeDuplicates(userFeatures);

        for (int i = 0; i < temp_feature.size(); i++) {
            if(featureCode.contains(temp_feature.get(i).getId())) {
                if(aepsFeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                    aepsTabOption.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(aepsFeatureCode);
                    //loginPresenter.performLogin(session.getUserName(),session.getPassword());
                }
                if(matmFeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                    matmTabOption.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(matmFeatureCode);
                }
                if(aeps2FeatureCode.equalsIgnoreCase(temp_feature.get(i).getId())){
                    aeps2Option.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(aeps2FeatureCode);
                }
            } else {
                Log.v("radha", "not contains : "+temp_feature.get(i).getId());
            }
        }
        if(featurescodesresponse.size() == 2){
            tabLayout.setWeightSum(2);
        }else if(featurescodesresponse.size() == 3){
            tabLayout.setWeightSum(3);
        }else{
            tabLayout.setWeightSum(1);
        }*/

    }

    @Override
    public void checkEmptyFields() {
        Toast.makeText(DashboardActivity.this, "Kindly get Registered with AEPS to proceed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {
        if (loadingView ==null){
            loadingView = showProgress(DashboardActivity.this);
        }
        loadingView.show();
    }

    @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }
    }

    /*
     * Biomectirc device's capture data
     */
    private void capture(){
        try {
            String pidOption = getPIDOptions ();
            if (pidOption != null) {
                Intent intent2 = new Intent();
                intent2.setAction ( "in.gov.uidai.rdservice.fp.CAPTURE" );
                intent2.setPackage ( "com.mantra.rdservice" );
                intent2.putExtra ( "PID_OPTIONS", pidOption );
                startActivityForResult ( intent2, 2 );
            }
        } catch (Exception e) {
            if(loadingView!=null){
                loadingView.hide();
            }
            if (withdrawalMobileNumber.getText()!=null && !withdrawalMobileNumber.getText().toString().matches("")){
                checkWithdrawalValidation();
            }else if (balanceMobileNumber.getText()!=null && !balanceMobileNumber.getText().toString().matches("")){
                checkBalanceEnquiryValidation();
            }else if (mobileNumber.getText()!=null && !mobileNumber.getText().toString().matches("")){
                checkCashDepositValidation();
            }
            fingerprint.setEnabled ( true );
            fingerprint.setBackgroundColor ( Color.BLUE );
            withdrawalFingerprint.setEnabled ( true );
            withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
            balanceFingerprint.setEnabled ( true );
            balanceFingerprint.setBackgroundColor ( Color.BLUE );
            withdrawalBar.setVisibility ( View.GONE );
            balanceEnqureyBar.setVisibility ( View.GONE );
            withdrawalNote.setVisibility ( View.GONE );
            fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
            fingerprintStrengthBalance.setVisibility ( View.GONE );
            balanceNote.setVisibility ( View.GONE );
            depositBar.setVisibility ( View.GONE );
            depositNote.setVisibility ( View.GONE );
            Util.showAlert(DashboardActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.setting_device));
        }
    }

    /*
     * Biomectirc device's capture data
     */
    private void morophoCapture(){
        try {
            String pidOption = getPIDOptions ();
            if (pidOption != null) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.CAPTURE");
                intent.setPackage("com.scl.rdservice");
                intent.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent, 4);
            }
        } catch (Exception e) {

            if(loadingView!=null){
                loadingView.hide();
            }
            if (withdrawalMobileNumber.getText()!=null && !withdrawalMobileNumber.getText().toString().matches("")){
                checkWithdrawalValidation();
            }else if (balanceMobileNumber.getText()!=null && !balanceMobileNumber.getText().toString().matches("")){
                checkBalanceEnquiryValidation();
            }else if (mobileNumber.getText()!=null && !mobileNumber.getText().toString().matches("")){
                checkCashDepositValidation();
            }
            fingerprint.setEnabled ( true );
            fingerprint.setBackgroundColor ( Color.BLUE );
            withdrawalFingerprint.setEnabled ( true );
            withdrawalFingerprint.setBackgroundColor ( Color.BLUE );
            balanceFingerprint.setEnabled ( true );
            balanceFingerprint.setBackgroundColor ( Color.BLUE );
            withdrawalBar.setVisibility ( View.GONE );
            balanceEnqureyBar.setVisibility ( View.GONE );
            withdrawalNote.setVisibility ( View.GONE );
            fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
            fingerprintStrengthBalance.setVisibility ( View.GONE );
            balanceNote.setVisibility ( View.GONE );
            depositBar.setVisibility ( View.GONE );
            depositNote.setVisibility ( View.GONE );
            Util.showAlert(DashboardActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.setting_device));

        }

    }

    /*
     * data needed for the biometric device's
     *
     * for device info and the capture of the finger prints
     */
    private String getPIDOptions() {
        try {
            String posh = getResources ().getString ( R.string.posh );
            if (positions.size() > 0) {
                posh = positions.toString().replace("[", "").replace("]", "").replaceAll("[\\s+]", "");
            }

            Opts opts = new Opts();
            opts.fCount = "1";
            opts.fType = "0";
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format ="0";
            opts.pidVer = "2.0";
            opts.timeout = "10000";
            opts.posh = posh;
            opts.env = "P";

            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = "1.0";
            pidOptions.Opts = opts;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
        }
        return null;
    }


    /*
     *validation for the aadharr number and the biometric's pid data
     */

    private class AuthRequest extends AsyncTask<Void, Void, String> {

        private String uid;
        private PidData pidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        DeviceInfo info ;

        private AuthRequest(String uid, PidData pidData) {
            this.uid = uid;
            this.pidData = pidData;
            dialog = new ProgressDialog(DashboardActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                info = pidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = info.rdsId;
                meta.rdsVer = info.rdsVer;
                meta.dpId = info.dpId;
                meta.dc = info.dc;
                meta.mi = info.mi;
                meta.mc = info.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = pidData._Skey;
                authReq.Hmac = pidData._Hmac;
                authReq.data = pidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = session.getFreshnessFactor();

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq!=null && meta!=null && info!=null) {


                if (tag.equalsIgnoreCase("3")) {
                    String vid = null;
                    String uid = null;

                    if(bl_aadhar_no_rd.isChecked()) {
                        uid = balanceAadharNumber.getText().toString();
                        if (uid.contains("-")) {
                            uid = uid.replaceAll("-", "").trim();
                        }
                    }
                    if(bl_aadhar_uid_rd.isChecked()) {
                        vid = balanceAadharVID.getText().toString();
                        if (vid.contains("-")) {
                            vid = vid.replaceAll("-", "").trim();
                        }
                    }

                    balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, balanceMobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    balanceEnquiryPresenter = new BalanceEnquiryPresenter(DashboardActivity.this);
                    balanceEnquiryPresenter.performBalanceEnquiry(session.getUserToken(), balanceEnquiryRequestModel);
                }
                else if (tag.equalsIgnoreCase("2")) {
                    String vid = null;
                    String uid = null;
                    if(aadhar_no_rd.isChecked()) {
                        uid = withdrawalAadharNumber.getText().toString();
                        if (uid.contains("-")) {
                            uid = uid.replaceAll("-", "").trim();
                        }
                    }
                    if(aadhar_uid_rd.isChecked()) {
                        vid = withdrawalAadharUID.getText().toString();
                        if (vid.contains("-")) {
                            vid = vid.replaceAll("-", "").trim();
                        }
                    }

                    cashWithdrawalRequestModel = new CashWithdrawalRequestModel(withdrawalAmountEnter.getText().toString().trim(), uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, withdrawalMobileNumber.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    cashWithdrawalPresenter = new CashWithdrawalPresenter(DashboardActivity.this);
                    cashWithdrawalPresenter.performCashWithdrawal(session.getUserToken(), cashWithdrawalRequestModel);

                }
                else if (tag.equalsIgnoreCase("1")) {
                    String aadharNo = aadharNumber.getText ().toString ();
                    if (aadharNo.contains ( "-" )) {
                        aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
                    }
                    cashDepositRequestModel = new CashDepositRequestModel(amountEnter.getText().toString().trim(),aadharNo, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "DEPOSIT", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    cashDepositPresenter = new CashDepositPresenter(DashboardActivity.this);
                    cashDepositPresenter.performCashDeposit(session.getUserToken(), cashDepositRequestModel);
                }
            }else{
                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }
   /* private class AuthRequest extends AsyncTask<Void, Void, String> {

        private String uid;
        private PidData pidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        DeviceInfo info ;

        private AuthRequest(String uid, PidData pidData) {
            this.uid = uid;
            this.pidData = pidData;
            dialog = new ProgressDialog(DashboardActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                info = pidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = info.rdsId;
                meta.rdsVer = info.rdsVer;
                meta.dpId = info.dpId;
                meta.dc = info.dc;
                meta.mi = info.mi;
                meta.mc = info.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = pidData._Skey;
                authReq.Hmac = pidData._Hmac;
                authReq.data = pidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = session.getFreshnessFactor();

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq!=null && meta!=null && info!=null) {


                if (tag.equalsIgnoreCase("3")) {
                    String vid = null;
                    String uid = null;

                    if(bl_aadhar_no_rd.isChecked()) {
                        uid = balanceAadharNumber.getText().toString();
                        if (uid.contains("-")) {
                            uid = uid.replaceAll("-", "").trim();
                        }
                    }
                    if(bl_aadhar_uid_rd.isChecked()) {
                        vid = balanceAadharVID.getText().toString();
                        if (vid.contains("-")) {
                            vid = vid.replaceAll("-", "").trim();
                        }
                    }

                    balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, balanceMobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    balanceEnquiryPresenter = new BalanceEnquiryPresenter(DashboardActivity.this);
                    balanceEnquiryPresenter.performBalanceEnquiry(session.getUserToken(), balanceEnquiryRequestModel);
                }
                else if (tag.equalsIgnoreCase("2")) {
                    String vid = null;
                    String uid = null;
                    if(aadhar_no_rd.isChecked()) {
                        uid = withdrawalAadharNumber.getText().toString();
                        if (uid.contains("-")) {
                            uid = uid.replaceAll("-", "").trim();
                        }
                    }
                    if(aadhar_uid_rd.isChecked()) {
                        vid = withdrawalAadharUID.getText().toString();
                        if (vid.contains("-")) {
                            vid = vid.replaceAll("-", "").trim();
                        }
                    }

                    cashWithdrawalRequestModel = new CashWithdrawalRequestModel(withdrawalAmountEnter.getText().toString().trim(), uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, withdrawalMobileNumber.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    cashWithdrawalPresenter = new CashWithdrawalPresenter(DashboardActivity.this);
                    cashWithdrawalPresenter.performCashWithdrawal(session.getUserToken(), cashWithdrawalRequestModel);

                }
                else if (tag.equalsIgnoreCase("1")) {
                    String aadharNo = aadharNumber.getText ().toString ();
                    if (aadharNo.contains ( "-" )) {
                        aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
                    }
                    cashDepositRequestModel = new CashDepositRequestModel(amountEnter.getText().toString().trim(),aadharNo, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "DEPOSIT", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    cashDepositPresenter = new CashDepositPresenter(DashboardActivity.this);
                    cashDepositPresenter.performCashDeposit(session.getUserToken(), cashDepositRequestModel);
                }
            }else{
                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }*/

    private class AuthRequestMorpho extends AsyncTask<Void, Void, String> {

        private String uid;
        private MorphoPidData morphoPidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        MorphoDeviceInfo morphoDeviceInfo ;

        private AuthRequestMorpho(String uid, MorphoPidData morphoPidData) {
            this.uid = uid;
            this.morphoPidData=morphoPidData;
            dialog = new ProgressDialog(DashboardActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                morphoDeviceInfo = morphoPidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = morphoDeviceInfo.rdsId;
                meta.rdsVer = morphoDeviceInfo.rdsVer;
                meta.dpId = morphoDeviceInfo.dpId;
                meta.dc = morphoDeviceInfo.dc;
                meta.mi = morphoDeviceInfo.mi;
                meta.mc = morphoDeviceInfo.mc;


                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = morphoPidData._Skey;
                authReq.Hmac = morphoPidData._Hmac;
                authReq.data = morphoPidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = session.getFreshnessFactor();

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq!=null && meta!=null && morphoDeviceInfo!=null) {

                if (tag.equalsIgnoreCase("3")) {

                    String vid = null;
                    String uid = null;

                    if(aadhar_no_rd.isChecked()) {
                        uid = balanceAadharNumber.getText().toString();
                        if (uid.contains("-")) {
                            uid = uid.replaceAll("-", "").trim();
                        }
                    }
                    if(aadhar_uid_rd.isChecked()) {
                        vid = balanceAadharVID.getText().toString();
                        if (vid.contains("-")) {
                            vid = vid.replaceAll("-", "").trim();
                        }
                    }

                    balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, balanceMobileNumber.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    balanceEnquiryPresenter = new BalanceEnquiryPresenter(DashboardActivity.this);
                    balanceEnquiryPresenter.performBalanceEnquiry(session.getUserToken(), balanceEnquiryRequestModel);
                }
                else if (tag.equalsIgnoreCase("2")) {
                    String vid = null;
                    String uid = null;
                    if(aadhar_no_rd.isChecked()) {
                        uid = withdrawalAadharNumber.getText().toString();
                        if (uid.contains("-")) {
                            uid = uid.replaceAll("-", "").trim();
                        }
                    }
                    if(aadhar_uid_rd.isChecked()) {
                        vid = withdrawalAadharUID.getText().toString();
                        if (vid.contains("-")) {
                            vid = vid.replaceAll("-", "").trim();
                        }
                    }
                    cashWithdrawalRequestModel = new CashWithdrawalRequestModel(withdrawalAmountEnter.getText().toString().trim(), uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, withdrawalMobileNumber.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    cashWithdrawalPresenter = new CashWithdrawalPresenter(DashboardActivity.this);
                    cashWithdrawalPresenter.performCashWithdrawal(session.getUserToken(), cashWithdrawalRequestModel);
                }
                else if (tag.equalsIgnoreCase("1")) {
                    String aadharNo = aadharNumber.getText ().toString ();
                    if (aadharNo.contains ( "-" )) {
                        aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
                    }
                    cashDepositRequestModel = new CashDepositRequestModel(amountEnter.getText().toString().trim(), aadharNo, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, bankIINNumber, meta.mc, meta.mi, mobileNumber.getText().toString().trim(), "DEPOSIT", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                    cashDepositPresenter = new CashDepositPresenter(DashboardActivity.this);
                    cashDepositPresenter.performCashDeposit(session.getUserToken(), cashDepositRequestModel);
                }

            }else{
                Util.showAlert(DashboardActivity.this,getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }
    /*
     * calendar data for the mantra and morpho
     *
     * capture date and time
     */
    private String generateTXN() {
        try {
            Date tempDate = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH);
            String dTTXN = formatter.format(tempDate);
            return dTTXN;
        } catch (Exception e) {
            return "";
        }
    }

    /*
     *  url for the sync of the data for the
     */

    private String getAuthURL(String UID) {
        String url = "http://developer.uidai.gov.in/auth/";
        url += "public/" + UID.charAt(0) + "/" + UID.charAt(1) + "/";
        url += "MG41KIrkk5moCkcO8w-2fc01-P7I5S-6X2-X7luVcDgZyOa2LXs3ELI"; //ASA
        return url;
    }

    private void setToolbar() {

        Toolbar mToolbar = findViewById ( R.id.toolbar );
        mToolbar.setTitle ( getResources ().getString ( R.string.home_title ));
        mToolbar.inflateMenu ( R.menu.aeps_menu );

        mToolbar.setOnMenuItemClickListener ( new Toolbar.OnMenuItemClickListener () {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()== R.id.action_report)
                {
                    Intent in = new Intent(DashboardActivity.this, ReportActivity.class);
                    startActivity(in);
                }
                else if(item.getItemId()== R.id.action_logout)
                {
                    if (session!=null){
                        session.clear();
                        Intent in = new Intent(DashboardActivity.this, LoginActivity.class);
                        startActivity(in);
                        finish();
                    }
                    // do something
                }/*else if(item.getItemId() == R.id.action_setting){
                    Intent in = new Intent(DashboardActivity.this, SettingsActivity.class);
                    startActivity(in);
                    finish();
                }*/

                return false;
            }
        } );
    }

    private void checkBalanceEnquiryValidation() {
        // TODO Auto-generated method stub
        if (balanceMobileNumber.getText () != null && !balanceMobileNumber.getText ().toString ().trim ().matches ( "" )
                && Util.isValidMobile ( balanceMobileNumber.getText ().toString ().trim () ) == true && balanceBankspinner.getText()!=null
                && !balanceBankspinner.getText().toString().trim().matches("")) {

            boolean status = false;
            if(bl_aadhar_no_rd.isChecked()){
                String aadharNo = balanceAadharNumber.getText ().toString ();
                if (aadharNo.contains ( "-" )) {aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
                }
                status = Util.validateAadharNumber ( aadharNo );
            }
            if(bl_aadhar_uid_rd.isChecked()) {
                String aadharVID = balanceAadharVID.getText().toString();
                if (aadharVID.contains ( "-" )) {aadharVID = aadharVID.replaceAll ( "-", "" ).trim ();
                }
                status = Util.validateAadharVID ( aadharVID );
            }

            if (status) {
                if (deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        balanceFingerprint.setEnabled ( true );
                        balanceFingerprint.setBackgroundColor ( Color.BLUE );
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }
                    if (!pidData._Resp.errCode.equals ( "0" )) {
                        balanceFingerprint.setEnabled ( true );
                        balanceFingerprint.setBackgroundColor ( Color.BLUE );
                        balanceSubmitButton.setEnabled ( false );
                        balanceSubmitButton.setBackgroundColor ( Color.GRAY );
                    } else {
                        balanceSubmitButton.setEnabled ( true );
                        balanceSubmitButton.setBackgroundColor ( Color.BLUE );
                    }
                } else if (deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )) {
                    if (morphoPidData == null) {
                        balanceFingerprint.setEnabled ( true );
                        balanceFingerprint.setBackgroundColor ( Color.BLUE );
                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }
                    if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                        balanceFingerprint.setEnabled ( true );
                        balanceFingerprint.setBackgroundColor ( Color.BLUE );
                        balanceSubmitButton.setEnabled ( false );
                        balanceSubmitButton.setBackgroundColor ( Color.GRAY );
                    } else {
                        balanceSubmitButton.setEnabled ( true );
                        balanceSubmitButton.setBackgroundColor ( Color.BLUE );
                    }
                }
            }
        }else {
            balanceSubmitButton.setEnabled ( false );
            balanceSubmitButton.setBackgroundColor ( Color.GRAY );
        }

    }


    private void checkCashDepositValidation() {
        // TODO Auto-generated method stub
        if (mobileNumber.getText () != null && !mobileNumber.getText ().toString ().trim ().matches ( "" )
                && Util.isValidMobile ( mobileNumber.getText ().toString ().trim () ) == true
                && mobileNumber.getText().toString().length () == 10
                && bankspinner.getText()!=null && !bankspinner.getText().toString().trim().matches("")
                && amountEnter.getText()!=null && !amountEnter.getText().toString().trim().matches("") ) {
            String aadharNo = aadharNumber.getText().toString();
            if (aadharNo.contains("-")) {
                aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
            }
            if (Util.validateAadharNumber (aadharNo) == true) {
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setBackgroundColor ( Color.BLUE );

                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    } if (!pidData._Resp.errCode.equals ( "0" )) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setBackgroundColor ( Color.BLUE );
                        submitButton.setEnabled ( false );
                        submitButton.setBackgroundColor ( Color.GRAY );
                    } else {
                        submitButton.setEnabled ( true );
                        submitButton.setBackgroundColor ( Color.BLUE );
                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setBackgroundColor ( Color.BLUE );

                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                        fingerprint.setEnabled ( true );
                        fingerprint.setBackgroundColor ( Color.BLUE );

                        submitButton.setEnabled ( false );
                        submitButton.setBackgroundColor ( Color.GRAY );
                    } else {
                        submitButton.setEnabled ( true );
                        submitButton.setBackgroundColor ( Color.BLUE );
                    }
                }
            }
        }else{
            submitButton.setEnabled(false);
            submitButton.setBackgroundColor(Color.GRAY);
        }
    }

    private void checkWithdrawalValidation() {
        // TODO Auto-generated method stub
        if (withdrawalMobileNumber.getText () != null
                && !withdrawalMobileNumber.getText ().toString ().trim ().matches ( "" )
                && Util.isValidMobile ( withdrawalMobileNumber.getText ().toString ().trim () ) == true
                && withdrawalMobileNumber.getText().toString().length () == 10
                && withdrawalBankspinner.getText()!=null
                && !withdrawalBankspinner.getText().toString().trim().matches("")
                && withdrawalAmountEnter.getText()!=null
                && !withdrawalAmountEnter.getText().toString().trim().matches("") ) {

            boolean status = false;
            if(aadhar_no_rd.isChecked()){
                String aadharNo = withdrawalAadharNumber.getText ().toString ();
                if (aadharNo.contains ( "-" )) {aadharNo = aadharNo.replaceAll ( "-", "" ).trim ();
                }
                status = Util.validateAadharNumber ( aadharNo );
            }
            if(aadhar_uid_rd.isChecked()) {
                String aadharVID = withdrawalAadharUID.getText().toString();
                if (aadharVID.contains ( "-" )) {aadharVID = aadharVID.replaceAll ( "-", "" ).trim ();
                }
                status = Util.validateAadharVID ( aadharVID );
            }

            if (status) {
                if(deviceSerialNumber.trim ().equalsIgnoreCase ( mantradeviceid )) {
                    if (pidData == null) {
                        withdrawalFingerprint.setEnabled ( true );
                        withdrawalFingerprint.setBackgroundColor ( Color.BLUE );

                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    }if (!pidData._Resp.errCode.equals ( "0" )) {
                        Log.v("panda","errorinfo"+pidData._Resp.errInfo);
                        withdrawalFingerprint.setEnabled ( true );
                        withdrawalFingerprint.setBackgroundColor ( Color.BLUE );

                        withdrawalSubmitButton.setEnabled ( false );
                        withdrawalSubmitButton.setBackgroundColor ( Color.GRAY );
                    } else {

                        withdrawalSubmitButton.setEnabled ( true );
                        withdrawalSubmitButton.setBackgroundColor ( Color.BLUE );
                    }
                }else if(deviceSerialNumber.trim ().equalsIgnoreCase ( morphodeviceid )||deviceSerialNumber.trim ().equalsIgnoreCase ( morphoe2device )){
                    if (morphoPidData == null) {
                        withdrawalFingerprint.setEnabled ( true );
                        withdrawalFingerprint.setBackgroundColor ( Color.BLUE );

                        Toast.makeText ( DashboardActivity.this, "Please Scan Your Finger", Toast.LENGTH_SHORT ).show ();
                        return;
                    } if (!morphoPidData._Resp.errCode.equals ( "0" )) {
                        withdrawalFingerprint.setEnabled ( true );
                        withdrawalFingerprint.setBackgroundColor ( Color.BLUE );

                        withdrawalSubmitButton.setEnabled ( false );
                        withdrawalSubmitButton.setBackgroundColor ( Color.GRAY );
                    } else {
                        withdrawalSubmitButton.setEnabled ( true );
                        withdrawalSubmitButton.setBackgroundColor ( Color.BLUE );
                    }
                }
            }
        }else {
            withdrawalSubmitButton.setEnabled ( false );
            withdrawalSubmitButton.setBackgroundColor ( Color.GRAY );
        }

    }

    private void checkRefundValidation() {
        // TODO Auto-generated method stub
        if(apiTidNumber.getText ()==null|| apiTidNumber.getText ().toString ().trim().matches ( "" )){
            refundSubmitButton.setEnabled(false);
            refundSubmitButton.setBackgroundColor(Color.GRAY);
        }else{
            refundSubmitButton.setEnabled(true);
            refundSubmitButton.setBackgroundColor(Color.BLUE);
        }
    }
    TextWatcher mWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub
            checkBalanceEnquiryValidation();

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
        }
    };

    TextWatcher cashDepositWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub
            checkCashDepositValidation();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
        }
    };
    TextWatcher cashWithdrawalWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub
            checkWithdrawalValidation();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub

        }
    };



    TextWatcher refundWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO Auto-generated method stub
            checkRefundValidation();
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
        }
        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
    }
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    public void releaseData() {
        amountEnter.setText(null);
        amountEnter.setError(null);
        aadharNumber.setText(null);
        aadharNumber.setError(null);

        mobileNumber.setText(null);
        mobileNumber.setError(null);

        bankspinner.setText(null);
        bankspinner.setError(null);

        withdrawalAmountEnter.setText(null);
        withdrawalAmountEnter.setError(null);

        withdrawalAadharNumber.setText(null);
        withdrawalAadharNumber.setError(null);

        withdrawalMobileNumber.setText(null);
        withdrawalMobileNumber.setError(null);

        withdrawalBankspinner.setText(null);
        withdrawalBankspinner.setError(null);



        balanceAadharNumber.setText(null);
        balanceAadharNumber.setError(null);

        balanceMobileNumber.setText(null);
        balanceMobileNumber.setError(null);

        balanceBankspinner.setText(null);
        balanceBankspinner.setError(null);


        bankIINNumber = "";

        apiTidNumber.setText(null);
        apiTidNumber.setError(null);

        tag = "";
        pidData=null;
        morphoPidData=null;
        balanceEnquiryRequestModel = null;
        cashDepositRequestModel = null;
        cashWithdrawalRequestModel = null;
        refundRequestModel = null;
        withdrawalBar.setVisibility ( View.GONE );
        balanceEnqureyBar.setVisibility ( View.GONE );
        withdrawalNote.setVisibility ( View.GONE );
        fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
        fingerprintStrengthBalance.setVisibility ( View.GONE );
        balanceNote.setVisibility ( View.GONE );
        depositBar.setVisibility ( View.GONE );
        depositNote.setVisibility ( View.GONE );
        fingerprintStrengthDeposit.setVisibility ( View.GONE );
    }


    /**/
   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK)
            return true;

        return true;
    }*/
}
