package com.iserveu.aeps.postpaid;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.iserveu.aeps.R;
import com.iserveu.aeps.fundtransferreport.MyErrorMessage;
import com.iserveu.aeps.utils.APIService;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_RECHARGE_URL;

public class PostPaid extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private APIService apiService;
    Session session;
    LoadingView loadingView;

    LinearLayout operator_circlecode_layout;
    Button postpaid_button;
    SearchableSpinner postpaid_select_operator,postpaid_select_operator_circle;
    EditText postpaid_mobileno,postpaid_amount,postpaid_stdcode,postpaid_accountno;
    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,latitudeString,circleCodeString,longitudeString;
    Toolbar toolbar;


    String[] operatorPostPaid = { "AIRCEL POSTPAID","BSNL POSTPAID","IDEA POSTPAID","RELIANCE POSTPAID","TATA DOCOMO POSTPAID",
            "VODAFONE POSTPAID","AIRTEL POSTPAID","RELIANCE CDMA POSTPAID","TATA INDICOM LANDLINE","LOOP POSTPAID","BSNL LANDLINE",
            "AIRTEL BROADBAND(DSL)","AIRTEL LANDLINE","IDEA LANDLINE","MTS POSTPAID"};

    String[] operatorCirclePostPaid = { "AndamanNicrobar","AndhraPradesh","ArunachalPradesh","Assam","Bangalore","Bihar","Chennai",
            "Chhattisgarh","DadraandNagarHaveli","DamanandDiu","Delhi","Goa","Gujarat","Haryana","HimachalPradesh","JammuAndKashmir",
            "Jharkhand","Karnataka","Kerala","Kolkata","MadhyaPradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Mumbai","Nagaland",
            "Orissa","Pondicherry","Punjab","Rajasthan","Sikkim","TamilNadu","Tripura","Uttar Pradesh East","Uttar Pradesh West","Uttrakhand",
            "West Bengal"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_paid);

        session = new Session(PostPaid.this);

        rechargetypeString="bill";
        stdCodeString="";
        pincodeString="";
        accountNoString="";
        latitudeString="";
        circleCodeString="";
        longitudeString="";

        operator_circlecode_layout = findViewById(R.id.operator_circlecode_layout);
        postpaid_mobileno = findViewById(R.id.postpaid_mobileno);
        postpaid_amount = findViewById(R.id.postpaid_amount);
        postpaid_stdcode = findViewById(R.id.postpaid_stdcode);
        postpaid_accountno = findViewById(R.id.postpaid_accountno);
        postpaid_button = findViewById(R.id.postpaid_button);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        postpaid_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 10){
                    mobilenoString = postpaid_mobileno.getText().toString();
                }else {
                    mobilenoString = null;
                }
            }
        });

        postpaid_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    amountString = null;
                }else {
                    amountString = postpaid_amount.getText().toString();
                }
            }
        });

        postpaid_stdcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                stdCodeString = postpaid_stdcode.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        postpaid_accountno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                accountNoString = postpaid_accountno.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        postpaid_select_operator = (SearchableSpinner) findViewById(R.id.postpaid_select_operator);
        postpaid_select_operator_circle = (SearchableSpinner) findViewById(R.id.postpaid_select_operator_circle);
        postpaid_select_operator.setOnItemSelectedListener(this);
        postpaid_select_operator_circle.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorPostPaid);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        postpaid_select_operator.setAdapter(aa);

        ArrayAdapter aaa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorCirclePostPaid);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        postpaid_select_operator_circle.setAdapter(aaa);

        postpaid_button.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (amountString==null|| mobilenoString==null){
            Toast.makeText(getApplicationContext(),"Enter 10 Digit MobileNo OR Enter Amount" , Toast.LENGTH_LONG).show();
        }else {
            rechargePostpaid();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//            adapterView.getItemAtPosition(i);


            switch (adapterView.getId())
            {
                case R.id.postpaid_select_operator:
                    operatorString = operatorPostPaid[i];
                    if (operatorString.equals(operatorPostPaid[1])){
                        operator_circlecode_layout.setVisibility(View.VISIBLE);

                    }else {
                        operator_circlecode_layout.setVisibility(View.GONE);
                        circleCodeString="";
                        stdCodeString="";
                        accountNoString="";
                    }
                   // Toast.makeText(getApplicationContext(),operatorString , Toast.LENGTH_LONG).show();

                    break;

                case R.id.postpaid_select_operator_circle:

                    circleCodeString = operatorCirclePostPaid[i];
                    Toast.makeText(getApplicationContext(),circleCodeString , Toast.LENGTH_LONG).show();
                    break;
            }

//        operatorString = operatorPostPaid[i];
//        circleCodeString = operatorCirclePostPaid[i];
//        Toast.makeText(getApplicationContext(),operatorString , Toast.LENGTH_LONG).show();
//        Toast.makeText(getApplicationContext(),circleCodeString , Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void rechargePostpaid() {
        showLoader();
        final PostPaidRequest postPaidRequest = new PostPaidRequest(stdCodeString,pincodeString,amountString,mobilenoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,longitudeString);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp123456600"+postPaidRequest);
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp12345660011"+session.getUserToken());
        Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566001177"+ Constants.BASE_URL+"getallrechargehere");

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                          //  performEncodedRecharge(postPaidRequest,encodedUrl);
                            proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    public void proceedRecharge(String url){

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode",stdCodeString);
            obj.put("amount",amountString);
            obj.put("mobileNumber",mobilenoString);
            obj.put("accountNo",accountNoString);
            obj.put("circleCode",circleCodeString);
            obj.put("operatorCode",operatorString);
            obj.put("rechargeType",rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization",session.getUserToken())
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if(obj.getString("status").equalsIgnoreCase("0")){
                                    SuccessDialog(obj.getString("statusDesc"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message=gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            //transaction failed
                            ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                            //  anError.getResponse();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
  /*  public void performEncodedRecharge(PostPaidRequest postPaidRequest, String encodedUrl){
        final PostPaidApi prepaidApi = this.apiService.getClient().create(PostPaidApi.class);

        prepaidApi.getPostpaid(session.getUserToken(),postPaidRequest, encodedUrl).enqueue(new Callback<PostPaidResponse>() {
            @Override
            public void onResponse(Call<PostPaidResponse> call, Response<PostPaidResponse> response) {
                if (response.isSuccessful()){
                    Log.d("STATUS", "TransactionSuccessfulVerifyOtp1234566"+response.body());
                    hideLoader();
                    SuccessDialog(response.body().getStatusDesc());
                }else {
                    hideLoader();
                    Log.d("STATUS", "TransactionFailedVerifyOtp1234577"+response.body());

                    Gson gson = new Gson();
                    MyErrorMessage message=gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                    //transaction failed
                    ErrorDialog("RECHARGE FAILED! "+message.getMessage());




                }
            }

            @Override
            public void onFailure(Call<PostPaidResponse> call, Throwable t) {
                hideLoader();
                Log.d("STATUS", "TransactionFailedVerifyOtp1234588"+t);
                ErrorDialog("RECHARGE FAILED! undefined");
            }
        });

    }
*/
    public void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(PostPaid.this);
        }
        loadingView.show();

    }
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }

    private void ErrorDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PostPaid.this);
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void SuccessDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PostPaid.this);
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        onBackPressed();

                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
