package com.iserveu.aeps.postpaid;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface PostPaidApi {
    @POST()
    Call<PostPaidResponse> getPostpaid(@Header("Authorization") String token, @Body PostPaidRequest postPaidRequest, @Url String url);
}
