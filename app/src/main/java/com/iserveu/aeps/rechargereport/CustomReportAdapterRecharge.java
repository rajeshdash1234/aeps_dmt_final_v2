package com.iserveu.aeps.rechargereport;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Util;

import java.util.List;

public class CustomReportAdapterRecharge extends RecyclerView.Adapter<CustomReportAdapterRecharge.ViewHolder> {

    private RechargeReport context;
    private List<RechargeResponseSetGet> rechargeResponseSetGets;
    public CustomReportAdapterRecharge(RechargeReport context , List rechargeResponseSetGets) {
        this.context = context;
        this.rechargeResponseSetGets = rechargeResponseSetGets;
    }

/*
public CustomReportAdapterRecharge(Callback<ReportFragmentResponse> callback, FinoTransactionReports[] finoTransactionReports) {

    }

    public CustomReportAdapterRecharge(Callback<ReportFragmentResponse> callback, FinoTransactionReports finoTransactionReport) {

    }
*/



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_row_recharge, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomReportAdapterRecharge.ViewHolder holder, int position) {

        holder.itemView.setTag(rechargeResponseSetGets.get(position));

        RechargeResponseSetGet pu = rechargeResponseSetGets.get(position);

        holder.transaction_id_recharge.setText(pu.getId());
        holder.operator_transaction_id_recharge.setText(pu.getOperatorTransactionId());
        holder.operator_description_recharge.setText(pu.getOperatorDescription());
        holder.mobile_number_recharge.setText(pu.getMobileNumber());
        holder.api_comment_recharge.setText(pu.getApiComment());
        holder.amount_transacted_recharge.setText(pu.getAmountTransacted());
        holder.current_bal_amt_recharge.setText(pu.getBalanceAmount());
        holder.transaction_type_recharge.setText(pu.getTransactionType());
        holder.created_date_recharge.setText(Util.getDateFromTime(Long.parseLong(pu.getCreatedDate())));
        holder.status.setText(pu.getStatus());
        holder.updated_date.setText(Util.getDateFromTime(Long.parseLong(pu.getUpdatedDate())));
        if (pu.getStatus().equals("SUCCESS")) {
            holder.cardViewFundtransferReport.setBackgroundColor(Color.GREEN);
        }else {
            holder.cardViewFundtransferReport.setBackgroundColor(Color.RED);
        }

    }

    @Override
    public int getItemCount() {
        return rechargeResponseSetGets.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView status,updated_date,transaction_id_recharge,user_track_id_recharge,operator_transaction_id_recharge,operator_description_recharge,
                mobile_number_recharge,amount_transacted_recharge,current_bal_amt_recharge,transaction_type_recharge,api_comment_recharge,created_date_recharge;

        public CardView cardViewFundtransferReport;
//        String date;

        public ViewHolder(View itemView) {
            super(itemView);

            cardViewFundtransferReport = itemView.findViewById(R.id.card_view_fundtransfer_report);


            transaction_id_recharge = (TextView) itemView.findViewById(R.id.transaction_id_recharge);
//            user_track_id_recharge = (TextView) itemView.findViewById(R.id.user_track_id_recharge);
            operator_transaction_id_recharge = (TextView) itemView.findViewById(R.id.operator_transaction_id_recharge);
            operator_description_recharge = (TextView) itemView.findViewById(R.id.operator_description_recharge);
            mobile_number_recharge = (TextView) itemView.findViewById(R.id.mobile_number_recharge);
            amount_transacted_recharge = (TextView) itemView.findViewById(R.id.amount_transacted_recharge);
            current_bal_amt_recharge = (TextView) itemView.findViewById(R.id.current_bal_amt_recharge);
            transaction_type_recharge = (TextView) itemView.findViewById(R.id.transaction_type_recharge);
            api_comment_recharge = (TextView) itemView.findViewById(R.id.api_comment_recharge);
            created_date_recharge = (TextView) itemView.findViewById(R.id.created_date_recharge);
            status = (TextView) itemView.findViewById(R.id.status);
            updated_date = (TextView) itemView.findViewById(R.id.updated_date);
        }
    }


}
