package com.iserveu.aeps.rechargereport;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.APIService;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_TRANSACTION_REPORT_URL;

//import com.ionicframework.ionicdemo144715.dashboardhomefragment.CustomReportAdapter;

public class RechargeReport extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    LoadingView loadingView;

    TextView textView;

    private RecyclerView reportRecyclerView;
//    CustomReportAdapter customReportAdapter;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    String transactionType = "RECHARGE";
//    String transactionType = "ISU_FT";

    private APIService apiService;
    Session session;

    ReportRechargeRequest reportRechargeRequestBody;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_report);

        // hide notification bar
        //getSupportActionBar().hide();
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        session = new Session(RechargeReport.this);

        textView = findViewById(R.id.text_date_picker);
        reportRecyclerView = findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(RechargeReport.this);

        reportRecyclerView.setLayoutManager(layoutManager);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(RechargeReport.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(),"jitu");
                dpd.setMaxDate ( Calendar.getInstance () );
                dpd.setAutoHighlight ( true );

            }
        });
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {


        String fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        String todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd);
        String api_todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd+1);

        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+(dayOfMonthEnd)+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        textView.setText ( date );
        Log.d("Report", "fromdate: "+fromdate);
        Log.d("Report", "fromdate todate: "+todate);

        /*ReportFragmentRequest*/ reportRechargeRequestBody= new ReportRechargeRequest(transactionType,fromdate,api_todate);
        reportCall();
        showLoader();
    }

   /* private void reportCall() {

//        VerifyBeneRequest verifyBeneRequest = new VerifyBeneRequest(customerIds,getAccountNo,bankName,beneNameEnter,ifscCode,pipeNoo);

        if (this.apiService == null) {
            this.apiService = new APIService();
        }

        final ReportRechargeApi reportRechargeApi = this.apiService.getClient().create(ReportRechargeApi.class);
        Log.d("STATUS", "TransactionSuccessful008811"+reportRechargeRequestBody);
        reportRechargeApi.getReportRecharge(session.getUserToken(),reportRechargeRequestBody, Constants.recharge+"transactiondetails").enqueue(new Callback<ReportRechargeResponse>() {
            @Override
            public void onResponse(Call<ReportRechargeResponse> call, Response<ReportRechargeResponse> response) {
                if (response.isSuccessful()){

                    Log.d("STATUS", "TransactionSuccessfulReport"+response.body());
                    ArrayList<RechargeResponseSetGet> rechargeResponseSetGet = response.body().getRechargeResponseSetGet();
                    for( int i=0;i<rechargeResponseSetGet.size();i++) {
                        Log.d("BENILIST", "benilist1111" + rechargeResponseSetGet);
                        mAdapter = new CustomReportAdapterRecharge(RechargeReport.this, rechargeResponseSetGet);

                        reportRecyclerView.setAdapter(mAdapter);
                        hideLoader();
                    }

                }else {
                    Log.d("STATUS", "TransactionFailedReport"+response.body());
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<ReportRechargeResponse> call, Throwable t) {

            }
        });
    }*/



    private void reportCall() {
        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_TRANSACTION_REPORT_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            encriptedReportCall(encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(RechargeReport.this);
        }
        loadingView.show();

    }

    private void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }
    //Edited Rajesh
    private void  encriptedReportCall(String encodedUrl){

        final ReportRechargeApi reportRechargeApi = this.apiService.getClient().create(ReportRechargeApi.class);
        Log.d("STATUS", "TransactionSuccessful008811"+reportRechargeRequestBody);

        //final ReportFragmentApi reportFragmentApi = this.apiService.getClient().create(ReportFragmentApi.class);
        reportRechargeApi.getReportRecharge(session.getUserToken(),reportRechargeRequestBody, encodedUrl).enqueue(new Callback<ReportRechargeResponse>() {
            @Override
            public void onResponse(Call<ReportRechargeResponse> call, Response<ReportRechargeResponse> response) {
                if (response.isSuccessful()){

                    Log.d("STATUS", "TransactionSuccessfulReport"+response.body());
                    ArrayList<RechargeResponseSetGet> rechargeResponseSetGet = response.body().getRechargeResponseSetGet();
                    for( int i=0;i<rechargeResponseSetGet.size();i++) {
                        Log.d("BENILIST", "benilist1111" + rechargeResponseSetGet);
                        mAdapter = new CustomReportAdapterRecharge(RechargeReport.this, rechargeResponseSetGet);

                        reportRecyclerView.setAdapter(mAdapter);
                        hideLoader();
                    }

                }else {
                    Log.d("STATUS", "TransactionFailedReport"+response.body());
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<ReportRechargeResponse> call, Throwable t) {
                hideLoader();

            }
        });

    }
}
