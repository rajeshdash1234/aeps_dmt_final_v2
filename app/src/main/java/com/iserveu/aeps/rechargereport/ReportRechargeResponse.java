package com.iserveu.aeps.rechargereport;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReportRechargeResponse {

    @Override
    public String toString() {
        return "ReportRechargeResponse{" +
                "rechargeResponseSetGet=" + rechargeResponseSetGet +
                '}';
    }

    public ArrayList<RechargeResponseSetGet> getRechargeResponseSetGet() {
        return rechargeResponseSetGet;
    }

    public void setRechargeResponseSetGet(ArrayList<RechargeResponseSetGet> rechargeResponseSetGet) {
        this.rechargeResponseSetGet = rechargeResponseSetGet;
    }
    @SerializedName("transactionReports")
    private ArrayList<RechargeResponseSetGet> rechargeResponseSetGet;

}
