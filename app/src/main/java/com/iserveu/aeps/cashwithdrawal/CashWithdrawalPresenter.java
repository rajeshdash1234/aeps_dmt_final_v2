package com.iserveu.aeps.cashwithdrawal;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.utils.AEPSAPIService;
import com.iserveu.aeps.utils.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_TRANSACTION_URL;


/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class CashWithdrawalPresenter implements CashWithDrawalContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private CashWithDrawalContract.View cashWithDrawalContractView;
    private AEPSAPIService aepsapiService;
    private Session session;
    /**
     * Initialize LoginPresenter
     */
    public CashWithdrawalPresenter(CashWithDrawalContract.View cashWithDrawalContractView) {
        this.cashWithDrawalContractView = cashWithDrawalContractView;
    }




    @Override
    public void performCashWithdrawal(final String token,final CashWithdrawalRequestModel cashWithdrawalRequestModel) {
       /* if (cashWithdrawalRequestModel!=null && cashWithdrawalRequestModel.getAadharNo() !=null && !cashWithdrawalRequestModel.getAadharNo().matches("") &&
                cashWithdrawalRequestModel.getAmount() !=null && !cashWithdrawalRequestModel.getAmount().matches("") &&
                cashWithdrawalRequestModel.getCi() !=null && !cashWithdrawalRequestModel.getCi().matches("") &&
                cashWithdrawalRequestModel.getDc() !=null && !cashWithdrawalRequestModel.getDc().matches("") &&
                cashWithdrawalRequestModel.getDpId() !=null && !cashWithdrawalRequestModel.getDpId().matches("") &&
                cashWithdrawalRequestModel.getEncryptedPID() !=null && !cashWithdrawalRequestModel.getEncryptedPID().matches("") &&
                cashWithdrawalRequestModel.getFreshnessFactor() !=null && !cashWithdrawalRequestModel.getFreshnessFactor().matches("") &&
                cashWithdrawalRequestModel.gethMac() !=null && !cashWithdrawalRequestModel.gethMac().matches("") &&
                cashWithdrawalRequestModel.getIin() !=null && !cashWithdrawalRequestModel.getIin().matches("") &&
                cashWithdrawalRequestModel.getMcData() !=null && !cashWithdrawalRequestModel.getMcData().matches("") &&
                cashWithdrawalRequestModel.getMi() !=null && !cashWithdrawalRequestModel.getMi().matches("") &&
                cashWithdrawalRequestModel.getMobileNumber() !=null && !cashWithdrawalRequestModel.getMobileNumber().matches("") &&
                cashWithdrawalRequestModel.getOperation() !=null && !cashWithdrawalRequestModel.getOperation().matches("") &&
                cashWithdrawalRequestModel.getRdsId() !=null && !cashWithdrawalRequestModel.getRdsId().matches("") &&
                cashWithdrawalRequestModel.getRdsVer() !=null && !cashWithdrawalRequestModel.getRdsVer().matches("") &&
                cashWithdrawalRequestModel.getsKey() !=null && !cashWithdrawalRequestModel.getsKey().matches("")
                ) {*/
            cashWithDrawalContractView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }

            AndroidNetworking.get(GET_AEPS_TRANSACTION_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            encryptCashWithDraw(token,cashWithdrawalRequestModel,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

       /* } else {
            cashWithDrawalContractView.hideLoader();
            cashWithDrawalContractView.checkEmptyFields();
        }*/
    }

    public void encryptCashWithDraw(String token, CashWithdrawalRequestModel cashWithdrawalRequestModel, String encodedUrl){
        CashWithdrawalAPI cashWithdrawalAPI =this.aepsapiService.getClient().create(CashWithdrawalAPI.class);
        cashWithdrawalAPI.checkCashWithDrawal(token,cashWithdrawalRequestModel,encodedUrl).enqueue(new Callback<CashWithdrawalResponse>() {
            @Override
            public void onResponse(Call<CashWithdrawalResponse> call, Response<CashWithdrawalResponse> response) {
                if(response.isSuccessful()) {
                    // String message = "";
                    if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {
                        //message = "Login Successful";
                        cashWithDrawalContractView.hideLoader();
                        cashWithDrawalContractView.checkCashWithdrawalStatus(response.body().getStatus(), response.body().getStatusDesc(),response.body());
                    }else{
                        cashWithDrawalContractView.hideLoader();
                        cashWithDrawalContractView.checkCashWithdrawalStatus("", "Cash Withdrawal Failed",null);
                        Log.v("laxmi","hf"+cashWithDrawalContractView);
                    }
                }else{
                    cashWithDrawalContractView.hideLoader();
                    cashWithDrawalContractView.checkCashWithdrawalStatus("", "Cash Withdrawal Failed",null);
                }
            }
            @Override
            public void onFailure(Call<CashWithdrawalResponse> call, Throwable t) {
                cashWithDrawalContractView.hideLoader();
                cashWithDrawalContractView.checkCashWithdrawalStatus("", "Cash Withdrawal Failed",null);
            }
        });
    }
}
