package com.iserveu.aeps.cashwithdrawal;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface CashWithdrawalAPI {
    //"/aeps/transaction/mob/v1"
    @POST()

    Call<CashWithdrawalResponse> checkCashWithDrawal(@Header("Authorization") String token, @Body CashWithdrawalRequestModel body, @Url String url);
}

