package com.iserveu.aeps.utils;

import android.location.Location;

public interface UpdateLocationInterface {

    public void onLocationUpdate(Location location);
    public void onAddressUpdate(String address);
}
