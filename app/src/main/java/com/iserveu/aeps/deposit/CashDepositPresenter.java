package com.iserveu.aeps.deposit;

import android.util.Log;

import com.iserveu.aeps.utils.AEPSAPIService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class CashDepositPresenter implements CashDepositContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private CashDepositContract.View cashDepositContractView;
    private AEPSAPIService aepsapiService;
    /**
     * Initialize LoginPresenter
     */
    public CashDepositPresenter(CashDepositContract.View cashDepositContractView) {
        this.cashDepositContractView = cashDepositContractView;
    }




    @Override
    public void performCashDeposit(String token,CashDepositRequestModel cashDepositRequestModel) {
        if (cashDepositRequestModel!=null && cashDepositRequestModel.getAadharNo() !=null && !cashDepositRequestModel.getAadharNo().matches("") &&
                cashDepositRequestModel.getAmount() !=null && !cashDepositRequestModel.getAmount().matches("") &&
                cashDepositRequestModel.getCi() !=null && !cashDepositRequestModel.getCi().matches("") &&
                cashDepositRequestModel.getDc() !=null && !cashDepositRequestModel.getDc().matches("") &&
                cashDepositRequestModel.getDpId() !=null && !cashDepositRequestModel.getDpId().matches("") &&
                cashDepositRequestModel.getEncryptedPID() !=null && !cashDepositRequestModel.getEncryptedPID().matches("") &&
                cashDepositRequestModel.getFreshnessFactor() !=null && !cashDepositRequestModel.getFreshnessFactor().matches("") &&
                cashDepositRequestModel.gethMac() !=null && !cashDepositRequestModel.gethMac().matches("") &&
                cashDepositRequestModel.getIin() !=null && !cashDepositRequestModel.getIin().matches("") &&
                cashDepositRequestModel.getMcData() !=null && !cashDepositRequestModel.getMcData().matches("") &&
                cashDepositRequestModel.getMi() !=null && !cashDepositRequestModel.getMi().matches("") &&
                cashDepositRequestModel.getMobileNumber() !=null && !cashDepositRequestModel.getMobileNumber().matches("") &&
                cashDepositRequestModel.getOperation() !=null && !cashDepositRequestModel.getOperation().matches("") &&
                cashDepositRequestModel.getRdsId() !=null && !cashDepositRequestModel.getRdsId().matches("") &&
                cashDepositRequestModel.getRdsVer() !=null && !cashDepositRequestModel.getRdsVer().matches("") &&
                cashDepositRequestModel.getsKey() !=null && !cashDepositRequestModel.getsKey().matches("")
                ) {
            cashDepositContractView.showLoader();

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }

            // this.aepsapiService = new AEPSAPIService();

            CashDepositAPI cashDepositAPI =
                    this.aepsapiService.getClient().create(CashDepositAPI.class);

            cashDepositAPI.checkCashDeposit(token,cashDepositRequestModel).enqueue(new Callback<CashDepositResponse>() {
                @Override
                public void onResponse(Call<CashDepositResponse> call, Response<CashDepositResponse> response) {

                    if(response.isSuccessful()) {
                        // String message = "";
                        if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {
                            //message = "Login Successful";
                            Log.v("laxmi","hf"+response.body().getStatus());
                            Log.v("laxmi","hf"+response.body().getApiComment());
                            cashDepositContractView.hideLoader();
                            cashDepositContractView.checkCashDepositStatus(response.body().getStatus(), response.body().getStatusDesc());

                        }else{
                            cashDepositContractView.hideLoader();
                            cashDepositContractView.checkCashDepositStatus("", "Cash Deposit Failed");

                        }

                    }else{
                        cashDepositContractView.hideLoader();
                        cashDepositContractView.checkCashDepositStatus("", "Cash Deposit Failed");

                    }
                }

                @Override
                public void onFailure(Call<CashDepositResponse> call, Throwable t) {
                    cashDepositContractView.hideLoader();
                    cashDepositContractView.checkCashDepositStatus("", "Cash Deposit Failed");
                }
            });
        } else {
            cashDepositContractView.checkEmptyFields();
        }
    }
}
