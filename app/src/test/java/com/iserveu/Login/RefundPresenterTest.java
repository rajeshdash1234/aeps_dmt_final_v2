/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iserveu.Login;



import com.iserveu.aeps.refund.RefundContract;
import com.iserveu.aeps.refund.RefundPresenter;
import com.iserveu.aeps.refund.RefundRequestModel;
import com.iserveu.aeps.utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link RefundPresenterTest}.
 */
public class RefundPresenterTest {


    /**
     *  Mock of RefundContract.View for unit testing
     */


    @Mock
    private RefundContract.View refundContractView;

    /**
     *  Mock of RefundPresenter for unit testing
     */



    @InjectMocks
    private RefundPresenter refundPresenter;



    /**
     *  setupRefundPresenterTest will be called before execution of the tests
     */

    @Before
    public void setupRefundPresenterTest() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test

        refundPresenter = new RefundPresenter(refundContractView);


    }


    /**
     *  emptyTokenErrorUi() checks whether token is empty string and requestModel is null
     */

    @Test
    public void emptyTokenErrorUi() {
        // When the presenter is asked to check refund with null request and empty token
        refundPresenter.performRefund("",null);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  nullTokenErrorUi() checks whether token and requestModel both are null
     */

    @Test
    public void nullTokenErrorUi() {
        // When the presenter is asked to check refund with null request and null token
        refundPresenter.performRefund(null,null);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  nullRequest() checks whether requestModelis null
     */
    @Test
    public void nullRequest() {
        // When the presenter is asked to check refund with null request
        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,null);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  emptyDataRequest() checks whether refundRequestModel contains empty Strings for both parameters transactionId , freshnessFactor
     */

    @Test
    public void emptyDataRequest() {
        // When the presenter is asked to login with empty field(s)
        RefundRequestModel refundRequestModel = new RefundRequestModel("","");
        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,refundRequestModel);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  nullDataRequest() checks whether refundRequestModel contains null for both parameters transactionId , freshnessFactor
     */

    @Test
    public void nullDataRequest() {
        // When the presenter is asked to login with empty field(s)
        RefundRequestModel refundRequestModel = new RefundRequestModel(null,null);

        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,refundRequestModel);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  emptyTransactionIdRequest() checks whether refundRequestModel contains empty string for transactionId
     */

    @Test
    public void emptyTransactionIdRequest() {
        // When the presenter is asked to login with empty field(s)
        RefundRequestModel refundRequestModel = new RefundRequestModel("","8549221210603767123");

        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,refundRequestModel);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  nullTransactionIdRequest() checks whether refundRequestModel contains null for transactionId
     */

    @Test
    public void nullTransactionIdRequest() {
        // When the presenter is asked to login with empty field(s)
        RefundRequestModel refundRequestModel = new RefundRequestModel(null,"8549221210603767123");

        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,refundRequestModel);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  emptyFreshnessFactorRequest() checks whether refundRequestModel contains empty String for freshness factor
     */

    @Test
    public void emptyFreshnessFactorRequest() {
        // When the presenter is asked to login with empty field(s)
        RefundRequestModel refundRequestModel = new RefundRequestModel("53618683","");

        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,refundRequestModel);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     *  nullFreshnessFactorRequest() checks whether refundRequestModel contains null for freshness factor
     */

    @Test
    public void nullFreshnessFactorRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like amount , aadhar no , operation , mobile number
        RefundRequestModel refundRequestModel = new RefundRequestModel("53618683",null);

        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,refundRequestModel);

        // Then an empty field error is shown in the UI
        verify(refundContractView).checkEmptyFields();



    }

    /**
     * successRequest() checks data whether it is successfully posted
     */

    @Test
    public void successRequest() {
        // When the presenter is asked to load refund with dummy success data
        RefundRequestModel refundRequestModel = new RefundRequestModel("6099","8549221210603767123");

        refundPresenter.performRefund(Constants.TEST_USER_TOKEN,refundRequestModel);
        // When show Success message in  UI

        refundContractView.checkRefundStatus("0","Successful");
        // Then show Success message in Test UI
        verify(refundContractView).checkRefundStatus("0","Successful");



    }





}
