package com.iserveu.aeps.fundtransferreport;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ReportFragmentApi {
//    @POST("/transactiondetails")
    @POST()
    Call<ReportFragmentResponse> getReport(@Header("Authorization") String token, @Body ReportFragmentRequest reportFragmentRequestBody, @Url String url);
}
