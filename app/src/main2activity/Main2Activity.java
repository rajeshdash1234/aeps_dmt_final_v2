package com.iserveu.aeps.main2activity;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iserveu.aeps.R;
import com.iserveu.aeps.bbps.Bbps;
import com.iserveu.aeps.dashboard.BalanceContract;
import com.iserveu.aeps.dashboard.BalancePresenter;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.dashboard.UserInfoModel;
import com.iserveu.aeps.datacard.DataCard;
import com.iserveu.aeps.dth.Dth;
import com.iserveu.aeps.fundtransfer.FundTransfer;
import com.iserveu.aeps.fundtransferreport.FundTransferReport;
import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.login.LoginContract;
import com.iserveu.aeps.login.LoginPresenter;
import com.iserveu.aeps.microatm.MicroAtmActivity;
import com.iserveu.aeps.postpaid.PostPaid;
import com.iserveu.aeps.prepaid.Prepaid;
import com.iserveu.aeps.special.Special;
import com.iserveu.aeps.utility.Utility;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Main2Activity extends AppCompatActivity implements BalanceContract.View, LoginContract.View{

    RelativeLayout fundtransfer_relative_layout,aeps_relative_layout,fundtransfer_report_relative_layout;
    RelativeLayout relative_layout_prepaid,relative_layout_postpaid,relative_layout_special,relative_layout_datacard,relative_layout_dth,relative_layout_utility,relative_layout_bbps;
    TextView balanceMoney,customer_name;
    private LoginPresenter loginPresenter;
    private BalancePresenter mActionsListener;
    Session session;
    private ImageView refresh_Payment,action_logout;
    private CircleImageView customer_image;
    private RelativeLayout recharge_relative_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        loginPresenter = new LoginPresenter(Main2Activity.this);
        session = new Session(Main2Activity.this);
        mActionsListener = new BalancePresenter(Main2Activity.this);
        balanceMoney = findViewById ( R.id. balanceMoney );

        fundtransfer_relative_layout = findViewById(R.id.fundtransfer_relative_layout);
        fundtransfer_report_relative_layout = findViewById(R.id.fundtransfer_report_relative_layout);
        aeps_relative_layout = findViewById(R.id.aeps_relative_layout);
        recharge_relative_layout = (RelativeLayout)findViewById(R.id.recharge_relative_layout);

        relative_layout_prepaid = findViewById(R.id.relative_layout_prepaid);
        relative_layout_postpaid = findViewById(R.id.relative_layout_postpaid);
        relative_layout_special = findViewById(R.id.relative_layout_special);
        relative_layout_datacard = findViewById(R.id.relative_layout_datacard);
        relative_layout_dth = findViewById(R.id.relative_layout_dth);
        relative_layout_utility =findViewById(R.id.relative_layout_utility);
        relative_layout_bbps =findViewById(R.id.relative_layout_bbps);




        fundtransfer_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransfer.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        refresh_Payment = (ImageView)findViewById(R.id.refresh_Payment);
        refresh_Payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rorate_Clockwise(refresh_Payment);
                mActionsListener.loadBalance(session.getUserToken());
                System.out.println(">>>>>------Rajesh");
            }
        });

        relative_layout_prepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Prepaid.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }

            }
        });
        relative_layout_postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), PostPaid.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_special.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Special.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_datacard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), DataCard.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_dth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Dth.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_utility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Utility.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        relative_layout_bbps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), Bbps.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });






        customer_name = (TextView)findViewById(R.id.customer_name);
        customer_name.setText("Hello, "+Constants.USER_NAME);

        customer_image = (CircleImageView)findViewById(R.id.customer_image);
        loadProfileImage();

        action_logout = (ImageView) findViewById(R.id.action_logout);
        action_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // onBackPressed();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(Main2Activity.this);
                builder1.setMessage("Do you want to logout this app.");
                builder1.setTitle("Log out");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                Intent intent = new Intent(Main2Activity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        aeps_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }

            }
        });

        fundtransfer_report_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransferReport.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadProfileImage(){
        String image_url ="https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/ADMIN_PROFILE%2F"+Constants.ADMIN_NAME+"%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";
        Glide.with(Main2Activity.this)
                .load(image_url)
                .apply(RequestOptions.placeholderOf(R.drawable.user_icon).error(R.drawable.user_icon))
                .into(customer_image);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActionsListener.loadBalance(session.getUserToken());
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Main2Activity.this);
        builder1.setMessage("Do you want to exit from this app.");
        builder1.setTitle("Exit");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {
        String aeps = "27";
        String dmt = "30";
        ArrayList<String> featureCode = new ArrayList<>();
        featureCode.add(aeps);
        featureCode.add(dmt);

        List<UserInfoModel.userFeature> temp_feature = Util.removeDuplicates(userFeatures);

        for (int i = 0; i < temp_feature.size(); i++) {
            if(featureCode.contains(temp_feature.get(i).getId())) {
                if(aeps.equalsIgnoreCase(temp_feature.get(i).getId())){
                    aeps_relative_layout.setVisibility(View.VISIBLE);
//                    aepsTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aepsFeatureCode);
                    loginPresenter.performLogin(session.getUserName(),session.getPassword());
                }
                if(dmt.equalsIgnoreCase(temp_feature.get(i).getId())){
                    fundtransfer_relative_layout.setVisibility(View.VISIBLE);
                    fundtransfer_report_relative_layout.setVisibility(View.VISIBLE);
//                    matmTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(matmFeatureCode);
                }
                if(temp_feature.get(i).getId().equalsIgnoreCase("4") || temp_feature.get(i).getId().equalsIgnoreCase("10")
                        || temp_feature.get(i).getId().equalsIgnoreCase("14") || temp_feature.get(i).getId().equalsIgnoreCase("15")
                        || temp_feature.get(i).getId().equalsIgnoreCase("11")){
                    recharge_relative_layout.setVisibility(View.VISIBLE);
                }


            } else {
                Log.v("radha", "not contains : "+userFeatures.get(i).getId());
            }
        }

    }

    @Override
    public void showBalance(String balance) {
        balanceMoney.setText(balance);

    }

    @Override
    public void emptyLogin() {

    }

    @Override
    public void checkLoginStatus(String status, String message, String token, String nextFreshnessFactor) {

    }

    @Override
    public void showLoginFeature(ArrayList<UserInfoModel.userFeature> userFeatures, String token) {
        loginPresenter.getLoginDetails(token);
    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }
    public void rorate_Clockwise(View view) {
        ObjectAnimator rotate = ObjectAnimator.ofFloat(view, "rotation", 0f, 180f);
        rotate.setRepeatCount(5);
        rotate.setDuration(500);
        rotate.start();
    }
}