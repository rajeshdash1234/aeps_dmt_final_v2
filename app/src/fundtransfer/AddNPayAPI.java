package com.iserveu.aeps.fundtransfer;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface AddNPayAPI {
    @POST()
    @Headers("Content-Type: application/json")

    Call<AddNPayResponse> getAddNPayReport(@Header("Authorization") String token, @Body AddNPayRequest addNPayRequestBody, @Url String url);
}


