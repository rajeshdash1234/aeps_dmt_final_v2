package com.iserveu.aeps.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LoginRequest {

    /**
     * request body parameters for login Api
     */
    @SerializedName("Authorization")
    @Expose
    private String Authorization;

    public LoginRequest(String Authorization) {
        this.Authorization = Authorization;
    }

    public String getAuthorization() {
        return Authorization;
    }

    public void setAuthorization(String authorization) {
        Authorization = authorization;
    }
}

