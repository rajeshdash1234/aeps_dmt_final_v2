/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.iserveu.aeps;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.gsg.gsgswipe";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "gsgswipe";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "0.0.1";
  // Fields from build type: release
  public static final String ADD_AND_PAY = "https://dmt.iserveu.tech/generate/v6";
  public static final String ADD_BENE_URL = "https://dmt.iserveu.tech/generate/v13";
  public static final String ADD_CUSTOMER = "https://dmt.iserveu.tech/generate/v14";
  public static final String BANK_IFSC = "https://dmt.iserveu.tech/generate/v3";
  public static final String BULK_TRANSFER = "https://dmt.iserveu.tech/generate/v5";
  public static final String DELETE_BENE_DMT = "https://dmt.iserveu.tech/generate/v25";
  public static final String GET_AEPS_BALANCE_URL = "https://itpl.iserveu.tech/generate/v21";
  public static final String GET_AEPS_FETCH_BILL_AMOUNT_URL = "https://itpl.iserveu.tech/generate/v24";
  public static final String GET_AEPS_RECHARGE_URL = "https://itpl.iserveu.tech/generate/v23";
  public static final String GET_AEPS_REPORT_URL = "https://itpl.iserveu.tech/generate/v10";
  public static final String GET_AEPS_TRANSACTION_URL = "https://itpl.iserveu.tech/generate/v22";
  public static final String GET_LOADED_FEATURE_URL = "https://itpl.iserveu.tech/generate/v2";
  public static final String GET_LOAD_WALLET_URL = "https://dmt.iserveu.tech/generate/v20";
  public static final String GET_LOGIN_ENCRIPTED_URL = "https://itpl.iserveu.tech/generate/v1";
  public static final String GET_MICRO_ATM_REFUND_URL = "https://itpl.iserveu.tech/generate/v9";
  public static final String GET_MICRO_ATM_REPORT_URL = "https://itpl.iserveu.tech/generate/v8";
  public static final String GET_TBalanceEnquiryStatus_MICRO_ATM_URL = "https://itpl.iserveu.tech/generate/v12";
  public static final String GET_TRANSACTION_REPORT_URL = "https://itpl.iserveu.tech/generate/v7";
  public static final String LOGIN_DETAILS_URL = "https://itpl.iserveu.tech/generate/v11";
  public static final String RESEND_OTP = "https://dmt.iserveu.tech/generate/v19";
  public static final String TRANSACTION = "https://dmt.iserveu.tech/generate/v4";
  public static final String VERIFY_BENE_URL = "https://dmt.iserveu.tech/generate/v17";
  public static final String VERIFY_OTP = "https://dmt.iserveu.tech/generate/v18";
  public static final String WALLET2_TRANSACTION_API = "https://dmt.iserveu.tech/generate/v42";
}
